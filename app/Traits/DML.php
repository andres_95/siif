<?php


   namespace RTR\Traits;

     trait DML{
        
           public function findItem($id){
                $item=$this->where($this->primaryKey,$id)->where('idestatus',1);

                return ($item->count()>0)?$item->first():false;
           }
           
           public function updItem($id,$data){
               
                 if(($item=$this->findItem($id))){
                    $item->update($data); 
                    return true;
                 }
                 return false;
           }
           public function delItem($id){
            if(($item=$this->findItem($id))){
                $item->update([
                    'idestatus'=>0
                ]); 
                 return true;
             }
                return false;
           }
         public function allItems(){
             return $this->where('idestatus',1);
         }

         private function setRequest($data){
              $fillables=$this->fillable;
              $request=[];
                 $data=array_change_key_case($data, CASE_LOWER);
                 $this->request=array_map('strtolower',$this->request);
                 
                 foreach($this->request as $key=>$value){
                      
                      if((isset($data[$value]) || isset($data[strtolower($value)])) && !isset($request[$key])){
                         
                         $request[$key]=$data[$value];
                       //  array_push($request,array($key=>$data[$value]));
                         continue;
                      } 
                     

                 }
                 return $request;
         } 

         public function createItem($data){
             $dat=$this->setRequest($data);
              $data=(sizeof($dat))?array_merge($data,$dat):$data;
             
           //   dd($data);
               $this->create($data);
         }
         
     }

?>