<?php

namespace RTR\Http\Controllers;

use Illuminate\Http\Request;
use RTR\modelos\TOperadore;

class operadorcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          TOperadore::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
          $operador=TOperadore::where('id','=',$request->idOperador)
                               ->where('estatus','=',1);

             if($operador->count()==0)
                return response()->json(['error'=>'este operador no se encuentra en el sistema'],404);
            $field=$request->except('id','estatus');
            $operador->update($field->all());
            return response()->json(['success'=>'operador actualizado']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
         $operador=TOperadore::where('id','=',$request->idOperador)
                   ->where('estatus','=',1);

           if($operador->count()==0)
               return response()->json(['error'=>'operador inexistente'],404);

            $operador->update([
                'estatus'=>0
            ]);

               return response()->json(['success'=>'operador eliminado'],200);
    }
}
