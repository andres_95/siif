<?php

namespace RTR\Http\Controllers\API\Actividades;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\TipoActividadRecurso;
use RTR\modelos\TTipoActividade;

class TipoActividadesController extends Controller
{
    //
     public function show(Request $request){
        $actividad=TTipoActividade::all();
        return TipoActividadRecurso::collection($actividad);
    }

    public function store(Request $request){
        do{ $id=rand(); }while(TTipoActividade::find($id));
        TTipoActividade::create([
            'idOperador'=>'12',
            'idfabricante'=>'3',
            'idtipo_actividades'=>$id,
            'descripcion_tipo_actividades'=>$request->descripcion,
            'idestatus'=>'1'
        ]);
        return response()->json(['success'=>'tipo de actividad registrado'],200);
    }

    public function update(Request $request){
        $tipo=TTipoActividade::where('idtipo_actividades','=',$request->id)->where('idestatus','=','1');
        if($tipo->count()==0)
            return response()->json(['error'=>'tipo de actividad no encontrado'],404);
        $tipo->update(['idestatus'=>'1']);
        return response()->json(['success'=>'tipo de actividad actualizada'],200);
    }

    public function delete(Request $request){
        $tipo=TTipoActividade::where('idtipo_actividades','=',$request->id)->where('idestatus','=','1');
        if($tipo->count()==0)
            return response()->json(['error'=>'tipo de actividad no encontrado'],404);
        $tipo->update(['idestatus'=>'0']);
        return response()->json(['success'=>'tipo de actividad eliminada'],200);
    }
}
