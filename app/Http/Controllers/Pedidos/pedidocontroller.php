<?php

namespace RTR\Http\Controllers\Pedidos;

use Validator;
use RTR\modelos\TPersona;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TOrdene;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;
use RTR\Http\Resources\clientePedidoResource;

use Response;

class pedidocontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
           $empresa=TPersona::where('idPersona','=',$request->id)->orwhere('nombre_completo_razon_social','=',$request->id)->where('idestatus','=','1')->first();
           return new clientePedidoResource($empresa->first());
          
    }
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
     public function getOrdenes(Request $request){
          $ordenes=TOrdene::all();
            
              if($request->has('idorden'))
                $ordenes=$ordenes->where('idorden','=',$request->idorden);

              if($request->has('idestatus'))
                $ordenes=$ordenes->where('idestatus','=',$request->idestatus);
                
                return response()->json(['pedidos'=>$ordenes->toArray()],200);
        // $ordenes=TOrdene::where('idPersona','=',$request->idPersona)->where('iorden','=',$request->id)->where('idestatus','=',$request->idestatus)->get();
         
     }

}
