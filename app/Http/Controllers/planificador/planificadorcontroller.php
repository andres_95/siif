<?php

namespace RTR\Http\Controllers\planificador;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\diasResource as dias;
use RTR\modelos\TDiasVisita;
use RTR\modelos\TPersona;
use RTR\modelos\THorario;
//use RTR\modelos\TPersona;
use RTR\modelos\TPlanificadore;

class planificadorcontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $horarios=THorario::all(['descripcion_horarios']);
        $clientes=TPersona::where('idPersona','=',$request->idrfv)->first()->clientes();
        $disponiblesEsedia=TDiasVisita::where('descripcion_dias_visita','like','%'.$request->dia.'%')->first()->personas()->get(['idPersona']);
        $disponibles=$clientes->whereIn('idPersona',$disponiblesEsedia);
        //return response()->json(['que_molestia'=>'Me tiene berserker '],200);
        return response()->json(['dia'=>$request->dia,'clientes'=>$disponibles->get(['nombre_completo_razon_social']),
                                  'horarios'=>$horarios
                                 ],200);

    }

    public function listarPlanificaciones(){
        $planificadores=TPlanificadore::where('idOperador','=',$this->user->idOperador);

           return response()->json([
                'agenda'=>$planificadores->get()
           ]);
    }

    public function planificar(Request $request){
         $plan=TPlanificadore::where('idCliente','=',$request->idcliente)
                ->where('fecha_agenda','=',$request->fecha)
                ->where('hora','=',$request->hora);

                 if($plan->count()>0)
                    return response()->json(['error'=>'Este cliente ya ha sido registrado en la planificación de otro representante de venta'],200);

        TPlanificadore::create(
           [
            'idOperador'=>$this->user->idOperador,
             'idfabricante'=>$this->user->idfabricante,
            'idRFV'=>$request->idrfv,
            'idCliente'=>$request->idcliente,
            'fecha_agenda'=>$request->fecha,
            'hora'=>$request->hora,
            'idbrick'=>$request->idbrick,
            'observacion_agenda'=>$request->observacion,
            'idSupervisor'=>$request->idsupervisor,
            'idestatus'=>'1'
            ]
        );
           return response()->json(['success'=>'plan creado'],200);
    }

    public function upPlanificar(Request $request){
        $plan=TPlanificadore::where('idAgenda','=',$request->idAgenda);


         if($plan->count()==0)
            return response()->json(['error'=>'Este cliente no se encuentra'],200);

            $plan->update(
            [   'idRFV'=>$request->idrfv,
                'idCliente'=>$request->idcliente,
                'fecha_agenda'=>$request->fecha,
                'hora'=>$request->hora,
                'idbrick'=>$request->idbrick,
                'observacion_agenda'=>$request->observacion,
                'idSupervisor'=>$request->idsupervisor,
                'idestatus'=>'1'
                ]
            );

             return response()->json(['success'=>'planificacion actualizada'],200);
    }

    public function delPlanificar(Request $request){
        $plan=TPlanificadore::where('idAgenda','=',$request->idAgenda);

         if($plan->count()==0)
            return response()->json(['error'=>'Este cliente ya ha sido registrado en la planificación de otro representante de venta'],200);

            $plan->update(
            [
                'idestatus'=>'0'
                ]
            );

            return response()->json(['success'=>'planficiacion eliminada'],200);
    }
      public function showPlanes(Request $request){
          $plan=TPlanificadore::where('idRFV','=',$request->idrfv);

              if($plan->count()==0)
              return response()->json(['error'=>'No hay planficaciones'],200);
            return response()->json(['planes'=>$plan->get()],200);
      }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $persona=TPersona::where('idPersona','=',$request->idPersona)->first();
         $dia=TDiasVisita::find($request->iddiavisita);
         
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function clientesByHorario(){
         $clientes=TDiasVisita::all();
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}

