<?php

namespace RTR\Http\Controllers\planificador;

use Illuminate\Http\Request;
use RTR\modelos\TDiasFeriado;
use RTR\modelos\TPaise;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class feriadocontroller extends Controller
{
    private $pais;
    private $feriado;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
         $this->pais=new TPaise();
         $this->feriado=new TDiasFeriado();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $pais=$this->pais->findItem($request->idpais);
                   if(!$pais)
                     return response()->json(['error'=>'pais no encontrado'],404);

         $data=$request->all();
         $data['idOperador']=$this->user->idOperador;
         $pais->dias_feriados()->create($data);
         
          return response()->json(['success'=>'se ha ingresado un dia feriado para este pais'],200);
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         
         if(!$this->feriado->updItem($id,$request->all()))
            return response()->json(['error'=>'dia feriado inexistente'],404);

       return response()->json(['success'=>'dia feriado modficado'],200);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!$this->feriado->delItem($id))
           return response()->json(['error'=>'dia feriado inexistente'],404);

        return response()->json(['success'=>'dia feriado eliminado'],200);
    }
}
