<?php

namespace RTR\Http\Controllers\Administrativo\Categorias;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use RTR\modelos\TCategoria;

use Carbon\Carbon;
use Validator;

class CategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * GET
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        return TCategoria::where('idOperador', $this->user->idOperador)
            ->where('idFabricante',$this->user->idFabricante)->get();
    }

    /**
     * Show the form for creating a new resource.
     * POST
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $current = Carbon::now();

        $validar=Validator::make($request->all(),[
            'idcat'=>'required',
            'nombre'=>'required',
            'FechaInicio'=>'nullable',
            'FechaFin'=>'nullable',
            'URL'=>'nullable'
        ],
        [
            'required'=>'Se necesita el :attribute'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }

        TCategoria::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$this->user->idFabricante,
            'idcategorias'=>$request->idcat,
            'NombreCategorias'=>$request->nombre,
            'FechaCreacion'=>$current,
            'FechaInicio'=>$request->FechaInicio,
            'FechaFin'=>$request->FechaFin,
            'URLImagenCategoria'=>$request->URL,
            'idestatus'=>1
        ]);
        return response(['mensaje'=>'la categoria ha sido creada exitosamente'],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return TCategoria::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *  POST
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $current = Carbon::now();
        if ($request->has('fechaInicio'))
            $inicio = Carbon::parse($request->fechaInicio);
        if ($request->has('fechaFin'))
            $fin = Carbon::parse($request->fechaFin);

        $validar=Validator::make($request->all(),[
            'id'=>'required',
            'nombre'=>'nullable',
            'FechaInicio'=>'nullable',
            'FechaFin'=>'nullable',
            'URL'=>'nullable'
        ],
        [
            'required'=>'Se necesita el :attribute'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }
        
        TCategoria::find($request->id)->update([
            'NombreCategorias'=>$request->nombre,
            'FechaInicio'=>$inicio,
            'FechaFin'=>$fin,
            'URLImagenCategoria'=>$request->URL
        ]);

        return response(['mensaje'=>'la categoria ha sido modificada exitosamente'],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        TCategoria::find($id)->update(['idestatus'=>0]);
            return response(['mensaje'=>'la categoria ha sido eliminada exitosamente'],200);
    }
}
