<?php

namespace RTR\Http\Controllers\Orden;

use RTR\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use RTR\modelos\TOrdenes;
use RTR\modelos\TPersona;
use RTR\modelos\TFacturas;

use RTR\Http\Resources\OrdenRecurso;
use RTR\Http\Resources\FormatoMayo;
use RTR\Http\Resources\FormatoProducto;

use Carbon\Carbon;
use Validator;

class ControladorOrdenes extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productIndex(Request $request){
        /*
         * al presionar ver detalle en la vista de las ordenes por RFV
         * necesito fecha, numero de orden, datos del cliente y del RFV
         * agregar el nombre del producto en la tabla t_item_ordenes
         */
        $response = collect([]);
        $total=0;
        if ( TOrdenes::find($request->orden)->producto()->get()->count() ==0 ){
            return response(["error"=>"la orden no tiene productos"],200);
        }
        foreach(TOrdenes::find($request->orden)->producto()->get() as $producto){
            $total+=$producto->pivot->item_total;
            $response->push($producto->pivot);
        }
        return response(['productos'=>$response,'Total'=>$total],200);
    }
    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear(Request $request){
        //REALIZAR LA TABLA DE RELACION ORDEN MAYORISTA
        $current = Carbon::now();
        $current = new Carbon;
        $costo=0;
        $orden=TOrdenes::all()->count()+1;

        foreach ($request->productos as $prod){
            $costo += $prod['precio']*$prod['cantidad']*($request['impuesto']/100) + $prod['precio']*$prod['cantidad'];
        }

        $validar=Validator::make($request->all(),[
            'productos'=>'array|required',
            'operador'=>'nullable',
            'fabricante'=>'nullable',
            'fabricante'=>'required'
        ],
        [
            'required'=>'Se necesita el :attribute',
            'int'=> 'El :attribute debe ser entero',
            'min'=>'debe introducir una cantidad de :attribute'  
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400); 
         }
         
            $nuevaOrden=TOrdenes::create([
                'idOperador'=>$request->operador,
                'idfabricante'=>$request->fabricante,
                //generar numero de orden
                'idorden'=> $orden,
                'idPersona'=>$request->RFV,
                'idMayorista'=>$request->mayoristas[0]['id'],
                'idPersona_solicitante'=>$request->cliente,
                //pueden ser llenados con insert, no tienen que
                //estar especificados en el request
                'idpais'=>$request->pais,
                'coordenadas_l' =>$request->lat,
                'coordenadas_a'=>$request->lon,
                'ididioma'=>$request->idioma,
                'idmoneda'=>$request->moneda,
                'tipo_operacion'=>$request->operacion,
                'impuesto'=>$request->impuesto,
                /*            'comision'=>$request->comision,
                este campo no se incluira, hacer tabla relacional          'costoTotal'=>$costo-$costo*$mayo['descuento']/100,
                este campo no se incluira, hacer tabla relacional            'descuento'=>$mayo['descuento'],
                */
                'fechaOrden'=>$current,
                
                /*se asignan en otras consultas
                
                'fecha_envio_orden'=>$request->fechaEnvio,
                'registro_entrega'=>$request->registro,
                'fecha_entrega'=>$request->entrega,
                'firma_entrega'=>$request->firmaEntrega,
                'firma_signature'=>$request->signature,

                'comentario_entrega'=>$request->comentario,*/

                //              la factura debe llenarse con la conciliacion.

                'idfactura'=>0,
                'idtipo_incidentes'=>$request->incidentes,
                'idestatus'=>4
            ]);

            foreach( $request->productos as $index=>$item ) {
                $precio=0;
                $precio = $item['precio']*$item['cantidad'] + $item['precio'] * $item['cantidad'] * $request->impuesto / 100;
                $nuevaOrden->producto()->attach(
                    $item['id'],[
                        'idOperador'=>$nuevaOrden->idOperador,
                        'idfactura'=>$nuevaOrden->idfactura,
                        'idfabricante'=>$nuevaOrden->idfabricante,
                        'idMayorista2'=>$request->mayoristas[0]['id'],
                        'iditem_orden'=>$index+1,
                        'item_price'=>$item['precio'],
                        'nombreproducto'=>$item['nombre'],
                        'cantidad_solicitada'=>$item['cantidad'],
                        'idunidades'=>$item['unidades'],
                        'item_descuento' => $request->mayoristas[0]['descuento'],
                        'item_total' => $precio - $precio*$request->mayoristas[0]['descuento']/100,
                        'item_impuesto' => $request->impuesto,
                        'idestatus' => 1
                ]);
            }

            foreach( $request->mayoristas as $index=>$mayo ) {
                $total = $costo-$costo*$mayo['descuento']/100;
                $nuevaOrden->mayorista()->attach(
                    $mayo['id'],[
                        //'idpersona'=>$mayo['id'],
                        'nombre_mayorista'=>$mayo['nombre'],
                        'idOperador'=>$request->operador,
                        'idpais'=>$request->pais,
                        'orden_total'=>$total+$total*$request->impuesto/100,
                        'impuesto'=>$request->impuesto,
                        'idCliente'=>$request->cliente,
                        'idmoneda'=>$request->moneda,
                        'ididioma'=>$request->idioma,
                        'item_descuento' => $mayo['descuento'],
                        'idestatus' => 1
                ]);
            }
            
        return response (['exito'=>'la orden ha sido registrada correctamente'],200); 
    }

    /**
     * Display the specified resource.
     *
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  
    public function show (Request $request){
    /*
     * al presionar seguimiento de pedidos
     */
        if ( TOrdenes::all()->count() === 0 )
            return response(["error"=>"no hay ordenes registradas"],404);

        if ($request->order!=='null'){
            return OrdenRecurso::collection(
                TOrdenes::where('idpersona','=',$request->persona)
                    ->where('idorden','=',$request->order)
                    ->paginate(15)
            );
        }
        return OrdenRecurso::collection(TOrdenes::where('idPersona',$request->persona)->paginate(15));
    }

    /**
     * se genera un objeto JSON para crear la nueva orden
     */
    public function generarJson($mayo,$product,$orden){
        $jsonObject = new \stdClass();
        $jsonObject->operador=$orden->idOperador;
        $jsonObject->fabricante=$orden->idfabricante;
        $jsonObject->productos=$product;
        $jsonObject->RFV=$orden->idPersona;
        $jsonObject->mayoristas=$mayo;
        $jsonObject->cliente=$orden->idPersona_solicitante;
        $jsonObject->impuesto=$orden->impuesto;
        $jsonObject->pais=$orden->idpais;
        $jsonObject->idioma=$orden->ididioma;
        $jsonObject->moneda=$orden->idmoneda;
        $jsonObject->operacion=$orden->tipo_operacion;
        $jsonObject->incidentes=$orden->idtipo_incidentes;
        $jsonObject->nFactura=0;
        $jsonObject->idestatus= 5;
        return json_encode($jsonObject);
    }
    /**
     * se genera concilia una factura respecto a una orden, 
     * se genera una nueva orden a partir de la primera
     */
    public function conciliarFactura (Request $request){
        $orden = TOrdenes::find($request->norden);
        $ordenProducto = collect([]);
        $current = Carbon::now();
        $current = new Carbon;
        
        $orden->update([ "idfactura" => $request->nfactura, 'idestatus' => 6 ]);

        $mayoristas = $orden->mayorista()->get();

        if($orden['idMayorista']) $mayoristas = $mayoristas->diff([$mayoristas->find($orden['idMayorista'])]);

        foreach( $request->productos as $key => $producto ){
            /**
             * actualizar en la tabla de la orden original el campo item_total
             * de acuerdo a la cantidad faltante, este monto será el nuevo monto del total 
             * en el nuevo producto a actualizar
             */
            $itemProducto = $orden->producto()->find($producto['idproducto'])->pivot;
            $itemProducto->update([
                "cantidad_conciliada" => $producto['cantidad_conciliada'],
                "cantidad_faltante"   => $producto['cantidad_faltante'],
                "cantidad_facturada"  => $producto['cantidad_facturada']
            ]);
            if( $producto['cantidad_faltante'] != 0 ){
                $itemProducto["cantidad_solicitada"] = $producto['cantidad_faltante'];
                $ordenProducto->push($itemProducto);
            }
        }

        if ( $mayoristas->count()=== 0 || $ordenProducto->count() === 0)
            return response(["exito"=>"La orden ha sido conciliada"],200);

        $mayoristas = FormatoMayo::collection($mayoristas);
        $ordenProducto = FormatoProducto::collection($ordenProducto);

        /*
         * el nuevo json debera tener el formato del json pasado cuando se crea la orden
         */ 
        $ordenJson = $this->generarJson($mayoristas,$ordenProducto,$orden);

        $validar=Validator::make($request->all(),[
            'productos'=>'array|required',
            'norden'=>'int|required',
            'nfactura'=>'required'
        ],
        [
            'required'=>'Se necesita el :attribute',
            'int'=> 'El :attribute debe ser entero', 
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400); 
         }
        
        $nuevaFactura = TFacturas::create([
            'idOperador'=>$orden->idOperador,
            'idfabricante'=>$orden->idfabricante,
            'idordenes'=> $request->norden,
            'idfactura'=> $request->nfactura,
            'idPersona_solicitante'=>$orden->idPersona_solicitante,
            //pueden ser llenados con insert, no tienen que
            //estar especificados en el request
            'idpais'                 =>$orden->pais,
            'fechaFactura'           =>$current,
            'Fecha_pago_factura'     =>$current,
            'ididioma'               =>$orden->idioma,
            'idmoneda'               =>$orden->idmoneda,
            'impuesto'               =>$orden->impuesto,
            'id_Persona_Solicitante' =>$orden->idPersona_solicitante,
            'idformaPago '           =>$request->formaPago, 
            'DireccionCliente'       =>TPersona::find($orden->idPersona_solicitante)['direccion_domicilio'], 
            'ReferenciaFactura'      =>$orden->norden,
            'DescripcionFactura'     =>$request->descripcion,
            /** No se llenara en este controlador
            * 'Banco'                  =>$request->banco, 
            * 'NumeroCuentaReferencia' =>$request->cuenta, 
            * 'Impuesto_iva'           =>$request->impuesto, 
            * 'RetencionLegal'         =>$request->retencion,
            */
            'idMayorista_despacho'   =>$orden->idMayorista,
            'Descuento'              =>$orden->mayorista()->first()->pivot['item_descuento'],
           //'MontoNeto_Factura'=>$request->total, 
           // monto con todos los descuentos y adiciones hechas a la facturas. se calcula a partir del 
           // monto de la factura y el descuento del mayorista
           //'Monto_Factura'          =>$request->monto, 
            'idestatus'             => 4,
        ]);
        
        /**
         * ahora genero la nueva orden con los nuevos parametros
         */
       // return collect(json_decode($ordenJson,true))->toJson();
        $this->crear($request->replace(json_decode($ordenJson,true)));
        
        $nuevaOrden = TOrdenes::all()->last();
        
        return response ([
            'norden'=>$nuevaOrden['idorden'],
            'productos'=>$ordenProducto,
            'mayoristas'=>$mayoristas,
            'fecha'=>$nuevaOrden['fechaOrden']
        ],200);
    }
}