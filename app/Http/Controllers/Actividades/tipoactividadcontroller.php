<?php
namespace RTR\Http\Controllers\Actividades;


    use Validator;
    use RTR\modelos\TTipoActividade;
    use RTR\modelos\TPersona;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use RTR\Http\Controllers\Controller;

    class tipoactividadcontroller extends Controller{
             
      public function __construct()
      {
          $this->middleware(function ($request, $next) {
              $this->user = Auth::user();
              return $next($request);
          });
      }
         public function index(){
             return response()->json(['tipos_actividades'=>TTipoActividade::where('idestatus',1)->where('idOperador',$this->user->idOperador)->get()],200);
         }

         public function show(Request $request){


         } 

         public function store(Request $request){
         
            //   do{ $id=rand(); }while(TTipoActividade::find($id));
              TTipoActividade::create([
                'idOperador'=>$this->user->idOperador,
                'idfabricante'=>$this->user->idfabricante,
                'idtipo_actividades'=>$request->idtipo,
                'descripcion_tipo_actividades'=>$request->descripcion,
                'idestatus'=>1
              ]);
                return response()->json(['success'=>'tipo de actividad registrado'],200);
         }
        public function update(Request $request,$id){
             $tipo=TTipoActividade::where('idtipo_actividades','=',$id)
                  ->where('idOperador','=',$this->user->idOperador)
                  ->where('idestatus','=',1);

                if($tipo->count()==0)
                   return response()->json(['error'=>'tipo de actividad no encontrado'],404);

                $tipo->update(['descripcion_tipo_actividades'=>$request->descripcion]);

                return response()->json(['success'=>'tipo de actividad actualizada'],200);
        }
      public function destroy($id){
            
        $tipo=TTipoActividade::where('idtipo_actividades','=',$id)->where('idestatus','=',1);
        
                        if($tipo->count()==0)
                           return response()->json(['error'=>'tipo de actividad no encontrado'],404);

                $tipo->update(['idestatus'=>'0']);

                return response()->json(['success'=>'tipo de actividad eliminada'],200);
      }
    }