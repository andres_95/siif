<?php
namespace RTR\Http\Controllers\Actividades;

    use Validator;

    use Illuminate\Support\Facades\Storage;

    use RTR\modelos\TActividadesRepresentante;
    use RTR\modelos\TGrupopersona;
    use RTR\modelos\TPersona;
    use RTR\modelos\TTipoActividade;
    use RTR\modelos\TTipoIncidente;
    use RTR\modelos\TProducto;
    use RTR\modelos\TPlanificadore;
    use RTR\modelos\TBrickRuta as ruta;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;

    use RTR\Http\Controllers\Controller;

    use RTR\Http\Resources\clasePersonaResource as clasePersona;
    use RTR\Http\Resources\ActividadesRecurso;
    use RTR\Http\Resources\vistaActividadResource as vista;

    
    

    //use Image;
    use Carbon\Carbon;

    class actividadcontroller extends Controller{
       private $user;
          public function __construct(){
              $this->user=Auth::User();
          }
    /**
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     **/
    public function index(Request $request){
          // return response('para que te lo chupeis',200); 
          $this->user=Auth::User();
         // return response()->json(['caso'=>$this->user],200);
         $actividades=TActividadesRepresentante::where('idRFV',$this->user);

                   if($actividades->count()==0)
                      return response()->json(['error'=>'no hay reportes generados'],404);

           return response()->json(['actividades'=>ActividadesRecurso::collection($actividades->get())],200);

    }

    public function show(Request $request){
        //   return response('para que te lo chupeis',200);   
        $this->user=Auth::User();
         $detalle=TActividadesRepresentante::where('idRFV','=', $this->user->idPersona)
                  ->where('idCliente','=',$request->cliente)
                  ->where('fecha_actividad','=',$request->fecha)
                  ->where('idestatus','<>','0');

                 if($detalle->count()==0)
                    return response()->json(['error'=>'disculpe esta actividad es inexistente'],404);
              
              return new actividad($detalle->get());
    }

  /*  public function saveFirma(Request $request){
        $file = $request->file('imagen');

            // Get the contents of the file
            $contents = $file->openFile()->fread($file->getSize());

            $actividad=TActividadesRepresentante::where('idReporte','=',$request->idreporte)->where('idestatus','=','1');

                if($actividad->count()==0)
                    return response()->json(['error'=>'actividad no encontrada'],404);

            $actividad->update([
                'Firma_cliente'=>$contents
            ]);

            return response()->json(['nombre'=>'firma registrada. Veamos si funciona'],200);
    }

    public function loadFirma(Request $request){

        $data=$request->idreporte;
           $actividad=TActividadesRepresentante::where('idReporte','=',$request->idreporte)->where('idestatus','=','1');

                     if($actividad->count()==0)
                         return response()->json(['error'=>'actividad no encontrada'],404);

           //  Storage::put('firma.jpg',$actividad->first()->Firma_cliente);
            $img = Image::make($actividad->first()->Firma_cliente);

              $data=base64_encode($actividad->first()->Firma_cliente);

             // $firma=base64_decode($data);
             // $img = Image::make($firma);
            //   return $img->response();

              //  return $img->response();
               return response()->json(['success'=>'firma cargada',
                                       'imagen previa'=>$data],200);

    }*/

    /**
     *  Implemented by Josue Becerra
     *   date 10:57 a.m.-
     *
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request){
        $tipo=TTipoActividade::find($request->tipo);
            if($tipo==null)
                return response()->json([ "error" => "No hay actividades." ],200);

        //decodifica la firma en base 64 para luego almacenar la data de la misma en la base de datos
        //   $firma=base64_decode($request->firma);
        $user=Auth::User();
        
        $agenda=TPlanificadore::find($request->idAgenda);

        $actividad=$tipo->actividades()->create([
            'idPersona'=>$agenda->idRFV,
            //'idOperador'=>$request->operador,
            //'idfabricante'=>$request->fabricante,
            'idRFV'=>$agenda->idRFV,
            'idCliente'=>$agenda->idCliente,
            'idtipo_incidentes'=>$request->incidentes,
            'coordenadas_l'=>$request->lat, 
            'coordenadas_a'=>$request->long,
            'Firma_cliente'=>$request->firma,
            'observaciones_cliente'=>$request->comentario,
            'idestatus'=>'1'
        ]);
        //  return response()->json(['creado'=>$actividad],200);
        $asociado='incidente desconocido';
        if($request->has('incidente')){
            $incidente=TTipoIncidente::find($request->incidente);
            $incidente->actividades()->save($actividad);
            //$actividad->incidente()->associate($request->incidente);
            //$actividad->save();
            //   $asociado=$request->incidente;
        }

        if($request->has('muestras')){
            foreach($request->muestras as $muestra){
            // $producto=TProducto::find($muestra['idproducto']);
                $actividad->muestras()->attach($muestra['idproducto'],['cantidad'=>$muestra['cantidad']]);
            }
        }
        $agenda->actividad()->associate($actividad);
        $agenda->update(['idestatus' =>'1']);

        return response()->json(['success'=>'actividad guardada'],200);
    }

    public function update(Request $request){   
    }

    public function viewFirma(Request $request){
         $data=base64_decode($request->firma);
         $img=Image::make($data);
            return $img->response();
    }

   
    public function actividades(Request $request){
        /*  $actividades=TGrupopersona::where('idgrupo_persona','=','RFV')
                      ->personas()->where('nombre_completo_razon_social','=',$request->nombre)
                      ->where('idciclos','=',$request->ciclo)
                      ->Actividades()->where('idtipo_actividades','=',$request->actividad)
                      ->where('muestra','=',$request->muestra)
                      ->where('')*/

              if($request->has('ruta'))
                 $personas=ruta::where('idbrick','=',$request->ruta)
                          ->where('idgrupo_persona','=','RFV')
                          ->first()->personas();
              else  {
                 $rfvs=TGrupopersona::where('idgrupo_persona','=','RFV')->first()
                        ->personas();
                  //  $rfvs=TPersona::where('idgrupo_persona','=','RFV')->get();

                    }

                      //  return response()->json(['actividades'=>$rfvs->get()],200);
              /*  foreach($rfvs as $rfv){
                    if()
                }*/

           $actividades=TActividadesRepresentante::all();

                       // if($request->has('mayorista'))


                 if($request->has('ciclo') && $rfvs->count()>0)
                      $rfvs=$rfvs->where('idciclos','=',$request->ciclo);

               // if($rfvs->count()>0)

                 if($request->has('idPersona' ) && $rfvs->count()>0)
                    $rfvs=$rfvs->where('idPersona','=',$request->idPersona)->first();


                /* if($request->has(''))
                    $personas=$personas->*/


                if($request->has('fecha_minimo') && $request->has('fecha_maximo') && $rfvs->count()>0){


                     $actividades=$actividades ->where('fecha_actividad','>=',$request->fecha_minimo)
                     ->where('fecha_actividad','<=',$request->fecha_maximo);
                }


                if($request->has('muestra'))
                   $actividades=$actividades->where('muestra','=',$request->muestra);

               if($request->has('actividad'))
                  $actividades=$actividades->where('idtipo_actividades','=',$request->actividad);

               if($request->has('situacion'))
                  $actividades=$actividades->where('idtipo_incidentes','=',$request->situacion);

              if($request->has('mayorista'))
                  $actividades=$actividades->where('idCliente','=',$request->mayorista);


                $actividades->whereIn('idPersona',$rfvs->get(['idPersona']));

              //   dd($actividades);

           return response()->json(['actividades'=>$actividades->toArray()],200);

    }

    public function firmadas(Request $request){
        $firmadas=TPersona::where('idPersona','=',$request->rfv)
                  ->where('idestatus','=','1')
                  ->Actividades()
                  ->where('idestatus','=','5')->get();
          return vista::collection($firmadas);
    }

    /**
     * Traer el cliente la actividad la fecha las muestras un comentario y firma. 
     * json formato {rfv,reporte,actividad}
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function detalleReporte(Request $request){
        /**
         * si la actividad era Entrega de Materiales 
         * ver todos los productos asociados a esa entrega,
         * traer codigo descripcion cantidad y codigo. 
         **/ 
        $this->user=Auth::User();
        $actividad = TActividadesRepresentante::where('idPersona',$this->user->idPersona)->find($request->reporte,[
            'idCliente','idreporte','idtipo_actividades','fecha_actividad','observaciones_cliente','Firma_cliente'
        ]); 
        $actividad['cliente']=TPersona::find($actividad['idCliente'])['nombre_completo_razon_social'];
        $actividad['actividad']=TTipoActividade::find($actividad['idtipo_actividades'])['descripcion_tipo_actividades'];
        $muestras = collect([]);
        if ( $actividad['idtipo_actividades'] == '4' ){
            foreach($actividad->muestras()->get() as $muestra)
                $muestras->push([
                    'codigo'=>$muestra['idproducto'],
                    'descripcion'=>$muestra['nombre_producto'],
                    'cantidad'=>$muestra->pivot['cantidad'],
                    'lote'=>$muestra['lote']
                    ]);
            $actividad['muestras']=$muestras;
        }    
        return $actividad;
    }
    /**
     * hace un filtrado por rango de fecha
     * @param $date1 la menor fecha
     * @param $date2 la mayor fecha
     * @param $activs las actividades del rfv  
     **/
    public function comparaFecha($date1,$date2,$activs){
        $result = collect([]);
        $result2 = collect([]);
        if ($date1 != null){
            foreach($activs as $act){
                if($act['fecha_actividad']->gte($date1))
                    $result->push($act);
            }
        }
        if ($date2 != null){
            if ($date1 == null)
                $result = $activs;
            foreach($result as $res){
                if($res['fecha_actividad']->lte($date2))
                    $result2->push($res);
            }
            return $result2;
        }
        return $result;
    }
    /**
     * Consulta personalizada de reportes del rfv por fecha, actividad y productos
     * si la actividad es entrega de muestras, ademas de la zona brick.
     */
    public function consultaReporte(Request $request){
        /**
         * busqueda por fecha
         **/
        $this->user = Auth::user();
        $fechaMin = null;
        $fechaMax = null;
        $filtrado = collect([]);
        /**
         * busqueda por rango de fecha  
         **/
        if ($request->has('fechaInicio')!='')
            $fechaMin = Carbon::parse($request->fechaInicio.' 00:00:00');

        if ($request->has('fechaFin')!='')
            $fechaMax = Carbon::parse($request->fechaFin.' 23:59:59');
        
        $actividades = TActividadesRepresentante::where('idPersona',$this->user->idPersona)->get();
        $filtrado = $this->comparaFecha( $fechaMin, $fechaMax, $actividades );
        
        if ($filtrado->count() == 0)
            return response(["error"=>"No hay actividades que cumplan con estos parametros"],200);
        return $filtrado[0]->muestras()->get();
        /** 
         * busqueda por tipo de actividad 
         **/
        if ($request->has('actividad')!=''){
           /**
            * busqueda por muestras
            **/
            if ($request->actividad === '005'){
                if ($request->has('muestra')){
                    $filtrado = $filtrado->filter(function($value,$key) use ($request){
                        if( $value->muestras()->find($request->muestra)->count() === 0 )
                            return false;
                        return true;
                    });   
                }
            }
            $filtrado = $filtrado->filter(function($value,$key) use ($request){
                return $value['idtipo_actividades'] == $request->actividad;
            });
            if ( $filtrado->count() === 1){
                return $filtrado->pop();
            }
        }
        if ($filtrado->count() == 0)
            return response(["error"=>"No hay actividades que cumplan con estos parametros"],200);
        
        return $filtrado;
    }

}