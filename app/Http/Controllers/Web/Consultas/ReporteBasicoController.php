<?php

namespace RTR\Http\Controllers\Web\Consultas;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TCiudade;
use RTR\modelos\TEstado;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TTipoActividade;
use RTR\modelos\TTipoIncidente;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TZona;
use RTR\modelos\TBrickRuta;
use RTR\modelos\TOrdene;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TFactura;
use Carbon\Carbon;
use PDF;

class ReporteBasicoController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        /* General */
        if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
            $fabrica = $request->session()->get('user.teams');
        }else
            $fabrica=$this->user->idFabricante;

        if($this->perfil->tipo == 'SUP')
            $RFV=TPersona::where('idsupervisor',$this->user->idPersona)->get();
        else
            $RFV=TPersona::withoutGlobalScopes()->where('idFabricante',$fabrica)->TypeRFV()->get();

        /* Reportes */
        $ranking=TRankingCliente::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();
        $actividades = TTipoActividade::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();
        $incidentes = TTipoIncidente::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();
        $especialidades = TEspecialidade::all();
        $zona=TZona::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();
        $brick=TBrickRuta::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();

        /*Ordenes*/
        $Mayorista=TPersona::withoutGlobalScopes()->where('idFabricante',$fabrica)->TypeMayorista()->get();
        $ordenes=TOrdene::all(['idorden']);
        $estatus = TEstatusOrdene::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();

        /*Facturas*/
        $facturas=TFactura::all(['idfactura']);
        return view('Consultas.Reportes',compact('RFV','ranking','actividades','incidentes','especialidades','zona','brick','estatus','ordenes','Mayorista','facturas','fabrica'));
    }


    public function Reporte_PDF (Request $request){
        //dd($request->all());
        //dd(count($request->filtros));
   //     return response(public_path(),200);
        $title = $request->titulo;
        $fecha = Carbon::now();
        $operaciones=0;
        $html = '
        <!DOCTYPE html>
        <html>
        <body style ="margin:0px; font-size:13px">
        <table style="border:solid 1px black; width:100%">
            <tr>
                <td style =" width:15%" >
                    <img src="'.public_path().'\img\logo.png" style="height:105px; width:145px" >
                </td>
                <td style ="margin:0px; width:80%">
                    <table style="width:100%">
                        <tr align="left">
                            <th style="font-size:15px">
                                SERVICIOS INTEGRALES IF.
                            </th>
                        </tr>
                        <tr>
                            <td>
                                SOLICITANTE
                            </td>
                            <td style="text-align:right">'
                                .strtoupper($this->user['nombre_completo_razon_social']).
                            '</td>
                        </tr>
                        <tr>
                            <td>
                                SUPERVISOR
                            </td>
                            <td style="text-align:right">'
                                .strtoupper(TPersona::find($this->user->idsupervisor)['nombre_completo_razon_social'])
                                /*strtoupper($request->supervisor)*/.
                            '</td>
                        </tr>
                        <tr>
                            <td>
                                FECHA DE SOLICITUD
                            </td>
                            <td style="text-align:right">'
                                .$fecha.
                            '</td>
                        </tr>
                        <tr>
                            <td>
                                EMPRESA
                            </td>
                            <td style="text-align:right">'
                                .strtoupper(TPersona::find($this->user->idempresa_Grupo)['nombre_completo_razon_social'])
                                /*strtoupper($request->fabricante)*/.
                            '</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <h3 style= "text-align:center; width:100% "> RESULTADOS DE LA BUSQUEDA DE '.strtoupper($title).'.</h3>
        <table style="width:100%, align:center">
        <tr>
        ';


        foreach($request->cabecera as $cab){
            $html.='<th>';
            $html.=strtoupper($cab['text']).'</th>
            ';
        }
        $html.='</tr>
        <tr>';
        foreach($request->busqueda as $bus){

            foreach($request->cabecera as $cab){
                if (!strpos($cab['value'], ".")) {
                    $html.='<td>'. $bus[$cab['value']] .'</td>';
                } else {
                    $c = explode(".", $cab['value']);
                    $html.='<td>'. $bus[$c[0]][$c[1]] .'</td>';
                }
            }
            $html.='</tr><tr>';
            $operaciones+=1;
        }
        if(count($request->filtros)>=1){
            $html=trim($html,'<tr>').
            '</table>
            <table style="border: solid 1px black; width:100%; margin-top:20px" >
                <tr>
                    <th style="font-size:15px">
                        FILTROS APLICADOS
                    </th>
                </tr>
                <tr>';
            foreach($request->filtros as $key=>$fil){
                if (($key+1) % 3 === 0){
                    $html.='</tr><tr>';
                }
                $html.='<td><strong>'.strtoupper($fil['nombre']).':</strong></td><td>'.strtoupper($fil['contenido']).'</td>';
            }
        }
        if($request->tipo=='o' || $request->tipo=='f'){
        $html.='</tr>
            </table>
            <table style="width:100%; border:solid black 1px; margin-top:10px">
            <tr>
                <td><strong>
                    TOTAL OPERACIONES:
                </strong></td>
                <td>'.
                    $operaciones.
                '</td>
                <td><strong>
                    TOTAL UNIDADES:
                </strong></td>
                <td>'.
                    $request->unidades.
                '</td>
                <td><strong>
                    MONTO TOTAL:
                </strong></td>
                <td>'.
                    $request->total.
                '</td>
            </tr>
        </table>';
        }
        if($request->tipo=='g'){
            $html.='</tr>
            </table>
            <table style="width:100%; border:solid black 1px; margin-top:10px">
            <tr>
                <td><strong>
                    ORDENES ESPERADAS:
                </strong></td>
                <td>'.
                $request->OE.
                '</td>
                <td><strong>
                    TOTAL ESPERADO:
                </strong></td>
                <td>'.
                $request->unidades.
                '</td>
                <td><strong>
                    ORDENES FACTURADAS:
                </strong></td>
                <td>'.
                $request->OF.
                '</td>
                <td><strong>
                    TOTAL FACTURADO:
                </strong></td>
                <td>'.
                $request->total.
                '</td>
            </tr>
        </table>';
        }
        if($request->fechaIni!="" || $request->fechaFin!=""){
            $html.='</tr>
            </table>
            <table style="width:100%; border:solid black 1px; margin-top:10px">
                <tr>
                    <td><strong>
                        FECHA INICIO:
                    </strong></td>
                    <td>'.
                        $request->fechaIni.
                    '</td>
                    <td><strong>
                        FECHA FIN:
                    </strong></td>
                    <td>'.
                        $request->fechaFin.
                    '</td>
                </tr>
            </table>
            </body>
            </html>';
        }else{
            $html.='</tr>
            </table>
            </body>
            </html>';
        }



       return  PDF::loadHtml($html)->setPaper('a4', 'landscape')->stream('Reporte.pdf');


    }

}
