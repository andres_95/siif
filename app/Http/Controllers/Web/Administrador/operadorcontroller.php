<?php

namespace RTR\Http\Controllers\Web\Administrador;
use RTR\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RTR\modelos\TOperadore;
use RTR\modelos\TPersona;
use RTR\modelos\TPaise;
use Carbon\Carbon;

class operadorcontroller extends Controller
{  
      
     public function crear(){
          
         $paises=TPaise::all();
      
         return view('Admin.usuarios.operador',compact('paises'));
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $Empresa=TPersona::withoutGlobalScopes()
                ->where('idOperador',$this->user->idOperador)
                ->where('idgrupo_persona','FABR')
                ->select(['idOperador','idPersona'])
                ->get();

          $data=$request->all();
          $data['nombre_completo_razon_social']=$data['nombre'].' '.$data['apellido'];
          $data['fecha_inicio_operacion']=Carbon::now()->format('Y-m-d');
        //  TOperadore::create($data);
           (new TOperadore())->createItem($data);
           return view('Admin.usuarios.home_personas',compact('Empresa'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
          $operador=TOperadore::where('id','=',$request->idOperador)
                               ->where('estatus','=',1);

             if($operador->count()==0)
                return response()->json(['error'=>'este operador no se encuentra en el sistema'],404);
            $field=$request->except('id','estatus');
            $operador->update($field->all());
            return response()->json(['success'=>'operador actualizado']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
         $operador=TOperadore::where('id','=',$request->idOperador)
                   ->where('estatus','=',1);

           if($operador->count()==0)
               return response()->json(['error'=>'operador inexistente'],404);

            $operador->update([
                'estatus'=>0
            ]);

               return response()->json(['success'=>'operador eliminado'],200);
    }
}
