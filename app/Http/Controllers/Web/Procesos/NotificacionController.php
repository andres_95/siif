<?php

namespace RTR\Http\Controllers\Web\Procesos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPersona;
use RTR\modelos\TPerfile;

class NotificacionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }
    
    public function index(){
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();

        return view('Procesos.Notificacion', compact('Empresa'));
    }

    public function rfvs(Request $request){
        $RFV=TPersona::withoutGlobalScopes()->where('idFabricante',$request->fabrica)->TypeRFV()->get();

        return response()->json(['RFV'=>$RFV],200);
    }
}
