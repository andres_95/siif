<?php

namespace RTR\Http\Controllers\Web\RTR;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TTipoActividade;
use RTR\modelos\TTipoIncidente;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TPersona;
use Illuminate\Support\Facades\DB;

class TomaReporteController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = $this->user->Perfil()->first();
            return $next($request);
        });
    }

    public function show_report_option()
    {
    	return view('RTR.TomaReportOptions'); 
    }

    public function show_new_report()
    {
    	$actividades = TTipoActividade::all();
    	$incidentes =  TTipoIncidente::all();
    	return view('RTR.nuevoReporte',compact('actividades','incidentes')); 
    }

    public function show_report_agend()
    {
        /*Consult whit INNER JOIN AND GROUP BY */
        $Operador = $this->user->idOperador;
        $Fabriante = $this->user->idFabricante;
        $RFV = $this->user->idPersona;
        $raw = "";


        

        if($this->perfil->tipo=='RFV'){
            $raw = DB::raw("t_personas g inner JOIN (select  idCliente, count(*) as total from t_planificadores where idOperador = '".$Operador."' and idFabricante = '".$Fabriante."' and idRFV= '".$RFV."' and idestatus=1 group by idCliente) idP ON g.idPersona = idP.idCliente");
        }
        else
        {
            $raw = DB::raw("t_personas g inner JOIN (select  idCliente, count(*) as total from t_planificadores where idOperador = '".$Operador."' and idFabricante = '".$Fabriante."'and (idSupervisor= '".$RFV."' or idRFV= '".$RFV."')  and idestatus=1 group by idCliente) idP ON g.idPersona = idP.idCliente");
        }


        $result = json_encode(
            DB::table($raw)
            ->select(['idPersona','nombre_completo_razon_social','idP.total'])
            ->get()->toArray()
        );

        return view('RTR.TomaReportAgenda',compact('result'));
    }

    public function show_new_report_agend(Request $request)
    {
        $actividades = TTipoActividade::all();
        $incidentes =  TTipoIncidente::all();
        $nombreCli= $request['nombre'];
        $idagenda=  $request['idagenda'];
        $idCli = $request['idcliente'];
        $RFV= TPersona::find($request['RFV'],['idPersona','nombre_completo_razon_social']);
        //$productos = TPersona::find($idCli)->materiales()->get()->toJson();
        return view('RTR.TomaReporte',compact('actividades','incidentes','nombreCli','idagenda','idCli','RFV'));
    }
}
