<?php

namespace RTR\Http\Controllers\ciclos;


use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TCiclo;
use Response;

class ciclocontroller extends Controller
{
    //
    public function index(){
        $ciclos=TCiclo::all(['idciclos','descripcion_ciclos']);
          return response()->json(['ciclos'=>$ciclos],200);        
        
    }

    public function store(Request $request){
        (new TCiclo())->createItem($request->all());
         return response()->json(['success'=>'se ha creado un ciclo'],200);
    }

    public function update(Request $request){
        if(!(new TCiclo())->updItem($request->id,$request->all()))
           return response()->json(['error'=>'ciclo no existente'],404);

        return response()->json(['sucess'=>'ciclo actualizado'],200);
    }
   public function destroy(Request $request){
       if(!(new TCiclo())->delItem($request->id))
           return response()->json(['error'=>'ciclo no encontrado'],404);

        return response()->json(['success'=>'ciclo eliminado'],200);
   }
  
}
