<?php
     namespace App\Http\Controllers\temporales;

     use Validator;
    // use App\Http\Resources\bricksrutaResource as ruta;
     use Illuminate\Http\Request;
     use App\Http\Controllers\Controller;
     use Illuminate\Support\Facades\Auth;
     use App\Http\Resources\tmpFacturaResource;
     use \Carbon\Carbon;
     use App\modelos\TTmpFactura;
     use Exception;

class facturacontroller extends Controller
{
    

      public function store(Request $request){
            TTmpFactura::create([
                'idorden'=>$request->orden,
                'idMayorista'=>$request->mayorista,
                'impuesto'=>$request->impuesto,
                'comision'=>$request->comision,
                'costoTotal'=>$request->total,
                'descuento'=>$request->descuento,
                'fechaFactura'=>$request->factura,
                'idtipo_incidentes'=>$request->incidente,
                'iditem_orden'=>$request->itemOrden,
                'idproducto'=>$request->producto,
                'idunidades'=>$request->unidad,
                'cantidad_solicitada'=>$request->solicitada,
                'cantidad_facturada'=>$request->facturada,
                'cantidad_conciliada'=>$request->conciliada,
                'cantidad_faltante'=>$request->faltante,
                'item_price'=>$request->price,
                'item_descuento'=>$request->itemDesc,
                'item_impuesto'=>$request->itemImp,
                'item_total'=>$request->itemTotal,
                'idestatus'=>'1'
            ]);
            return response()->json(['success'=>'se ha creado una factura'],200);
         
      }
    public function update(Request $request){
        $factura=TTmpFactura::where('idorden','=',$request->id)->where('idestatus','=','1');

              if($factura->count()==0)
                 return response()->json(['error'=>'factura no encotrada'],404);
  
            $factura->update([
                'idMayorista'=>$request->mayorista,
                'impuesto'=>$request->impuesto,
                'comision'=>$request->comision,
                'costoTotal'=>$request->total,
                'descuento'=>$request->descuento,
                'fechaFactura'=>$request->factura,
                'idtipo_incidentes'=>$request->incidente,
                'iditem_orden'=>$request->itemOrden,
                'idproducto'=>$request->producto,
                'idunidades'=>$request->unidad,
                'cantidad_solicitada'=>$request->solicitada,
                'cantidad_facturada'=>$request->facturada,
                'cantidad_conciliada'=>$request->conciliada,
                'cantidad_faltante'=>$request->faltante,
                'item_price'=>$request->price,
                'item_descuento'=>$request->itemDesc,
                'item_impuesto'=>$request->itemImp,
                'item_total'=>$request->itemTotal
            ]);

              return response()->json(['success'=>'factura actualizada'],200);
                     
    }
   public function delete(Request $request){
         $factura=TTmpFactura::where('idorden','=',$request->id)->where('idestatus','=','1');
    
                  if($factura->count()==0)
                     return response()->json(['error'=>'factura no encotrada'],404);
           $factura->update([
               'idestatus'=>'0'
               ]);
               return response()->json(['success'=>'factura eliminada'],200);   
        
   }

   public function index(){
          $factura=TTmpFactura::where('idestatus','=','1');

          return tmpFacturaResource::collection($factura->get());

   }
}
