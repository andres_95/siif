<?php
namespace App\Http\Controllers\temporales;

    use Validator;
    use App\modelos\TTmpProducto;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;
    use App\Http\Resources\tmpProductoResource as producto;

   
     class productocontroller extends Controller{

          public function index(){

                 return producto::collection(TTmpProducto::where('idestatus','=','1')->get());

          }

          public function store(Request $request){
               TTmpProducto::create([  
                'idOperador'=>$request->operador,
                'idfabricante'=>$request->fabricante,
                'idProducto'=>$request->producto,
                'descripcion_producto'=>$request->descripcion,
                'presentacion'=>$request->presentacion,
                'precio_fabrica'=>$request->precio,
                'idMayorista'=>$request->idmayorista,
                'Mayorista'=>$request->nombre,
                'descuento'=>$request->descuento,
                'Alta_Baja_Update'=>$request->abu,
                'idestatus'=>'1'
               ]);

                return response()->json(['success'=>'producto creado']);

          }  
    
      public function update(Request $request){
            $producto=TTmpProducto::where('idOperador','=',$request->id)
                                 ->orwhere('idProducto','=',$request->id)
                                 ->orwhere('idMayorista','=',$request->id);
             if($producto->count()==0)
                 return response()->json(['error'=>'producto no encontrado'],404);
          
                 $producto->update([  
                    'idOperador'=>$request->operador,
                    'idfabricante'=>$request->fabricante,
                    'idProducto'=>$request->producto,
                    'descripcion_producto'=>$request->descripcion,
                    'presentacion'=>$request->presentacion,
                    'precio_fabrica'=>$request->precio,
                    'idMayorista'=>$request->idmayorista,
                    'Mayorista'=>$request->nombre,
                    'descuento'=>$request->descuento,
                    'Alta_Baja_Update'=>$request->abu,
                    'idestatus'=>'1'
                   ]);

              return response()->json(['success'=>'producto actualizado'],200);
          
      } 

    public function delete(Request $request){
        $producto=TTmpProducto::where('idOperador','=',$request->id)
        ->orwhere('idProducto','=',$request->id)
        ->orwhere('idMayorista','=',$request->id);
                if($producto->count()==0)
                return response()->json(['error'=>'producto no encontrado'],404);

                $producto->update([  
                      'idestatus'=>'0'
                ]);

                return response()->json(['success'=>'producto eliminado'],200);

    }
}


    ?>