<?php

namespace App\Http\Controllers\temporales;

    use Validator;
    use App\modelos\TTmpRepresentante;
    use App\modelos\TTipoActividade;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;
    use App\Http\Resources\tmpRepresentanteResource as representante;
   

class representantecontroller extends Controller
{
    public function index(){
        return representante::collection(
            TTmpRepresentante::where('idestatus','=','1')->get()
        );
  }
 
  public function show(Request $request){
        
  }  

  public function store(Request $request){
     //  $tipo=TTmpRepresentante::find($request->tipo);
     
      $representante=TTmpRepresentante::create([
        'idOperador'=>$request->operador,
		'idfabricante'=>$request->fabricante, 
		'idRFV'=>$request->rfv,
		'idsupervisor'=>$request->supervisor,
		'nombres_RFV'=>$request->nombre,
		'apellidos_RFV'=>$request->apellido,
		'linea'=>$request->linea,
		'telefono'=>$request->telefono,
		'eMail'=>$request->email,
		'idzona'=>$request->zona,
		'Descripcion_zona'=>$request->descZona,
		'direccion'=>$request->direccion,
		'idBrick'=>$request->brick,
		'descripcion_Brick'=>$request->descBrick,
		'Actividades'=>$request->actividad,
		'Alta_Baja_Update'=>$request->abu,
		'idestatus'=>'1'
      ]);

       return response()->json(['success'=>
              'Representate registrado'
      ],200);
  }

public function update(Request $request){
        $representante=TTmpRepresentante::where('idOperador','=',$request->id)
                  ->orwhere('idfabricante','=',$request->id)
                  ->orwhere('idRFV','=',$request->id)
                  ->where('idestatus','=','1');

          if($representante->count()==0)
             return response()->json(['error'=>'representante no encontrado'],404);

         $representante->update([
            'idOperador'=>$request->operador,
            'idfabricante'=>$request->fabricante, 
            'idRFV'=>$request->rfv,
            'idsupervisor'=>$request->supervisor,
            'nombres_RFV'=>$request->nombre,
            'apellidos_RFV'=>$request->apellido,
            'linea'=>$request->linea,
            'telefono'=>$request->telefono,
            'eMail'=>$request->email,
            'idzona'=>$request->zona,
            'Descripcion_zona'=>$request->descZona,
            'direccion'=>$request->direccion,
            'idBrick'=>$request->brick,
            'descripcion_Brick'=>$request->descBrick,
            'Actividades'=>$request->actividad,
            'Alta_Baja_Update'=>$request->abu
         ]);

         return response()->json(['success'=>'representante actualizado'],200);
         
   }
  
    public function delete(Request $request){
        $representante=TTmpRepresentante::where('idOperador','=',$request->id)->orwhere('idfabricante','=',$request->id)->orwhere('idRFV','=',$request->id)->where('idestatus','=','1');
        
                  if($representante->count()==0)
                     return response()->json(['error'=>'representante no encontrado'],404);

            $representante->update(['idestatus'=>'0']);


            return response()->json(['success'=>'representante eliminado'],200);
    }
}

?>