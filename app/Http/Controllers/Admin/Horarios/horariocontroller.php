<?php

namespace RTR\Http\Controllers\Admin\Horarios;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\THorario;
use RTR\modelos\TPersona;
use RTR\modelos\TPaise;
use Validator;      

class horariocontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="Horarios";
        $pais=TPaise::all();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','pais','fabrica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
           $validator=Validator::make($request->all(),
             [
                 'fabricante'=>'required|exists:t_personas,idPersona',
                 'descripcion'=>'required|unique:t_horarios,descripcion_horarios'
             ]
             );
               if($validator->fails())
                  return response()->json($validator->errors()->all(),404);

         $data=$request->all();
         $data['idOperador']=$this->user->idOperador;
         $data['idpais']=$this->user->idpais;
         $data['idestatus']=1;
         
         $horario=THorario::create([
             'idpais'=>$this->user->idpais,
             'idOperador'=>$this->user->idOperador,
             'idFabricante'=>$request->fabricante, 
             'descripcion_horarios'=>$request->descripcion,
             'idestatus'=>1
         ]);
         $pais=TPaise::find($this->user->idpais);
         return response(['success'=>$horario['idhorarios'],'pais'=>$pais->nombrePais,'id'=>$horario['idhorarios']],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
        $TA=THorario::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                      ->where('idestatus',1)
                     ->get();
        foreach ($TA as $item)
            $item->tipo='h';

        $hola=CRFRecurso::collection($TA)->toJson();
        return response($hola,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        // $horario=new THorario();
                $validator=Validator::make($request->all(),
                [
                    'descripcion'=>'required|unique:t_horarios,descripcion_horarios'
                ]
                );
                if($validator->fails())
                    return response()->json($validator->errors()->all(),404);

            if(!THorario::withoutGlobalScopes()->where('idhorarios',$request->id)->update([
                'descripcion_horarios'=>$request->descripcion
            ])){
               return response()->json(['error'=>'horario no encontrado'],404);
            }
               $pais=TPaise::find($this->user->idpais);
               return response(['success'=>$request->id,'pais'=>$pais->nombrePais],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $validator=Validator::make($request->all(),
        [
            'id'=>'required|exists:t_horarios,idhorarios'
        ]);
        if($validator->fails())
            return response()->json($validator->errors()->all(),404);

        $horario=new THorario();
       // dd($horario);
        if(!THorario::withoutGlobalScopes()->where('idhorarios',$request->id)->update([
            'idestatus'=>0
        ]))
           return response()->json(['error'=>'horario no encontrado'],404);

           return response()->json(['success'=>'horario eliminado'],200);
    }
}
