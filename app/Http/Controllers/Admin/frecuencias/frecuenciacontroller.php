<?php

namespace RTR\Http\Controllers\frecuencias;

use Illuminate\Http\Request;
use RTR\modelos\TFrecuenciaVisita;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;

class frecuenciacontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=$request->all();
        $data['idOperador']=$this->user->idOperador;
        $data['idestatus']=1;
        TFrecuenciaVisita::create($data);
        return response()->json(['success'=>'se ha registrado un frecuencia de visitas'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $frecuencia=new TFrecuenciaVisita();
          if(!$frecuencia->updItem($id,$request->all()))
                return response()->json(['error'=>'frecuencia de visita no encontrada'],404);

           return response()->json(['success'=>'se ha hecho cambios en una de las frecuencias de visita'],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $frecuencia=new TFrecuenciaVisita();
        if(!$frecuencia->delItem($id))
              return response()->json(['error'=>'frecuencia de visita no encontrada'],404);

         return response()->json(['success'=>'se ha eliminado una de las frecuencias de visita'],200);
    }
}
