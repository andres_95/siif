<?php

namespace RTR\Http\Controllers\Admin\Bricks;

use Illuminate\Http\Request;
use RTR\modelos\TMoneda;

use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;

class monedacontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['monedas'=>TMonedad::where('idestatus',1)->where('idOperador',1)],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $data=$request->all();
         $data['idOperador']=$this->user->idOperador;
         $data['idestatus']=1;
         TMoneda::create($data);
         return response()->json(['success'=>'moneda agregada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          if(!(new TMoneda())->updItem($id,$request->all()))
            return response()->json(['error'=>'esta moneda no existe'],404);

        return response()->json(['success'=>'se ha hecho modificaciones en la informacion de la moneda'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!(new TMoneda())->delItem($id))
        return response()->json(['error'=>'esta moneda no existe'],404);

    return response()->json(['success'=>'moneda eliminada'],200);
    }
}
