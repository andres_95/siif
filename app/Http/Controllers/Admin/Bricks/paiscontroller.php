<?php

namespace RTR\Http\Controllers\Admin\Bricks;
     
    use Validator;
    use App\Http\Resources\bricksrutaResource as ruta;
    use Illuminate\Http\Request;
    use RTR\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Auth;
    use \Carbon\Carbon;
    use RTR\modelos\TPaise;
    use RTR\modelos\TEstado;
    use RTR\modelos\TCiudade;
    use RTR\modelos\TIdioma;
    use RTR\modelos\TMoneda;
    use RTR\modelos\TPrecioMoneda;
    
    use Exception;

     class paiscontroller extends Controller{

            private $user;
            private $content;
            private $status;
            public function __construct(){
                $this->middleware(function ($request, $next) {
                    $this->user = Auth::user();
                    return $next($request);
                });
               
            }

           public function store(Request $request){
                 $count=TPaise::all()->count()+1;
               $pais=TPaise::create([
                   'idpais'=>$count,
                   'abreviatura'=>$request->abr,
                   'nombrePais'=>$request->pais,
                   'codigo'=>$request->codigo,
                   'idestatus'=>1
               ]);

                if($request->has('estados')){
                   foreach($request->estados as $estado)
                     {
                         $nuevo=TEstado::create([
                             'idpais'=>$count,
                             'nombreCiudad'=>$request->estado,
                             'idestado'=>$estado->idestado,
                             'idestatus'=>1
                         ]);
                           
                       
                     }
                }
                  return response()->json(['success'=>'un nuevo pais ha sido agregado'],200);
           }

           public function update(Request $request,$id){
               $pais=TPaise::where('idpais','=',$id)->where('idestatus','=',1);

                  if($pais->count()==0)
                     return response()->json(['error'=>'pais no encontrado'],404);
             if($request->has(['abr','pais','codigo']))
                $pais->update([
                    'abreviatura'=>$request->abr,
                    'nombrePais'=>$request->pais,
                    'codigo'=>$request->codigo,
                ]); 

               

                  return response()->json(['success'=>'pais modificado'],200);
           }
            
           public function destroy($id){
            $pais=TPaise::where('idpais','=',$id)->where('idestatus','=',1);
            
                              if($pais->count()==0)
                                 return response()->json(['error'=>'pais no encontrado'],404);
            
                            $pais->update([
                                'idestatus'=>0
                            ]); 
            
                              return response()->json(['success'=>'este pais ha sido eliminado'],200);
           }
         /**
          * @param Illuminate\Http\Request
          * @return Illuminate\Http\Response $response
          */
           public function destroyState(Request $request,$id){
               $pais=TPaise::where('idpais','=',$id);

                      if($pais->count()==0)
                          return response()->json(['error'=>'pais no encontrado'],404);

                     $estados=$pais->first()->estados()->where('idestatus','=','1')
                                     ->where('idestado','=',$request->idestado);

                         if($estados->count()==0)
                            return response()->json(['error'=>'estado no encontrado'],404);

                        $estados->where('idestado','=',$request->idestado)->update([
                            'idestatus'=>'0'
                        ]);

                           return response()->json(['success'=>'estado eliminado'],200);

                        
                   
           }

           public function show($id){
               $pais=TPaise::where('idpais','=',$id)->where('idestatus','=',1);
                   
               return response()->json(['pais'=>(($pais->count()>0)?$pais->first():'no existe pais con este id'),
                                         'estados'=>(($pais->count()>0 && $pais->first()->estados()->count()>0)?$pais->first()->estados()->get():'no tiene estados'),
                                         'idiomas'=>(($pais->count()>0 && $pais->first()->idiomas()->count()>0)?$pais->first()->idiomas()->get():'no hay idiomas asignados'),
                                         'monedas'=>(($pais->count()>0 && $pais->first()->monedas()->count()>0)?$pais->first()->monedas()->get():'no hay idiomas asignados')]
                               ,200);
           }

             /**
              * 
              * define los idiomas a dicho pais
              */
            public function defineIdioma(Request $request){
                 $idioma=(new TIdioma())->findItem($request->ididioma);
                 $pais=(new TPaise())->findItem($request->idpais);
                 
                    if(!$pais || !$idioma){
                        $this->content['error']='idioma o pais no encontrado'; 
                         $this->status=404;
                      }else{ 
                           $pais->idiomas()->attach($idioma);
                           $this->content['success']='se ha definido un idioma'; 
                           $this->status=200;
                        };

                    return response()->json([$this->content],$this->status);
                    
                    
            }

            public function changeIdioma(Request $request,$id){
                $idioma=(new TIdioma())->findItem($id);
                $nuevo=(new TIdioma())->findItem($request->nuevo);
                $pais=(new TPaise())->findItem($request->idpais);
                
                   if(!$pais || !$idioma){
                       $this->content['error']='idioma o pais no encontrado'; 
                        $this->status=404;
                     }else{ 
                          $pais->idiomas()->detach($idioma);
                          $pais->idiomas()->attach($nuevo);
                          $this->content['success']='se ha definido un idioma'; 
                          $this->status=200;
                       };

                   return response()->json([$this->content],$this->status);
            }

            public function deleteIdioma(Request $request,$id){
                $idioma=(new TIdioma())->findItem($id);
                
                $pais=(new TPaise())->findItem($request->idpais);
              //  dd($pais);
                   if(!$idioma || !$pais){
                    $this->content['error']='idioma o pais no encontrado'; 
                    $this->status=404;
                   }
                   else{
                    $pais->idiomas()->detach($idioma);
                    $this->content['success']='idioma quitado'; 
                    $this->status=200;
                   }
                   return response()->json([$this->content],$this->status);   
            }
        
            public function setMoneda(Request $request,$id){
                $moneda=(new TMoneda())->findItem($id);
                $pais=(new TPaise())->findItem($request->idpais);
                $data=$request->all();
                $data['idOperador']=$this->user->idOperador;
                $data['fecha_precio_registro']=Carbon::now()->format('Y-m-d');
                $data['idestatus']=1;
                  if(!$pais || !$moneda){
                    return response()->json(['error'=>'moneda o pais no encontrada'],404);
                  }
                  $pais->monedas()->attach($moneda,$data);

                    return response()->json(['success'=>'se inserto una moneda para este pais'],200);
            }

           public function updMoneda(Request $request,$id){
            $moneda=(new TMoneda())->findItem($id);
            $nuevo=($request->has('idnuevo') && $id!=$request->idnuevo)?(new TMoneda())->findItem($request->idnuevo):null;
            $pais=(new TPaise())->findItem($request->idpais);
            $data=$request->except('idnuevo');
            $data['idOperador']=$this->user->idOperador;
            $data['fecha_precio_registro']=Carbon::now()->format('Y-m-d');
            $data['idestatus']=1;
                 
                    if($nuevo)
                       $data['idmoneda']=$request->idnuevo;

                  if(!$pais)
                    return response()->json([
                         
                    'error'=>'este pais no existe'
                    ]);
                $pais->monedas()->updateExistingPivot($id,$data);

                  return response()->json(['success'=>'se ha actualizado la moneda a este pais'],200);
                
                  
           }

           public function delMoneda($id){
               if(!(new TPrecioMoneda())->delItem($id))
                  return response()->json(['error'=>'esta tasa ya no existe'],404);

                  return response()->json(['success'=>'Tasa eliminada'],200);
           }

           public function index(){
               return response()->json([
                   'pais'=>$pais->where('idestatus',1)->get()
               ]);
           }
          
        }
    
        