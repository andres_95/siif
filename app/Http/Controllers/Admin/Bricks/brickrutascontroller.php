<?php

namespace App\Http\Controllers\Admin\Bricks;
     
    use Validator;
    use RTR\Http\Resources\bricksrutaResource as ruta;
    use Illuminate\Http\Request;
    use RTR\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Auth;
    use \Carbon\Carbon;
    use RTR\modelos\brickRuta;
    use RTR\modelos\TZona;
    use Exception;


    class brickrutascontroller extends Controller{
           
        public function __construct()
        {
            $this->middleware(function ($request, $next) {
                $this->user = Auth::user();
                return $next($request);
            });
        }

      /*   public function store(Request $request){
             do{ $id=rand(); }while(brickRuta::find($id)!=null);

             brickRuta::create([
                 'idbrick'=>$id,
                 'Descripcion'=>$request->descripcion,
                 'estatus'=>'1'
             ]);

             return response()->json(['success'=>'se ha creado una ruta']);
         }*/

     /*   public function update(Request $request){
             
             $ruta=brickRuta::where('idbrick','=',$request->id)->where('estatus','=','1');
              if($ruta->count()==0)
                 return response()->json(['error'=>'ruta no encontrado'],200);

              $ruta->update(['Descripcion'=>$request->descripcion]);

              return response()->json(['success'=>'se ha hecho cambios en la ruta'],200);

               

        }*/

       public function destroy(Request $request){
                $ruta=brickRuta::where('idbrick','=',$request->id)->where('estatus','=','1');
                if($ruta->count()==0)
                   return response()->json(['error'=>'ruta no encontrado'],200);

                $ruta->update(['estatus'=>'0']);

                return response()->json(['success'=>'se ha eliminado esta ruta'],200);


       }
       public function store(Request $request){
           $data=$request->all();
           $data['idOperador']=$this->user->idOperador;
           $data['idFabricante']=$this->user->idFabricante;
           $data['idestatus']=1;
            
            $zona=TZona::find($data['idzona']);
                
                if(!$zona)
                  return response()->json([
                       'error'=>'zona no encontrada'
                  ],404);

                  $zona->rutas()->create($data);
             
            
       }
       public function update(Request $request,$id){
           if(!(new brickRuta)->updItem($id,$request->all()))
               return response()->json(['error'=>'ruta no encontrada'],404);
            
          
       }
    public function index(){
           return ruta::collection(brickRuta::where('estatus','=','1')->get());
    }

    public function show(Request $request){
           
         return new ruta(brickRuta::find($request->id));
     }

    }