<?php

namespace RTR\Http\Controllers\Admin\coberturas;
use RTR\modelos\TPersona;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TCoberturaRv;
use \Carbon\Carbon;


class coberturacontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $persona=TPersona::where('idPersona',$request->id);

           if($persona->count()==0)
               return response()->json(['error'=>'no encontrado'],404);

        $data=$request->all();
        $data['idOperador']=$this->user->idOperador;
        $data['fechaRegistro']=Carbon::now()->format('Y-m-d');
        $persona->first()->setCobertura($data);

          return response()->json(['success'=>'cobertura creada'],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $persona=TPersona::where('idPersona',$request->id)->where('idestatus',1);
        
                   if($persona->count()==0)
                       return response()->json(['error'=>'no encontrado'],404);
        
                $persona->first()->updCobertura($request->all(),$id);

         return response()->json(['success'=>'cobertura actualizada'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $cobertura=TCoberturaRv::where('cod_cobertura_rfv',$id)->where('idestatus',1);

            if($cobertura->count()==0)
              return response()->json(['error'=>'no encontrado'],404);
            $cobertura->update([
                'idestatus'=>'0'
            ]);

                return response()->json(['success'=>'cobertura eliminada'],200);
    }
}
