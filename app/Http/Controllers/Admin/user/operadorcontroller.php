<?php
   namespace RTR\Http\Controllers\Admin\user;


    use Validator;
    use App\modelos\TOperadore;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;
    use App\Http\Resources\clasePersonaResource as clasePersona;

    class operadorcontroller extends Controller{
        
         public function index(){
             
         }

          public function show(Request $request){
          
          }
           /**
            * 
            * @param  Illuminate\Http\Request $request
            * @return  Illuminate\Http\Response
            */
         public function store(Request $request){
              do{ $id=rand(); }while(TOperadore::find($id)!=null);

            $valiador=Validator::make($request->all(),
            [
               'id'=>'required|exists:t_personas,idPersona',
                'tlf'=>'required',
                'movil'=>'required',
                'email'=>'required',
                'direccion'=>'required'
                
            ],
            [
                'required'=>'se necesita :attribute' 
            ]

            );
             TOperadore::create([
                'idOperador'=>$id,
                'idPersona'=>$request->id,
                'documento_identidad'=>$request->id,
                'nombre_completo_razon_social'=>$request->razon,
                'telefono_persona'=>$request->tlf,
                'movil_persona'=>$request->movil,
                'email_persona'=>$request->email,
                'direccion_domicilio'=>$request->direccion,
                'idestatus' =>'1'
             ]);
                return response()->json(['success'=>'nuevo operador creado'],200);

         }

          /**
            * 
            * @param  Illuminate\Http\Request $request
            * @return  Illuminate\Http\Response
            */
            public function update(Request $request){
                $operador=TOperadore::where('idOperador','=',$request->id)->where('idestatus','=','1');

                   if($operador->count()==0)
                      return response()->json(['error'=>'Este operador no se encuentra',404]);

                $operador->update([
                    'idPersona'=>$request->documento_Identidad,
                    'documento_identidad'=>$request->documento_Identidad,
                    'nombre_completo_razon_social'=>$request->razon,
                    'telefono_persona'=>$request->tlf,
                    'movil_persona'=>$request->movil,
                    'email_persona'=>$request->email,
                    'direccion_domicilio'=>$request->direccion,
                ]);
                return response()->json(['success'=>'Datos del operador modificado con exito'],200);
            } 

            /**
            * 
            * @param  Illuminate\Http\Request $request
            * @return  Illuminate\Http\Response
            */

            public function delete(Request $request){
                $operador=TOperadore::where('idOperador','=',$request->id)->where('idestatus','=','1');
                
                     if($operador->count()==0)
                         return response()->json(['error'=>'Este operador no se encuentra']);
           
                 $operador->update(['idestatus'=>'0']);

                 return response()->json(['success'=>'operador dado de baja'],200);

            }


    }