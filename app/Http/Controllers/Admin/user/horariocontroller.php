<?php
   namespace RTR\Http\Controllers\Admin\user;
   /**
    *  Implemented by Josue Becerra
    *  fecha 17 de enero de 2018 
    *
    */

    use Validator;
    use RTR\modelos\Horario;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;
    
       class horariocontroller extends Controller{

           public function index(){
               $horarios=Horario::where('estatus','=','1');
               return response()->json(['horarios'=>$horarios->get(['descripcion_horarios'])],200); 
           }
           public function show(Request $request){
               $horarios=Horario::where('cod_horarios','=',$request->id)->where('estatus','=','1')->first(['descripcion_horarios']);
               return response()->json(['horarios'=>$horarios],200); 
          }
          
        public function store(Request $request){
              do{ $id=rand(); }while(Horario::find($id));
             Horario::create([
                'idOperador'=>'3',
                'cod_horarios'=>$id,
                'descripcion_horarios'=>$request->descripcion,
                'estatus' => '1'
             ]);
             return response()->json(['success'=>'se ha agregado un nuevo horario'],200);
        }
       
       public function update(Request $request){
             $validar=Validator::make($request->all(),[
                 'id'=>'required'
             ],
              [
                  'required'=>':attribute requerido'  
              ]
              );
                   if($validar->fails()){
                       return response()->json(['error'=>$validar->errors()->all()],200);

                   }
           $horario=Horario::where('cod_horarios','=',$request->id)->where('estatus','=','1');
    
              if($horario->count()==0)
                    return response()->json(['error'=>'horario no encontrado'],200);

          $horario->update([
              'descripcion_horarios'=>$request->descripcion
          ]);

              return response()->json(['success'=>'se ha hecho cambios de este horario'],200);
       }

      public function delete(Request $request){
          $horario=Horario::where('cod_horarios','=',$request->id)->where('estatus','=','1');

             if($horario->count()==0)
                  return response()->json(['error'=>'horario no encontrado'],200);

                  $horario->update([
                    'estatus'=>'0'
                ]);
                return response()->json(['success'=>'horario eliminado'],200);
        }
    }

    ?>