<?php
   namespace RTR\Http\Controllers\Admin\user;


    use Validator;
    use RTR\modelos\TGrupopersona;
    use RTR\modelos\TPersona;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use RTR\Http\Controllers\Controller;
    use RTR\Http\Resources\TGrupoPersonaResource as GrupoPersonas;

    class grupopersonacontroller extends Controller{

          private $user;
          public function __construct(){
              $this->middleware(function ($request, $next) {
                  $this->user = Auth::user();
                  return $next($request);
              });
          }
        
           public function store(Request $request){
             // $user=Auth::User();
         /*   idgrupo_persona 
            descripciongrupoPerson  */
            $validar=Validator::make($request->all(),[
              'id'=>'required|unique:t_grupopersonas,idgrupo_persona',
              'descripcion'=>'required|unique:t_grupopersonas,descripciongrupoPersona'
            ]);
              
               if($validar->fails())
                 return response()->json($validar->errors()->all(),404);
               // dd($request->all());
               TGrupopersona::create([
                 'idOperador'=>$this->user->idOperador,
                 'idgrupo_persona'=>$request->id,
              //   'idFabricante'=>$request->fabricante,
                'descripciongrupoPersona'=>$request->descripcion,
                 'idestatus'=>'1'
               ]);

                 return response()->json(['success'=>'grupo de persona agregado'],200);

           }

         public function update(Request $request){
          $validar=Validator::make($request->all(),[
            'id'=>'required',
            'descripcion'=>'required'
          ]);
              $grupo=TGrupopersona::where('idgrupo_persona','=',$request->id)
                      ->where('idOperador',$this->user->idOperador)
                      ->where('idestatus','=','1');
               
               if($grupo->count()==0)
                 return response()->json(['error'=>'grupo eliminado o no existente'],404);

                 $grupo->update([
                   'idgrupo_persona'=>$request->id,
                   'descripciongrupoPersona'=>$request->descripcion
                   ]);
                return response()->json([
                   
                     'success'=>'la descripcion del grupo ha sido cambiado'
                ]) ;
         }

         public function destroy(Request $request){
       /*   $validar=Validator::make($request->all(),[
            'id'=>'required'
          ]);*/

            $grupo=TGrupopersona::where('idgrupo_persona','=',$request->id)->where('idestatus','=','1');

            if($grupo->count()==0)
               return response()->json(['error'=>'grupo eliminado o no existente'],404);

            $grupo->update(['idestatus'=>'0']);
              return response()->json(['success'=>'grupo eliminado'],200);
         }

        public function index(){
             
           return TGrupoPersonas::collection(TGrupoPersona::where('idestatus','=','1')->get());
        }

       public function show(Request $request){
          $validar=Validator::make($request->all(),[
            'id'=>'required|exists:t_TGrupoPersona',

          ]);
            
          if($validar->fails()){
            return response()->json(['error'=>'grupo no encontrado']);
         }
           
           $grupo=TGrupopersona::where('idgrupo_persona','=',$request->id)->where('idestatus','=','1');

             
           return new TGrupopersonas($grupo);
              
           
       }

       public function ver(){
            $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->where('idestatus',1)
            ->select(['idOperador','idPersona'])
            ->get();
            $tipo=TGrupoPersona::where('idestatus',1)->get();
                foreach($tipo as $item){
                  $item->id=$item->idgrupo_persona;
                  $item->descripcion=$item->descripciongrupoPersona;
                }
            $T='Grupo';
            return view('Admin.usuarios.Aspectos',compact('Empresa','T','tipo'));
       }

    }