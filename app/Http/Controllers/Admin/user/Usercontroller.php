<?php

namespace RTR\Http\Controllers\Admin\user;

use Illuminate\Http\Request;
use Validator;
use RTR\modelos\TPersona;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TGrupopersona;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;
use RTR\Http\Resources\personaResource;
use RTR\Http\Resources\clienteResource;
use Response;

class Usercontroller extends Controller
{   

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      //  $user=Auth::User();

        $persona=TPersona::create([
            'idOperador'=>$this->user->idOperador,
            'idfabricante'=>$this->user->idfabricante,
            'idempresa_Grupo'=>$this->user->idempresa_Grupo,
            'idPersona'=>$request->id,
            'username'=>$request->user,
            'password'=>$request->password,
            'confirm_password'=>$request->confirm,
            'nombre_persona'=>$request->nombre,
            'apellido_persona'=>$request->apellido,
            'nombre_completo_razon_social'=>$request->nombre.' '.$request->apellido,
            'documento_Identidad'=>$request->id,
            'sexo_genero_persona'=>$request->sexo,
            'fecha_nacimiento_registro'=>$request->fecha,
            'idestatus'=>'1'
          ]);

           return response()->json(['success'=>'usuario registrado'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $persona=TPersona::where('idPersona','=',$request->id)->where('idestatus','=','1');

               if($persona->count()==0)
                  return response()->json(['error'=>'usuario no encontrado'],404);

        $persona->update([
            'idPersona'=>$request->id,
            'username'=>$request->user,
            'password'=>$request->password,
            'confirm_password'=>$request->confirm,
            'nombre_persona'=>$request->nombre,
            'apellido_persona'=>$request->apellido,
            'nombre_completo_razon_social'=>$request->nombre.' '.$request->apellido,
            'documento_Identidad'=>$request->id,
            'sexo_genero_persona'=>$request->sexo,
            'fecha_nacimiento_registro'=>$request->fecha
        ]);

           return response()->json(['success'=>'usuario modficado'],200);
    }
     
      public function suspend(Request $request){
        $persona=TPersona::where('idPersona','=',$request->id)->where('idestatus','=','1')->orwhere('idestatus','=','2');
        
                       if($persona->count()==0)
                          return response()->json(['error'=>'usuario no encontrado'],404);

          $persona->update([
              'idestatus'=>'2'
          ]);
            return response()->json(['sucess'=>'usuario suspendido'],200);
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $persona=TPersona::where('idPersona','=',$request->id)->where('idestatus','=','1')->orwhere('idestatus','=','2');
        
                       if($persona->count()==0)
                          return response()->json(['error'=>'usuario no encontrado'],404);

          $persona->update([
              'idestatus'=>'0'
          ]);
            return response()->json(['sucess'=>'usuario eliminado'],200);
    }
}
