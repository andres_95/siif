<?php

namespace RTR\Http\Controllers\Admin\Facturacion;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Resources\FacturaRecurso;
use RTR\modelos\TPersona;
use RTR\modelos\TFactura;

class FacturaController extends Controller
{
    //
     public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    } 
    
    public function index(Request $request){

        $Empresa=TPersona::withoutGlobalScopes()
                                ->where('idOperador',$this->user->idOperador)
                                ->where('idgrupo_persona','FABR')
                                ->select(['idOperador','idPersona'])
                                ->get();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.ordenes.lista_facturas',compact('Empresa','fabrica'));
    }

    public function show(Request $request){
        $factura=TFactura::where('idFabricante',$request->fabricante)->get();
        
        return FacturaRecurso::collection($factura);
    }
}
