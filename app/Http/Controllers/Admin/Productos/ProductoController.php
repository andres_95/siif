<?php

namespace RTR\Http\Controllers\Admin\Productos;

use RTR\modelos\TProducto;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;

use Carbon\Carbon;

class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     **/
    public function index()
    {
        //
        return TProducto::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validar=Validator::make($request->all(),[
            'tipo'     => 'nullable',
            'id'          => 'required',
            'mayorista'         => 'required',
            'nombre'     => 'required',
            'descripcion'=> 'nullable',
            'principio'  => 'nullable',
            'precio'     => 'nullable',
            'descuento'  => 'nullable',
            'categoria'        => 'required',
            'unid'          => 'nullable',
            'Finicpub' => 'nullable',
            'Ffinpub'    => 'nullable',
            'linea'    => 'nullable',
            'promocion' =>'nullable',
            'dias' =>'nullable'
        ],
        [
            'required'=>'Se necesita el :attribute'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }
        
        $now = Carbon::now();
        if ($request->has('fechaExp'))
            $fechaExpedicion = Carbon::parse($request->fechaExp);
        if ($request->has('fechaVenc'))
            $fechaVencimiento = Carbon::parse($request->fechaVenc);
        if ($request->has('Finicpub'))
            $FechaInicioPublicacion = Carbon::parse($request->Finicpub);
        if ($request->has('Ffinpub'))
            $FechaFinPublicacion = Carbon::parse($request->Ffinpub);

        $nuevoProducto=TProducto::create([
            'idOperador'          => $this->user->operador,
            'idfabricante'        => $this->user->fabricante,
            'idproducto'          => $request->id,
            'idPersona'           => $this->user->idPersona,
            'idMayorista'         => $request->mayorista,
            'idpais'              => $this->user->idpais,
            'ididioma'            => $this->user->ididioma,
            'idmoneda'            => $this->user->idmoneda,
            'idlinea_producto'    => $request->linea,
            'idtipo_producto'     => $request->tipo,
            'nombre_producto'     => $request->nombre,
            'descripcion_producto'=> $request->descripcion,
            'principio_producto'  => $request->principio,
            'Precio_producto'     => $request->precio,
            'Descuento_producto'  => $request->descuento,
            'idcategorias'        => $request->categoria,
            'idunidades'          => $request->unid,
            'cantidad_producto_existente' => $request->cantidad,
            'fechaRegistro_producto'      => $now,
            'fechaExpedicion_producto'    => $fechaExpedicion,
            'fechaVencimiento_producto'   => $fechaVencimiento,
            'dias_publicacion'            => $request->dias,
            'idpromocion'                 => $request->promocion,
            'FechaInicioPublicacion'      => $FechaInicioPublicacion,
            'FechaFinPublicacion'         => $FechaInicioPublicacion,
            'estatus_producto'            => 1
        ]);

        return response(["exito"=>"producto creado"],200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response (TProducto::find($id),200 );  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validar=Validator::make($request->all(),[
            'tipo'     => 'nullable',
            'id'          => 'required',
            'mayorista'         => 'nullable',
            'nombre'     => 'nullable',
            'descripcion'=> 'nullable',
            'principio'  => 'nullable',
            'precio'     => 'nullable',
            'descuento'  => 'nullable',
            'categoria'        => 'nullable',
            'unid'          => 'nullable',
            'Finicpub' => 'nullable',
            'Ffinpub'    => 'nullable',
            'linea'    => 'nullable',
        ],
        [
            'required'=>'Se necesita el :attribute'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }
        $producto = TProductos::find($request->id);
        //hacer para descuento y precio.
        if ($request->has('fechaExp'))
            $fechaExpedicion = Carbon::parse($request->fechaExp);
        if ($request->has('fechaVenc'))
            $fechaVencimiento = Carbon::parse($request->fechaVenc);
        if ($request->has('Finicpub'))
            $FechaInicioPublicacion = Carbon::parse($request->Finicpub);
        if ($request->has('Ffinpub'))
            $FechaFinPublicacion = Carbon::parse($request->Ffinpub);

        $producto->update([
            'idMayorista'         => $request->mayorista,
            'idlinea_producto'    => $request->linea,
            'idtipo_producto'     => $request->tipo,
            'nombre_producto'     => $request->nombre,
            'descripcion_producto'=> $request->descripcion,
            'principio_producto'  => $request->principio,
            'Precio_producto'     => $request->precio,
            'Descuento_producto'  => $request->descuento,
            'idcategorias'        => $request->categoria,
            'idunidades'          => $request->unid,
            'cantidad_producto_existente' => $request->cantidad,
            'fechaRegistro_producto'      => $now,
            'fechaExpedicion_producto'    => $fechaExpedicion,
            'fechaVencimiento_producto'   => $fechaVencimiento,
            'dias_publicacion'            => $request->dias,
            'idpromocion'                 => $request->promocion,
            'FechaInicioPublicacion'      => $FechaInicioPublicacion,
            'FechaFinPublicacion'         => $FechaInicioPublicacion,
            'estatus_producto'            => 1
        ]);

        return response(["mensaje"=>"el producto ha sido modificado exitosamente"],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $target = TProductos::where('estatus_producto','1')->find($id);

        if (!$target)
            return response(["mensaje"=>"El producto no ha sido encontrado."],200);

        $target->update(['estatus_producto'=>'0']);

        return response(["mensaje"=>"el producto ha sido eliminado exitosamente"],200);
    }
}
