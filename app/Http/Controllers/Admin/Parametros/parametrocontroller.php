<?php

namespace RTR\Http\Controllers\Admin\Parametros;

use Illuminate\Http\Request;

use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TParametro;
use RTR\modelos\TPersona;
use RTR\modelos\TPaise;
use RTR\Http\Resources\CRFRecurso;
use Validator;

class parametrocontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T='Parametros';
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=$request->all();
        $data['idestatus']=1;
        $data['idOperador']=$this->user->idOperador;
        
          $validator=Validator::make($request->all(),TParametro::$validators);
             if($validator->fails())
                return response()->json($validator->errors()->all(),404);

        TParametro::create([
            'idOperador'=>$this->user->idOperador,
            'idparametro'=>$request->id,
            'idFabricante'=>$request->fabricante,
            'descripcion'=>$request->descripcion,
            'valor_meta'=>$request->valor_meta,
            'Porcentaje_Desviacion'=>$request->Porcentaje_Desviacion,
            'idestatus'=>1
        ]);
           return response()->json(['sucess'=>'parametro creado'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
        //
        $parametros=TParametro::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                               ->where('idestatus',1)
                               ->get();

            foreach($parametros as $param){
                $param->tipo='p';
            }
            // dd($parametros);
            return response(CRFRecurso::collection($parametros),200);
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        
        /*  dd( array(TParametro::$validators,
          'valor_meta'=>'required|numeric',
          'id'=>'required|exists:t_parametros,idparametro'));*/
        $validator=Validator::make($request->all(),
            array_merge(TParametro::$validators,
            ['id'=>'required|exists:t_parametros,idparametro']));
        if($validator->fails())
           return response()->json($validator->errors()->all(),404);

         $parametro=TParametro::where('idparametro',$request->id)->where('idestatus',1);
             if($parametro->count()==0)
                return response()->json(['error'=>'parametro no encontrado'],404);

            $parametro->update([
                'descripcion'=>$request->descripcion,
                'valor_meta'=>$request->valor_meta,
                'Porcentaje_Desviacion'=>$request->Porcentaje_Desviacion
            ]);

              return response()->json(['success'=>'parametro actualizado'],200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
          $validator=Validator::make($request->all(),[
              'id'=>'required|exists:t_parametros,idparametro'
          ]);
            if($validator->fails())
               return response()->json($validator->errors()->all(),404);

        $parametro=TParametro::where('idparametro',$request->id)->where('idestatus',1);
        if($parametro->count()==0)
           return response()->json(['error'=>'parametro no encontrado'],404);

          $parametro->update([
              'idestatus'=>0
          ]);
          return response()->json(['success'=>'parametro eliminado'],200);
    }
}
