<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 26/04/2018
 * Time: 12:20
 */

namespace RTR\Http\Controllers\Admin\Seguridad;

use Illuminate\Http\Request;
use RTR\modelos\TLogAuditoria;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPersona;

class AuditoriasController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request){
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['idOperador','idPersona'])
            ->get();
        $T="Auditoria";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.Seguridad.auditoria',compact('Empresa','T','fabrica'));
    }

    public function show(Request $request){

        $auditoria=TLogAuditoria::withoutGlobalScopes()
            ->where('idFabricante',$request->fabricante)
            ->get();

        return response($auditoria,200);
    }

    public function destroy(Request $request){
        $modulos=$request->modulos;

          foreach($modulos as $modulo){

            $audit=TLogAuditoria::where('cod_modulo',$modulo['cod_modulo'])
                                  ->where('cod_opcion',$modulo['cod_opcion'])
                                  ;
              $audit->update(['idestatus'=>$modulo['idestatus']]);

          }
            return response()->json(['success'=>'cambios guardados'],200);
    }

}