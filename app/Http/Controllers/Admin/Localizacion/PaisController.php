<?php

namespace RTR\Http\Controllers\Admin\Localizacion;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TEstado;
use RTR\modelos\TPaise;
use RTR\Http\Resources\SinFABR;
use Validator;

class PaisController extends Controller
{
    //
    public function show(Request $request){

        $estado=TEstado::where('idpais',$request->pais)->get();

        return response()->json($estado);
    }

    public function pais(){
        $pais=TPaise::where('idestatus','=',1)->get();

        return response($pais,200);
    }

    public function index()
    {
        $pais=TPaise::where('idestatus','=',1)->get();
        foreach ($pais as $item)
            $item->tipo='p';

        $T="Paises";
        $valor=SinFABR::collection($pais)->toJson();
        return view('Admin.estadistico.SinFABR',compact('valor','T'));
    }

    public function store(Request $request){
         //   dd($request);
          $validator=Validator::make($request->all(),TPaise::$validators);

             if($validator->fails())
                return response()->json($validator->errors()->all(),404);


         TPaise::create([
             'idpais'=>$request->id,
             'abreviatura'=>$request->abreviatura,
             'nombrePais'=>$request->descripcion,
             'codigo'=>$request->codigo,
             'idestatus'=>1
         ]) ;
         
           return response()->json(['success'=>'pais agregado'],200);

    }  
     public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'descripcion'=>'required',
            'abreviatura'=>'required',
            'codigo'=>TPaise::$validators['codigo']
           ]);
        
           if($validator->fails())
              return response()->json($validator->errors()->all(),404);

          if(!TPaise::where('idpais',$request->id)
                  ->first()->update([
            'nombrePais'=>$request->descripcion,
            'abreviatura'=>$request->abreviatura,
            'codigo'=>$request->codigo
          ])){
              return response()->json(['error'=>'pais no encontrado'],404);
          }
            return response()->json(['success'=>'pais actualizado'],200);
     }

     public function destroy(Request $request){

        $validator=Validator::make($request->all(),['id'=>'required|exists:t_paises,idpais']);
        
           if($validator->fails())
              return response()->json($validator->errors()->all(),404);

          if(!TPaise::where('idpais',$request->id)
                     ->update(['idestatus'=>0])
            )
            return response()->json(['error'=>'pais no encontrado'],404);

            return response()->json(['success'=>'pais eliminado'],200);
     }
}
