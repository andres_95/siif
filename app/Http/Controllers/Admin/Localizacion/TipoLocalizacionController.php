<?php

namespace RTR\Http\Controllers\Admin\Localizacion;


use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\TLocalizacione;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoLocalizacione;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use Validator;

class TipoLocalizacionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="Localizacion";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }

    public function index2(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="TipoLocalizacion";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator=Validator::make($request->all(),TTipoLocalizacione::$validators);
            if($validator->fails())
              return response()->json($validator->errors()->all(),404);

         TTipoLocalizacione::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->fabricante,
            'cod_tipo_localizacion'=>$request->id,
            'descripcion_tipo_localizacion'=>$request->descripcion ,
            'idestatus'=>1
         ]);

           return response()->json(['sucess'=>'forma de localizacion registrada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        

        $local=TTipoLocalizacione::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                                   ->where('idestatus',1)
                                  ->get();
        foreach ($local as $item){
            $item->tipo='l';
        }
      
        $hola=CRFRecurso::collection($local)->toJson();
      //  dd($request->fabricante);
        return response($hola,200);
    }

    public function show2(Request $request)
    {
        $local=TTipoLocalizacione::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get();
        foreach ($local as $item){
            $item->tipo='tl';
        }
        $hola=CRFRecurso::collection($local)->toJson();
        return response($hola,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array('descripcion'=>TTipoLocalizacione::$validators['descripcion']));

          if($validator->fails())
                return response()->json($validator->errors()->all(),404);
                
        $localizacion=TTipoLocalizacione::withoutGlobalScopes()->where('idOperador','=',$this->user->idOperador)
                                          ->where('cod_tipo_localizacion','=',$request->id);
                                          

                    if($localizacion->count()==0)
                       return response()->json(['error'=>'forma de localizacion no encontrada'],404);

                $localizacion->update([
                    'descripcion_tipo_localizacion'=>$request->descripcion
                ]);

                return response()->json(['sucess'=>'forma de localizacion actualizada'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array('id'=>TTipoLocalizacione::$validators['id']));

        if($validator->fails())
           return response()->json($validator->errors()->all(),404);

                    $localizacion= TTipoLocalizacione::withoutGlobalScopes()->where('cod_tipo_localizacion','=',$request->id)
                    ->where('idestatus',1);

            if($localizacion->count()==0)
                    return response()->json(['error'=>'forma de localizacion no encontrada'],404);

                  $localizacion->update([
                          'idestatus'=>'0'
               ]);
               return response()->json(['sucess'=>'forma de localizacion eliminada'],200);
    }
}
