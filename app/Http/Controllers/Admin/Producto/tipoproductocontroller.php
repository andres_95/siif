<?php 
      namespace RTR\Http\Controllers\Admin\Producto;
      
        use RTR\ExcelExport\TipoProductoFormato;
        use RTR\Http\Resources\CRFRecurso;
        use RTR\modelos\TPersona;
        use RTR\modelos\TTipoProducto;
        use Validator;
        use Illuminate\Http\Request;
        use RTR\Http\Controllers\Controller;
        use Illuminate\Support\Facades\Auth;

    class tipoproductocontroller extends Controller{


        public function __construct()
        {
            $this->middleware(function ($request, $next) {
                $this->user = Auth::user();
                return $next($request);
            });
        }

         public function index(){
              $actividad=TTipoProducto::where('estatus','=','1')->get();
            
               return response()->json(['Tipo de producto'=>$actividad],200);
         }

        public function index2(Request $request)
        {
            $Empresa=TPersona::withoutGlobalScopes()
                ->where('idOperador',$this->user->idOperador)
                ->where('idgrupo_persona','FABR')
                ->select(['nombre_completo_razon_social','idPersona'])
                ->get();
            $T="TipoProducto";
            $fabrica = $request->session()->get('user.teams');
            return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
        }

         public function show(Request $request){
            $actividad=TTipoProducto::where( 'idtipo_producto','=',$request->id)->where('idestatus','=','1')
            ->get();
            
               return response()->json(['Tipo de producto'=>$actividad],200);
         }

        public function show2(Request $request)
        {
            $local=TTipoProducto::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                   ->where('idestatus',1)
                   ->get();
            foreach ($local as $item){
                $item->tipo='tp';
            }
            $hola=CRFRecurso::collection($local)->toJson();
            return response($hola,200);
        }

         public function store(Request $request){
             
            $validators=Validator::make($request->all(),[
                'id'=>'required|unique:t_tipo_productos,idtipo_producto',
                'fabricante'=>'required|exists:t_personas,idPersona',
                'descripcion'=>'required'
            ]);
                if($validators->fails())
                   return response()->json($validators->errors()->all(),404);

            $tipo=TTipoProducto::create([
                'idOperador' =>$this->user->idOperador, 
                'idtipo_producto'=>$request->id,
                'idFabricante'=>$request->fabricante,
                'descripcion_tipo_producto'=>$request->descripcion ,
                'idestatus' =>'1'
               
              ]);
               return response()->json(['success'=>$tipo['idtipo_producto']],200);

         }
        public function update(Request $request){
              $validar=Validator::make($request->all(),[
                  'id'=>'required|exists:t_tipo_productos,idtipo_producto',
                  'fabricante'=>'required|exists:t_personas,idPersona',
                  'descripcion'=>'required'
                  ]);
              if($validar->fails())
                   return response()->json($validators->errors()->all(),404);

             $actividad=TTipoProducto::withoutGlobalScopes()->where('idtipo_producto','=',$request->id)->where('idestatus','=','1');
                if($actividad->count()==0)
                   return response()->json(['errors'=>'Tipo de producto no encontrado'],404);

           $actividad->update([
               'descripcion_tipo_producto'=>$request->descripcion
           ]);
            return response()->json(['success'=>$request->id],200);
        }

         public function destroy(Request $request){
                    
            $validar=Validator::make($request->all(),[
                'id'=>'required|exists:t_tipo_productos,idtipo_producto',
                ]);
            if($validar->fails())
                 return response()->json($validators->errors()->all(),404);

                $actividad=TTipoProducto::withoutGlobalScopes()->where('idtipo_producto','=',$request->id)->where('idestatus','=','1');
                    if($actividad->count()==0)
                        return response()->json(['errors'=>'Tipo de producto no encontrada'],404);

                $actividad->update([
                    'idestatus'=>'0'
                ]);
          return response()->json(['success'=>'Tipo de producto eliminada'],200);
       }

        public function exportExcel(Request $request)
        {
            $validator = Validator::make($request->all(),
                [
                    'id' => 'required|exists:t_personas,idFabricante'
                ]
            );

            if($validator->fails()){
                return response()->json($validator->errors()->all(), 400);
            }
            return (new TipoProductoFormato())->download('tipoproductoformato.xlsx');
        }
    }