<?php

namespace RTR\Http\Controllers\Admin\unidades;

use Illuminate\Http\Request;
use RTR\modelos\TUnidade;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\SinFABR;

use Validator;
class unidadcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          return response()->json(['unidades'=>TUnidade::where('idestatus','=',1)->get()],200);
    }

    public function index2()
    {
        $unidad=TUnidade::where('idestatus','=',1)->get();
        foreach ($unidad as $item)
            $item->tipo='u';

        $T="Unidades";
        $valor=SinFABR::collection($unidad)->toJson();
        return view('Admin.estadistico.SinFABR',compact('valor','T'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $validator=Validator::make($request->all(),
            [
                'descripcion'=>'required|unique:t_unidades,descripcion_unidades'

            ]
            );
              if($validator->fails())
                return response()->json($validator->errors()->all(),404);

        //
        $unidad=TUnidade::create([
            'descripcion_unidades'=>$request->descripcion
        ]);
         //  dd($unidad['id']);
            return response()->json(['success'=>'unidad ingresada','id'=>$unidad['id']],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator=Validator::make($request->all(),
        [
            'id'=>'required|exists:t_unidades,id',
            'descripcion'=>'required|unique:t_unidades,descripcion_unidades'

        ]
        );
          if($validator->fails())
            return response()->json($validator->errors()->all(),404);
        //
        $unidad=TUnidade::where('id','=',$request->id)
                ->where('idestatus','=',1);

                if($unidad->count()==0)
                   return response()->json(['error'=>'unidad no encontrada'],200);
                 $unidad->update([
                     'descripcion_unidades'=>$request->descripcion
                 ]);

                return response()->json(['success'=>'unidad actualizada'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator=Validator::make($request->all(),
        [
            'id'=>'required|exists:t_unidades,id',
           

        ]
        );
          if($validator->fails())
            return response()->json($validator->errors()->all(),404);
        //
        $unidad=TUnidade::where('id','=',$request->id)
        ->where('idestatus','=',1);

        if($unidad->count()==0)
             return response()->json(['error'=>'unidad no encontrada'],200);
        
         $unidad->update([
             'idestatus'=>0
         ]);

        return response()->json(['success'=>'unidad eliminada'],200);
}
    
}
