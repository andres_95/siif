<?php

namespace RTR\Http\Controllers\Admin\planificador;


use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;

use RTR\modelos\TPersona;
use RTR\modelos\TActividadesRepresentante;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TDiasVisita;
use RTR\Http\Resources\CitaRecurso;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Validator;

class ControladorPlanificador extends Controller{

      
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Mostrar todos los clientes del RFV junto con las fechas de disponibilidad.(check)
     * hacer una consulta a la tabla r_cliente_rfv primero, y luego de eso hacer una consulta a la tabla
     * t_persona_dias donde el cliente especifica los dias donde esta disponible.(check)
     * la hora podemos consultarla en la tabla t_personas.
     *  
     * Crear una cita nueva con un cliente en la tabla t_planificadores (check)
     * recibir json con {cliente,rfv,fecha de visita,hora de visita}
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $validar=Validator::make($request->all(),[
            'rfv'    => 'required',
            'cliente'=> 'required',
            'fecha'  => 'required',
            'hora'   => 'required'
        ],
        [
            'required'=>'Se necesita la :attribute',
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400); 
        }

        $rfv = TPersona::find($request->rfv);
        $fecha = $request->fecha.' '.$request->hora;

       if (TPlanificadore::where('fecha_agenda',$request->fecha)->get()->count() !== 0 && TPlanificadore::where('hora',$request->hora)->get()->count() !== 0)  
            return response(['error'=>'la actividad ya ha sido agendada'],200);

        TPlanificadore::create([
            'idOperador'   => $rfv['idOperador'],//Auth::User()->idOperador,
            'idfabricante' => $rfv['idfabricante'],
            'idAgenda'     => TPlanificadore::all()->count()+1,
            'idRFV'        => $request->rfv,
            'idCliente'    => $request->cliente,
            'idbrick'     =>$request->brick,
            'fecha_agenda' => $fecha,
            'hora'         => $fecha,
            //'idbrick'      => $rfv->brick()->get[''],
            'observacion_agenda' =>$request->comentarios,
            'idSupervisor' => $rfv['idsupervisor'],
            'observacion_agenda'=>$request->descripcion,
            'idestatus' => 0,      
        ]);

        return response(['exito'=>'la actividad ha sido agendada satisfactoriamente'],200);

    }

    /**
     * Para mostrar todas las citas agendadas por el rfv
     * recibir json {rfv}
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrarVisitas(Request $request){
        /*foreach(TPlanificadore::where('idRFV',$request->rfv)->get() as $rfv){
            $citas->push(['cliente'=>TPersona::find($rfv['idCliente'])['nombre_completo_razon_social'],'cita'=>$rfv]);
        }*/
        //return TPlanificadore::where('idRFV',$request->rfv)->get();
        //$citas = CitaRecurso::collection(TPlanificadore::where('idRFV',$request->rfv)->get());
        return CitaRecurso::collection(TPlanificadore::where('idRFV',$request->rfv)->get());
    }

    /**
     * Mostrar los dias que esta disponible el rfv 
     * pasar json {rfv}
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function mostrarDisponible(Request $request){   
        $rfv = TPersona::find($request->rfv);
        $clientes = $rfv->clientes()->get(['idPersona','nombre_completo_razon_social','horaMin','horaMax']);
        if ($clientes->count()==0)
            return response(['error'=>'el RFV no tiene clientes asociados'],200);
        foreach($clientes as $cli){
            $dias=TPersona::find($cli->idPersona)->dias()->get(['descripcion_dias_visita']);
            if ($dias->count() == 0)
                $cli['diasDisponible'] = 'no esta disponible';
            $cli['diasDisponible']=$dias;
        }
        return response()->json(['RFV'=>$rfv['idPersona'],'Clientes'=>$clientes->toArray()],200);
    }

    /**
     * Misma estructura de la funcion de mostrarVisitas pero las citas agendadas 
     * estan por arreglo y aparecen por cada uno de los clientes.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function citaReporte(Request $request){
        $clientes = TPersona::find($request->rfv)->clientes()->get(['idPersona','nombre_completo_razon_social']);
        
        foreach($clientes as $cli){
            $citas = TPlanificadore::where('idRFV',$request->rfv)->where('idCliente',$cli['idPersona'])->get(['idAgenda','fecha_agenda','hora']);
            if ($citas->count() == 0)
                $cli['citas'] = "no hay citas agendadas"; 
            $cli['citas'] = $citas;
        }
        return response($clientes,200);
    }




       /**
        *  
        *
        */
        public function update(Request $request,$id){
           
            $planificador=(new TPersona())->findItem($request->idRFV)->agenda()->where('idAgenda',$id)->where('idestatus',1);
            $actividad=(new TActividadesRepresentante())->findItem($request->idreporte);
              
            if(!($planificador->first())||!($planificador->first()->attach($actividad)->update($request->except('idAgenda'))))
                     return response()->json(['error'=>'esta actividad agendada no existe o ya se actualizado'],404);
                return response()->json(['success'=>'agenda modificada'],200);
        }

        public function destroy(Request $request,$id){
            $planificador=(new TPersona())->findItem($request->idRFV)->agenda()->where('idAgenda',$id)->where('idestatus',1);
             
            if(!($planificador->first())||!($planificador->first()->update(['idestatus'=>0])))
               return response()->json(['error'=>'esta actividad agendada no existe o ya se actualizado'],404);
           
         return response()->json(['success'=>'agenda modificada'],200);

        }
       
        





}
?>
