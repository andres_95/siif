<?php

namespace RTR\Http\Controllers\Admin\Orden;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\FacturaRecurso;
use RTR\modelos\TPersona;
use RTR\modelos\TFactura;

class FacturaController extends Controller
{

    public function index(){

        $Empresa=TPersona::withoutGlobalScopes()->where('idFabricante','')->get(['idPersona']);
        return view('Admin.ordenes.lista_facturas',compact('Empresa'));
    }

    public function show(Request $request){
        $factura=TFactura::where('idFabricante',$request->fabricante)->get();
        
        return FacturaRecurso::collection($factura);
    }
    public function destroy(Request $request){
        if(!(new TFactura())->delItem($request->idfactura))
           return response()->json(['error'=>'factura no encontrada'],404);

           return response()->json(['success'=>'factura eliminada'],200);
   }

}