<?php

namespace RTR\Http\Controllers\Admin\ranking;

use Illuminate\Http\Request;
use RTR\modelos\TOperadore;
use RTR\modelos\TRankingCliente as Rank;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class rankingcontroller extends Controller
{

/*
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          return response()->json(['ranking'=>Rank::where('idOperador','=',$this->user->idOperador)
                                    ->where('estatus','=','1')->get()
          ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         Rank::create([
             'idOperador'=>$this->user->idOperador,
             'descripcion_ranking_clientecol'=>$request->descripcion,
             'estatus'=>1
         ]);

         return response()->json(['success'=>'ranking registrado'],200);
         
         
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
         $operador=TOperadore::find($this->user->idOperador);

            $rank=$operador->ranks()->where('id','=', $id);

                if($rank->count()==0)
                   response()->json(['error'=>'ranking no encontrado'],404);
            $descripcion=$request->only('descripcion');
             $rank->update(
                 ['descripcion_ranking_clientecol'=>$request->descripcion]);
          
              return response()->json(['success'=>'ranking actualizado'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $operador=TOperadore::find($this->user->idOperador);
        
                    $rank=$operador->ranks()->where('id','=', $id);
        
                        if($rank->count()==0)
                           response()->json(['error'=>'ranking no encontrado'],404);
                           
                  //  $descripcion=$request->only('descripcion_ranking_clientecol');
                     $rank->update([
                         'estatus'=>0
                     ]);
                  
                      return response()->json(['success'=>'ranking eliminado'],200);     
                
                  
    }

}
