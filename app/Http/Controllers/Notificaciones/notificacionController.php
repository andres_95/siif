<?php

namespace RTR\Http\Controllers\Notificaciones;

use Illuminate\Http\Request;
use RTR\modelos\TNotificacion;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TTipoNotificacion;
use RTR\Notifications\rtrNotification;
use RTR\Http\Resources\notificacionResource as notificacion;
use Validator;


class notificacionController extends Controller
{


    private $user;
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
         $tipo=TTipoNotificacion::find($request->tipo);
         if($tipo==null || $tipo->idestatus=='0')
            return response()->json([
                 
                'error'=>'este tipo de notificación no existe'
            ],200);

        TNotificacion::create([
            'idOperador'=>$this->user->idOperador,
            'idPersona'=>$request->idPersona, 
            'descripcion_notoficacion' =>$request->descripcion, 
            'idestatus' =>'1',
            'idtipo'=>$request->tipo
        ]); 
           return response()->json(['success'=>'notificacion creada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    public function notificar(Request $request){
        $validator=Validator::make($request->all(),[
            'idnotificacion'=>'required',
            'actividad_notificacion'=>'required'
        ]);
            if($validator->fails())
               return response()->json(['error'=>$validador->errors()->all()],404);

      $notificacion=TNotificacion::find($request->idnotificacion);
      $notificacion->idestatus=$request->actividad_notificacion;
      $notificacion->save();

        return response()->json(['success'=>'listo'],200);
           
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id){
        //

       /* $validator=Validator::make($request->all(),[
            'descripcion'=>'required',
            'id'=>'required'     
        ]);*/
           /* if($validator->fails())
               return response()->json(['error'=>$validator->errors()->all()],404);*/
               
          $notificacion=TNotificacion::where('idNotificacion','=',$id)
                          ->where('idestatus','=','1');
                        
                         
                      

                if($notificacion->count()==0)
                    return response()->json(['error'=>'Notificacion no encontrada'],404);
                
                $notificacion->first()->update([
                    'idPersona'=>$request->idPersona ,
                    'descripcion_notoficacion'=>$request->descripcion 
                    
                ]); 

                   return response()->json(['success'=>'Notificacion actualizada'],200);
        }
        public function destroy($id){
               
            /*$validator=Validator::make($request->all(),[
                'id'=>'required'     
            ]);*/
                        
           /* if($validator->fails())
            return response()->json(['error'=>$validator->errors()->all()],404);*/

            $notificacion=TNotificacion::where('idNotificacion','=',$id)
            ->where('idestatus','=','1');
                   

                    if($notificacion->count()==0)
                        return response()->json(['error'=>'Notificacion no encontrada'],404);
                    
                    $notificacion->update([
                        'idestatus'=>'0'
                    ]); 

                    return response()->json(['success'=>'Notificacion eliminada'],200);
        }

      public function index(Request $request){
       /* $mensaje=TMensajeNotificable::find(1);
        return response()->json(['mensajes'=>$mensaje->gruposDestino()->get()]);*/
           $notificaciones=TNotificacion::where('idPersona','=',$this->user->idPersona)
                             ->where('idOperador','=',$this->user->idOperador);

             if($notificaciones->count()==0)
                return response()->json(null,404);

                   return notificacion::collection($notificaciones->get());
               // return response()->json(['notificaciones'=>$notificaciones->get()],200);
    }
    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function indexcrud(){
        $notificaciones=TNotificacion::where('idOperador','=',$this->user->idOperador);
     
           if($notificaciones->count()==0)
              return response()->json(['error'=>'no hay notificaciones'],404);
     
               ///  return $notificaciones->get();
      //  dd($notificaciones);
         return response()->json(['notificaciones'=>'que beta'],200);
      // return response()->json(['success'=>'Notificacion eliminada'],200);
    }

   
}