<?php

namespace RTR\Http\Controllers\Notificaciones;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TTipoNotificacion;
use RTR\Http\Controllers\Controller;

class tiponotificacioncontroller extends Controller
{

    private $user;
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['tipo'=>TTipoNotificacion::where('idestatus','=','1')->get()],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
           
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        TTipoNotificacion::create([
            'idOperador'=>$this->user->idOperador,
            'titulo'=>$request->titulo,
            'idestatus'=>'1'
        ]);
           
           response()->json(['success'=>'tipo de notificacion agregado'],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

       $tipo=TTipoNotificacion::where('id','=',$id)
                  ->where('idestatus','=','1');

            if($tipo->count()==0)
               return response()->json(['error'=>'tipo de notificacion no encontrado'],404);

      $tipo->update([
           
            'titulo'=>$request->titulo,
           
        ]);

           return response()->json(['success'=>'tipo de notificacion actualizado'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
                    $tipo=TTipoNotificacion::where('id','=',$id)
                          ->where('idestatus','=','1');

            if($tipo->count()==0)
                return response()->json(['error'=>'tipo de notificacion no encontrado'],404);

            $tipo->update([
            
            'idestatus'=>'0'
            ]);

            return response()->json(['success'=>'tipo de notificacion eliminado'],200);
    }
}
