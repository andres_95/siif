<?php

namespace RTR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PDF;
use PHPExcel_Cell;
use Validator;
use Storage;

class filercontroller extends Controller
{
    //
    public function checkExcel(){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');
        
        $writer = new Xlsx($spreadsheet);
        $writer->save('../storage/hello world.xlsx');
       // $file=public_path('../storage/hello world.xlsx');
        //return Storage::download('hello world.xlsx');

       // $file = public_path('../storage/hello world.xlsx');
       /* return (new Response($file, 200))
            ->header('Content-Type', 'application/vnd.ms-excel')
            ->header('Content-Disposition: attachment; filename="sheet1.xlsx"');*/
     //  return response()->download($file,'dfs.xlsx',['Content-Type: application/excell']);
     
         return response()->download('../storage/hello world.xlsx');
    }

    public function checkPDF(){
         $html='<!DOCTYPE html>
         <html>
         <body>
         
         <table style="width:100%">
           <tr>
             <th>Firstname</th>
             <th>Lastname</th> 
             <th>Age</th>
           </tr>
           <tr>
             <td>Jill</td>
             <td>Smith</td>
             <td>50</td>
           </tr>
           <tr>
             <td>Eve</td>
             <td>Jackson</td>
             <td>94</td>
           </tr>
           <tr>
             <td>John</td>
             <td>Doe</td>
             <td>80</td>
           </tr>
         </table>
         
         </body>
         </html>
         ';
         PDF::loadHTML($html)->save('../storage/myfile.pdf');

           $file=storage_path('../storage/myfile.pdf');
           $headers = ['Content-Type: application/pdf'];
           $newName = 'league.pdf';
           return response()->download($file,$newName);
    }

    private function getControl($items){
        $keys=array();  //keys table stores keys for each json row
        $values=array();  //values matrix stores values for each json row

         foreach($items as $item){
                 $value_array=array();
                foreach($item as $key=>$value){
                      // echo $value;
                      $key=str_replace("_"," ",$key);
                    if(array_search($key,$keys)===false){
                        
                        array_push($keys,$key);
                    }
                     array_push($value_array,$value);
                }
                  array_push($values,$value_array); 

           }
              
              return ['keys'=>$keys,'values'=>$values];
        }
    
         private function find($string,$worksheet){
            $coordinate=null;
           
            foreach ($worksheet->getRowIterator() as $row) {
               
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                   //    even if a cell value is not set.
                                                                   // By default, only cells that have a value
                                                                   //    set will be iterated.
                foreach ($cellIterator as $cell) {
                 
                      
                        if($cell->getValue()===$string){
                         
                          $coordinate=$cell->getCoordinate();
                          }
                        
                }
               
            }
                 return $coordinate;
         }
        
    public function excel(Request $request){

        $validator=Validator::make($request->all(),[
            'title'=>'required',
            'items'=>'required'

        ]);
            if($validator->fails()){
                 return response()->json(['error'=>$validator->errors()->all()],404);
            }

         $data=$this->getControl($request->items);
         $keys=$data['keys'];
         $values=$data['values'];
             
       /*  $spreadsheet = new Spreadsheet();
         $sheet = $spreadsheet->getActiveSheet();
         $greenNotBold = array("font" => array("bold" => true,"size"=>12,),);*/
               //crear lector de office 
         $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx'); 
         $reader->setReadDataOnly(FALSE);
         $spreadsheet = $reader->load("../storage/test.xlsx");
         $spreadsheet->setActiveSheetIndex(0);
         $worksheet = $spreadsheet->getActiveSheet();  //obtiene la hoja de calculo

          $theme=$this->find('$Subject',$worksheet);
          $titleCoordinate=$this->find('$column',$worksheet);
          $valueCoordinate=$this->find('$values',$worksheet);
            
            
           if($theme){
             $greenNotBold = array("font" => array("bold" => true,"size" => 20,),);
             $column=$worksheet->getCell($theme)->getColumn();
             $worksheet->getColumnDimension($column)
             ->setAutoSize(false);
             //$worksheet->mergeCells($theme:$theme[0])->getAlignment()->setWrapText(true); 
             $worksheet->getStyle($theme)
                                  ->applyFromArray($greenNotBold);
                                  
                                 
             $worksheet->setCellValue($theme,$request->title); 
          
           }
             
          
           
        if($titleCoordinate!==null && $valueCoordinate!==null ){
            $column=$worksheet->getCell($titleCoordinate)->getColumn();
            $valuecol=$worksheet->getCell($valueCoordinate)->getColumn();
           ///  return response('sdf',200);
            $greenNotBold = array("font" => array("bold" => true,"size" => 12,),);
           $worksheet->fromArray($keys,NULL,$titleCoordinate);
           $worksheet->getStyle($titleCoordinate.':'.chr(ord($titleCoordinate)+sizeof($keys)).$worksheet->getCell($titleCoordinate)->getRow())
                          ->applyFromArray($greenNotBold);
           
          $worksheet->fromArray($values,NULL,$valueCoordinate);
          $writer=new Xlsx($spreadsheet);
          $writer->save('../storage/'.$request->title.'.xlsx');

        }
        $headers =array(
            'Content-type:application/pdf',
            'Content-Disposition:attachment; filename=../storage/'.$request->title.'.xlsx',
            'Content-Transfer-encoding:binary',
            'Content-Length:filesize(../storage/'.$request->title.'.xlsx)'
        );
                 
        $file=basename('../storage/'.$request->title.'.xlsx');
        return response()->download('../storage/'.$request->title.'.xlsx',$file,$headers)
        ->deleteFileAfterSend(true);
     }
     
    public function pdf(Request $request){
        $validator=Validator::make($request->all(),[
            'title'=>'required',
            'items'=>'required'

        ]);
            if($validator->fails()){
                 return response()->json(['error'=>$validator->errors()->all()],404);
            }

         $data=$this->getControl($request->items);
         $keys=$data['keys'];
         $values=$data['values'];
         $style='
         <head>
         <style>
         table, th, td {
             border: 1px solid black;
             border-collapse: collapse;
         }
         th, td {
             padding: 5px;
             text-align: left;
         }
         </style>
         </head>
             ';
              $title='<center><h1>'.$request->title.'</h1></center>';
              
             /* <tr>
              <th>Month</th>
              <th>Savings</th>
            </tr>*/
                //En esta zona, se arma la tabla con las claves y con los valores
              $tr='';
              $td='';
                 //Arma las claves como cabeceras de la tabla
              foreach($keys as $key){
                 $td=$td.'<th>'.$key.'</th>';
              }
             $tr='<tr>'.$td.'</tr>';
              
                    //Armar el cuerpo de la tabla con los registros de items
                foreach($values as $registro){
                     $td='';
                   foreach($registro as $value){
                       $td=$td.'<td>'.$value.'</td>';
                   }
                   $tr=$tr.'<tr>'.$td.'</tr>';
                }

            $table='<table style="width:100%">'.$tr.'</table>';

              
            PDF::loadHTML($style.$title.$table)->save('../storage/'.$request->title.'.pdf'); 
            $file=basename('../storage/'.$request->title.'.pdf');
            $headers = array('Content-Type: application/pdf');
            
           return response()->download('../storage/'.$request->title.'.pdf',$file,$headers)
                        ->deleteFileAfterSend(true);
    }



}
