<?php
namespace RTR\Http\Utilities;

class MonitorUtils
{
    public static function utf8_converter($array)
    {
        array_walk_recursive($array, function(&$item, $key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    public static function csvtojson($file,$delimiter)
    {
        if (($handle = fopen($file, "r")) === false)
        {
            die("can't open the file.");
        }

        $csv_headers = fgetcsv($handle,0 , $delimiter);
        $csv_json = array();

        while ($row = fgetcsv($handle, 0, $delimiter))
        {
            $csv_json[] = array_combine($csv_headers, $row);
        }

        fclose($handle);
        return json_encode($csv_json);
    }

    public static function get_EsquemaAsociado($codigoEntrada){

        $salida = "notFound";

        $ruta = __DIR__. '/../../../Commons';
        $sch_path = __DIR__.'/../../../Commons/schema_source.csv';

        $listadoEsquemas = self::read_CSV_ConfigSys($sch_path);

        $hallado = false;
        foreach($listadoEsquemas as $obj){
            if($codigoEntrada == $obj->codigo){
                $esquemaAsociado = $obj->schema;
                $hallado = true;
                break;
            }
        }

        if($hallado){
            $salida = realpath($ruta . $esquemaAsociado);
        }

        return $salida;

    }

    public static function read_CSV($archivo){

        $file = MonitorUtils::csvtojson($archivo, ";");
        $file = json_decode($file);

        return $file;
    }

    public static function read_CSV_ConfigSys($ruta){
        $file = self::read_CSV($ruta);
        return $file;
    }





    public static function eliminar_simbolos($string){

        $string = trim($string);

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );

        $string = str_replace(
            array("\\", "¨", "º", "-", "~",
                "#", "@", "|", "!",
                "·", "$", "%", "&", "/",
                "(", ")", "?", "'", "¡",
                //"¿", "[", "^", "<code>", "]",
                "¿", "^", "<code>",
                "+", "¨", "´",
                ">", "< ", ";"
            ),
            ' ',
            $string
        );
        return $string;
    }

}

