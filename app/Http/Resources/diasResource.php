<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;


class diasResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);

         $clientes=$this->personas();
         $dias=$this->descripcion_días_visita;
         $listas=[];
         $tok = strtok($dias, ",");
         array_push($listas,$tok);
         while ($tok !== false) {
             $tok = strtok(",");
             if($tok!==false)
              array_push($listas,$tok);
         }
            
          return [
              'dias'=>(count($listas)>0)?$listas:$dias,
              'clientes'=>$clientes->get()
          ];
           
    }
}
