<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;

class CitaRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $estado = "Visitado";
        if($this->idestatus == 1){
            $estado = "Por Visitar";
        }
        return array(
            'start'=>$this->fecha_agenda.' '.$this->hora,
            'title'=>TPersona::find($this['idCliente'])['nombre_completo_razon_social'],
            'rfv'=>TPersona::find($this['idRFV'])['nombre_completo_razon_social'],
            'fecha'=>$this->fecha_agenda,
            'hora'=>$this->hora,
            'status'=>$estado
        );
    }
}
