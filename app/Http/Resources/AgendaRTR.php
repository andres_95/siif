<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TFrecuenciaVisita;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TPlanificadore;

class AgendaRTR extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = Auth::user();
        return [
            'idRFV' => $user->nombre_completo_razon_social,
            'nombre_completo_razon_social' => $this->nombre_completo_razon_social,
            'idranking' => TRankingCliente::withoutGlobalScopes()->find($this->idranking)->descripcion_ranking_cliente,
            'idactividad_negocio' => TEspecialidade::find($this->idactividad_negocio)->descripcion_especialidad,
            'iddias_visita' =>[],
            'idhorarios' =>[],
            'brick' => [],
            'frecuencia' => substr(TFrecuenciaVisita::find($this->idfrecuencia)->descripcion_frecuencia_visitas,0,1),
            'visitado' => TPlanificadore::withoutGlobalScopes()->where(['idRFV'=>$user->idPersona,'idCliente'=>$this->idPersona,'idestatus'=>2])->get()->count(),
            'dias_visita' => $this->Dias()->get(),
            'horarios' => $this->Horarios()->get()
        ];
    }
}
