<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class representanteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
           [
              'nombre'=>$this->nombre_persona.''.$this->apellido_persona 
           ];
   }
}