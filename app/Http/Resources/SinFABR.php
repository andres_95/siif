<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class SinFABR extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $alias='';
        $tipo='';
        $pais='';
        $estado='';
        $abr='';

        if($this->tipo == 'u'){
            $id=$this->id;
            $descripcion=$this->descripcion_unidades;
        }
        if($this->tipo == 'se'){
            $id=$this->id;
            $descripcion=$this->descripcion_actividad_negocio;
        }
        if($this->tipo == 'p'){
            $id=$this->idpais;
            $descripcion=$this->nombrePais;
            $abr=$this->abreviatura;
        }
        if($this->tipo == 'i'){
            $id=$this->ididioma;
            $descripcion=$this->descripcion;
        }
        if($this->tipo == 'c'){
            $id=$this->idCiudad;
            $descripcion=$this->nombreCiudad;
            $pais=$this->idpais;
            $estado=$this->idestado;
        }
        if($this->tipo == 'e'){
            $id=$this->idestado;
            $descripcion=$this->nombreCiudad;
            $pais->$this->idpais;
        }
        if($this->tipo == 'fp'){
            $id=$this->idformaPago;
            $descripcion=$this->descripcion;
        }
        if($this->tipo == 'eg'){
            $id=$this->id;
            $descripcion=$this->descripcion;
        }
        if($this->tipo == 'to'){
            $id=$this->id;
            $descripcion=$this->descripcion;
        }
        return [
            'id' => $id,
            'descripcion' => $descripcion,
            'pais'=>$pais,
            'tipo'=>$tipo,
            'alias'=>$alias,
            'estado'=>$estado,
            'abr'=>$abr
        ];
    }
}