<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;

class logResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
          $persona=TPersona::withoutGlobalScopes()->find($this->idPersona);
          
        return [
            'Persona'=>$persona->nombre_completo_razon_social,
            'Dispositivo'=>(!$this->id_Dispositivo_generoModifcacion)?"sin dispositivo":$this->id_Dispositivo_generoModifcacion,
            'Tipo'=>(!$this->tipoLog)?"CRUD":$this->tipoLog,
            'Descripcion'=>$this->RegistroOcurrido,
            'Fecha'=>$this->fecha_registro
        ];
    }
}
