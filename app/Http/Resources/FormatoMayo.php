<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FormatoMayo extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"        =>$this->pivot->idPersona,
    		"nombre"    =>$this->pivot->nombre_mayorista,
    		"descuento" =>$this->pivot->item_descuento,
            //"incidentes"=>$this->pivot->idtipo_incidente,
            "total"     =>$this->pivot->orden_total
        ];
    }
}
