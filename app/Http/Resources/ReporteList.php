<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoActividade;

class ReporteList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'RFV'=> TPersona::withoutGlobalScopes()->find($this->idRFV)->nombre_completo_razon_social,
            'Cliente' => TPersona::withoutGlobalScopes()->find($this->idCliente)->nombre_completo_razon_social,
            'idRFV' => $this->idRFV,
            'idreporte' => $this->idreporte,
            'fecha_actividad' => $this->fecha_actividad->format('d/m/Y H:i'),
            'tipo_actividades' => TTipoActividade::withoutGlobalScopes()->find($this->idtipo_actividades)->descripcion_tipo_actividades,
            'observaciones_cliente' => $this->observaciones_cliente,
            'loading' => false
        ];
    }
}
