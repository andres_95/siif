<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;
class AgendaCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->idAgenda,
            'Cliente'=>$this->idCliente,
            'fecha'=>$this->fecha_agenda,
            'hora'=>$this->hora,
            'Brick'=>$this->idbrick,
            'RFV'=>TPersona::find($this->idRFV,['idPersona','nombre_completo_razon_social'])
        ];
    }
}
