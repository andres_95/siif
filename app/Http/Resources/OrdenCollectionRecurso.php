<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TPersona;
class OrdenCollectionRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            'nOrden' => $this->idorden,
            'estatus'=> TEstatusOrdene::find($this->idestatus)->descripcion,
            'cliente'=> TPersona::find($this->idPersona_solicitante)['nombre_completo_razon_social'],
            'persona'=> TPersona::find($this->idPersona)['nombre_completo_razon_social'],
            'Registrado_Por'=>TPersona::find($this->idpasadopor)['nombre_completo_razon_social'],
            'fecha'  => $this->fechaOrden,
            'Total'=>$this->costoTotal,
        );
    }
}
