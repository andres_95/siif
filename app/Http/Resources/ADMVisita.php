<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TFrecuenciaVisita;
use RTR\modelos\TPersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TPlanificadore;

class ADMVisita extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $rfv=$this->id_RFV;
        return [
            'RFV'=>TPersona::withoutGlobalScopes()->find($rfv)->nombre_persona,
            'Cliente'=>TPersona::withoutGlobalScopes()->find($this->id_cliente)->nombre_persona,
        ];
    }
}