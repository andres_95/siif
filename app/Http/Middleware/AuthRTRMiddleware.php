<?php

namespace RTR\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use RTR\modelos\TPersona;

class AuthRTRMiddleware
{  
     private $auth;

    public function  __construct(Auth $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * 
     * 
     */
    public function handle($request, Closure $next,$guard=null)
    {    //  return response($request->bearerToken(),200);
       if($this->auth->guard($guard)->guest()) 
       {
           return response()->json(['error'=>'no autorizado'],404);
       }  
           return $next($request);
    }
}
