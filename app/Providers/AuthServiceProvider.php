<?php

namespace RTR\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'RTR\Model' => 'RTR\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
      /*  $this->app['Auth']->extend('api',function($request){
            return response('hola mundo',200);
        });*/

         Auth::extend('api',function($request){
           //  $user=TPersona::where('api_token','=',$request->input('api_token'))->first();
           if($request->input('api_token')){
            $user=TPersona::where('api_token','=',$request->input('api_token'))->first();
               
             return $user;
           }
          });
      
    }
}
