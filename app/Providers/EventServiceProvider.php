<?php

namespace RTR\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'RTR\Events\Event' => [
            'RTR\Listeners\EventListener'
        ],
        'RTR\Events\UserEvent'=>[
            'RTR\Listeners\UserListener',
            
        ],
        'RTR\Events\UpdateUserEvent'=>[
            'RTR\Listeners\UpdateUserListener',
        ],
        'RTR\Events\ProductEvent'=>[
            'RTR\Listeners\ProductListener',
        ],
        'RTR\Events\UpdateProductEvent'=>[
            'RTR\Listeners\UpdateProductListener'
        ],
        'RTR\Events\EsquemaEvent'=>[
            'RTR\Listeners\EsquemaListener'
        ],
        'RTR\Events\UpdateEsquemaEvent'=>[
            'RTR\Listeners\UpdateEsquemaListener'
        ],
        'RTR\Events\ordenEvent'=>[
            'RTR\Listeners\ordenListener'
        ],
        'RTR\Events\UpdateordenEvent'=>[
            'RTR\Listeners\UpdateordenListener'
        ],
        'RTR\Events\facturaEvent'=>[
            'RTR\Listeners\facturaListener'
        ],
        'RTR\Events\UpdatefacturaEvent'=>[
            'RTR\Listeners\UpdatefacturaListener'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
