<?php

namespace RTR\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use RTR\modelos\TPersona;

class UpdateUserEvent extends Event
{
   

      /**
     * Create a new event instance.
     *
     * @return void
     */
    public $persona;
    public $auditable;
    
        public function __construct(TPersona $persona)
        {
            //
            $this->persona=$persona;
            $this->auditable=$this->isAuditable($persona);
        }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
