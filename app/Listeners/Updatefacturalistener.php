<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\events\UpdatefacturaEvent;

class Updatefacturalistener
{
   

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdatefacturaEvent $event)
    {
        //
           if($event->factura['idestatus']==0)
            {
                $this->log_null($event->factura);
                return;
            }
             $this->log_update($event->factura);
    }
}
