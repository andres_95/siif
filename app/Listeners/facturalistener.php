<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\events\facturaEvent;

class facturalistener extends EventListener
{
   
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(facturaEvent $event)
    {
        //
        $this->log_create($event->factura);
    }
}
