<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\Events\UpdateEsquemaEvent;

class UpdateEsquemaListener extends EventListener
{
   

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateEsquemaEvent $event)
    {
        //
        if(!$event->isAuditable($event->esquema)) 
            return;

          if($event->esquema['idestatus']==0){
            $this->log_null($event->esquema);
            return;
          }
            $this->log_update($event->esquema);
    }
}
