<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\events\UpdateProductEvent;

class UpdateProductListener extends EventListener
{
   

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateProductEvent $event)
    {
        //
        if(!$event->isAuditable($event->producto))
            return;
            
         if($event->producto['estatus_producto']==0){
            $this->log_null($event->producto);
            return;
        }
            $this->log_update($event->producto);
    }
}
