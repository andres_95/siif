<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\Events\UpdateUserEvent;

class UpdateUserListener extends EventListener
{
  

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateUserEvent $event)
    {
        //
        if(!$event->isAuditable($event->persona))
            return;

        if($event->persona['idestatus']==0){
           $this->log_null($event->persona);
           return;
        }
         $this->log_update($event->persona);
    }
}
