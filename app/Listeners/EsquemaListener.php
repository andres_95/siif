<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\events\EsquemaEvent;

class EsquemaListener extends EventListener
{
  

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(EsquemaEvent $event)
    {
        //
       if($event->isAuditable($event->esquema)) 
          $this->log_create($event->esquema);
    }
}
