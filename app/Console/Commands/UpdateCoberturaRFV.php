<?php

namespace RTR\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use RTR\modelos\TPersona;
use RTR\modelos\TCoberturaRv;
use RTR\modelos\TBrickRutaPersona;
use RTR\modelos\TCiclo;

use Validator;
use Carbon\Carbon;

class UpdateCoberturaRFV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCoberturaRFV';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualizar tabla de cobertura';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        //dd("hola");
        $empresas=TPersona::withoutGlobalScopes()
                        ->where('idgrupo_persona','FABR')
                        ->select(['idOperador','idPersona'])
                        ->get();
        $aux=[];
        foreach($empresas as $item){
            array_push($aux,"'".$item['idPersona']."'");
        }
         
        $current = Carbon::now();
        $yesterday = Carbon::yesterday();

        $ordenBase = "SELECT idPersona, SUM(TotalUnidades) AS unidades, SUM(costoTotal) AS total, COUNT(*) AS cantidad FROM `t_ordenes` WHERE `fechaOrden` = DATE('".$current."')";
        $reporteBase = "SELECT idRFV, COUNT(*) AS cantidad FROM `t_planificadores` WHERE `idFabricante` IN (".implode(",",$aux).") AND `fecha_agenda` = DATE('".$current."')";

        $esperadas = "SELECT esp.idPersona, 0 AS cantFact, 0 AS montoFact, 0 AS unidFact,  esp.cantidad AS cantEsp, esp.total AS montoEsp, esp.unidades AS unidEsp FROM (".$ordenBase." AND `idestatus` NOT IN ('6','8','7') GROUP BY idPersona) AS esp";
        $facturadas ="SELECT fact.idPersona, fact.cantidad AS cantFact, fact.total AS montoFact, fact.unidades AS unidFact, 0 AS cantEsp,  0 AS montoEsp, 0 AS unidEsp FROM (".$ordenBase." AND `idestatus` = '6' GROUP BY idPersona) AS fact";

        $totalOrden = "SELECT res.idPersona, SUM(res.cantFact) AS cantFact, SUM(res.montoFact) AS montoFact, SUM(res.unidFact) AS unidFact, SUM(res.cantEsp) AS cantEsp, SUM(res.montoEsp) AS montoEsp, SUM(res.unidEsp) AS unidEsp, 0 AS sinVisitar, 0 AS visitados FROM ((".$facturadas.") UNION (".$esperadas.")) AS res GROUP BY res.idPersona";

        $visitados = "SELECT vis.idRFV, vis.cantidad AS visitados, 0 AS sinVisitar FROM (".$reporteBase." AND `idestatus` = '2' GROUP BY idRFV) AS vis";
        $sinVisitar = "SELECT sv.idRFV, 0 AS visitados, sv.cantidad AS sinVisitar FROM (".$reporteBase." AND `idestatus` = '1' GROUP BY idRFV) AS sv";

        $totalReporte = "SELECT res.idRFV AS idPersona, 0 AS cantFact, 0 AS montoFact, 0 AS unidFact, 0 AS cantEsp,  0 AS montoEsp, 0 AS unidEsp, SUM(res.sinVisitar) AS sinVisitar, SUM(res.visitados) AS visitados FROM ((".$visitados.") UNION (".$sinVisitar.")) AS res GROUP BY res.idRFV";

        $totalPersonas = DB::select("SELECT pers.idPersona AS idPersona, SUM(pers.cantFact) AS cantFact, SUM(pers.montoFact) AS montoFact, SUM(pers.unidFact) AS unidFact, SUM(pers.cantEsp) AS cantEsp, SUM(pers.montoEsp) AS montoEsp, SUM(pers.unidEsp) AS unidEsp, SUM(pers.SinVisitar) AS sinVisitar, SUM(pers.visitados) AS visitados FROM ((".$totalOrden.") UNION (".$totalReporte.")) AS pers GROUP BY pers.idPersona");

        if ($current->month !== $yesterday->month)
        {
            foreach(TPersona::where('idgrupo_persona','RFV')->where('idestatus',1)->get() as $persona)
            {
                $zonaBrick = TBrickRutaPersona::where('idPersona',$persona->idPersona)->first();
                $ciclo = TCiclo::where('idFabricante',$persona->idFabricante)->first();
                TCoberturaRv::create([
                    'idOperador'=>$persona['idOperador'],
                    'idFabricante'=>$persona['idFabricante'],
                    'idRFV'=>$persona['idPersona'],
                    'idsupervisor'=>$persona['idsupervisor'],
                    'idzona'=>$zonaBrick['idzona'],
                    'idruta'=>$zonaBrick['idbrick'],
                    'idestado'=>$persona['idestado'],
                    'idciudad'=>$persona['idciudad'],
                    'fechaRegistro'=>$current,
                    'cant_esperada'=>0,
                    'cant_realizada'=>0,
                    'productoEsperado'=>0,
                    'productoFacturado'=>0,
                    'cant_ordenes_realizadas'=>0,
                    'cant_ordenes_parciales'=>0,
                    'porce_cobertura'=>0,
                    'monto_facturado'=>0,
                    'monto_esperado'=>0,
                    'numero_ciclo'=>$ciclo->id,
                    'MesRegistro'=>$current->month."/".$current->year,
                    'idestatus'=>1
                ]);
            }
        }

        foreach ($totalPersonas as $persona)
        {
            $update = "UPDATE `t_cobertura_rfv` SET cant_esperada = cant_esperada + ".$persona->sinVisitar.", cant_realizada = cant_realizada + ".$persona->visitados.", monto_facturado = monto_facturado + ".$persona->montoFact.", cant_ordenes_realizadas = cant_ordenes_realizadas + ".$persona->cantFact.", cant_ordenes_parciales = cant_ordenes_parciales + ".$persona->cantEsp.", monto_esperado = monto_esperado + ".$persona->montoEsp.", productoEsperado = productoEsperado + ".$persona->unidEsp.", productoFacturado = productoFacturado + ".$persona->unidFact.", porce_cobertura = ( (cant_realizada + cant_esperada - cant_esperada)*100/(cant_realizada + cant_esperada)), fechaRegistro = '".$current."' WHERE MONTH(`fechaRegistro`) = MONTH('".$current."') AND `idRFV` = '".$persona->idPersona."'";
            DB::statement($update);
        }
    }
}
