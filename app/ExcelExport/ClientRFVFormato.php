<?php

namespace RTR\ExcelExport;


use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use RTR\modelos\TPersona;

class ClientRFVFormato implements FromCollection, WithHeadings, WithTitle,WithEvents
{
    use Exportable, RegistersEventListeners;


    static $fabricante;

    public function __construct($Fabricante = '')
    {
        self::$fabricante = $Fabricante;
    }

    public function collection()
    {
        return collect([]);
    }

    public function headings(): array
    {
        return [
            'Cliente',
            'RFV'
        ];
    }
    public function title(): string
    {
        return 'Cliente_RFV';
    }

    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );

        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('B')->setAutoSize(true);

        $cliente= TPersona::withoutGlobalScopes()
            ->where(['idFabricante'=>self::$fabricante,'idgrupo_persona'=>'CLI'])
            ->select(['idPersona','nombre_completo_razon_social'])
            ->get()->toArray();
        $event->sheet->fromArray(
            $cliente,
            NULL,
            'E3'
        );
        $event->sheet->mergeCells('E1:F1');
        $event->sheet->setCellValue('E1', 'Clientes');
        $event->sheet->setCellValue('E2', 'ID');
        $event->sheet->setCellValue('F2', 'NOMBRE');
        $event->sheet->getColumnDimension('E')->setAutoSize(true);
        $event->sheet->getColumnDimension('F')->setAutoSize(true);

        $rfv= TPersona::withoutGlobalScopes()
            ->where(['idFabricante'=>self::$fabricante,'idgrupo_persona'=>'RFV'])
            ->select(['idPersona','nombre_completo_razon_social'])
            ->get()->toArray();
        $event->sheet->fromArray(
            $rfv,
            NULL,
            'H3'
        );

        $event->sheet->mergeCells('H1:I1');
        $event->sheet->setCellValue('H1', 'Representantes de venta');
        $event->sheet->setCellValue('H2', 'ID');
        $event->sheet->setCellValue('I2', 'NOMBRE');
        $event->sheet->getColumnDimension('H')->setAutoSize(true);
        $event->sheet->getColumnDimension('I')->setAutoSize(true);

        $cantC = count($cliente)+2;
        //dd($stringC,$cant);
        $event->sheet->setDataValidation(
            'A:A',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1('$E$3:$E$'.$cantC)
        );

        $cantR = count($rfv)+2;
        //dd($cant);
        $event->sheet->setDataValidation(
            'B:B',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1('$H$3:$H$'.$cantR)
        );

        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $event->sheet->getStyle('E1:F1')->applyFromArray($fondo);
        $event->sheet->getStyle('H1:I1')->applyFromArray($fondo);

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];
        $event->sheet->getStyle('E1:F'.$cantC)->applyFromArray($bordes);
        $event->sheet->getStyle('H1:I'.$cantR)->applyFromArray($bordes);

        $event->sheet->mergeCells('K1:O1');
        $event->sheet->setCellValue('K1', 'REGLAS DE FORMATO');
        $event->sheet->mergeCells('K2:O2');
        $event->sheet->setCellValue('K2', 'Los clientes son los que estan aqui listados');
        $event->sheet->mergeCells('K3:O3');
        $event->sheet->setCellValue('K3', 'los RFV son los que estan aqui listados');

        $event->sheet->getStyle('K1:O1')->applyFromArray($fondo);
        $event->sheet->getStyle('K1:O3')->applyFromArray($bordes);

    }
}