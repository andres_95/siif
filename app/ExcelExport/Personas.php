<?php
namespace RTR\ExcelExport;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use RTR\modelos\TPersona;


class Personas implements FromQuery
{
	use Exportable;


    public function operador($year)
    {
        $this->year = $year;

        return $this;
    }

    public function tipo($t)
    {
    	$this->tipo = $t;
    	return $this;
    }

    public function query()
    {
        return TPersona::query()->where('idOperador', $this->year)->where('idgrupo_persona',$this->tipo);
    }
}