<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 17/04/2018
 * Time: 16:39
 */
namespace RTR\ExcelExport;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use RTR\modelos\TPersona;

class ClienteRfvFormato implements FromCollection, WithHeadings, WithTitle, WithEvents
{
    use Exportable, RegistersEventListeners;

    static $fabricante;

    public function __construct(string $Fabricante)
    {
        self::$fabricante= $Fabricante;
    }

    public function title(): string
    {
        return 'Cliente_Rfv';
    }

    public function headings(): array
    {
        return [
            'RFV',
            'Cliente'
        ];
    }

    public function collection()
    {
        return collect([]);
    }

    public static function aftersheet(aftersheet $event)
    {
        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );
        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('B')->setAutoSize(true);

        $cliente=TPersona::withoutGlobalScopes()
            ->select(['idPersona','nombre_completo_razon_social'])
            ->where('idOperador',Auth::user()->idOperador)
            ->where('idFabricante',self::$fabricante)
            ->where('idgrupo_persona', 'CLI')
            ->get()->toArray();
        $stringC = '"';

        foreach ($cliente as $clave => $valor)
        {
            $stringC.= $valor['idPersona'].',';
        }
        $stringC = trim($stringC, ',').'"';

        $RFV=TPersona::withoutGlobalScopes()
            ->select(['idPersona','nombre_completo_razon_social'])
            ->where('idOperador',Auth::user()->idOperador)
            ->where('idFabricante',self::$fabricante)
            ->where('idgrupo_persona', 'RFV')
            ->get()->toArray();
        $stringR = '"';

        foreach ($RFV as $clave => $valor)
        {
            $stringR.= $valor['idPersona'].',';
        }
        $stringR = trim($stringR, ',').'"';

        $event->sheet->setDataValidation(
            'A:A',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($stringC)
        );

        $event->sheet->setDataValidation(
            'B:B',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($stringR)
        );

        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

    }

}