<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 12/04/2018
 * Time: 03:35 PM
 */

namespace RTR\ExcelExport\PersonaFormato;

use Illuminate\Support\Facades\Request;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PersonaInformacion implements FromCollection, WithTitle, WithEvents, WithColumnFormatting
{
    use Exportable, RegistersEventListeners;

    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
        return [
            'A'=>'@',
            'D'=>'@',
            'G'=>'@',
            'J'=>'@',
            'P'=>'@',
            'S'=>'@',
            'Y'=>'@',
            'M'=>'@',
            'AH'=>'@',
            'V'=>'@',
            'AB'=>'@',
            'AE'=>'@',
            'AK'=>'@',
            'AN'=>'@',
            'AQ'=>'@',
            'AT'=>'@',
            'O'=>'@',
        ];
    }

    public function collection()
    {
        return collect([]);
    }

    public function title(): string
    {
        return 'TablasRelacionadas';
    }

    public static function afterSheet(AfterSheet $event)
    {
        $tabla = new PersonaQuery(Request::getFacadeRoot()->id);

        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

        $start = 'A';
        $end = 'B';
        $ax = 'A';

        foreach ($tabla->info as $e)
        {
            $event->sheet->getColumnDimension($ax)->setAutoSize(true);

            if(sizeOf(str_split($start)) >= 2)
            {
                $last = ++str_split($start)[1];

                if ($last === 'X' || $last === 'Z' || $last === 'AA' || $last ==='AB')
                {
                    $last = ++str_split($start)[0];
                    $start = $last . 'A';
                    $end = $last . 'B';
                }
            }

            if(!$e[1])
            {
                $length = sizeOf($e[0])+2;

                $event->sheet->getStyle($start.'1:'.$end.'1')->applyFromArray($fondo);
                $event->sheet->getStyle($start.'1:'.$end.$length)->applyFromArray($bordes);

                $event->sheet->fromArray(
                    $e[0],
                    NULL,
                    $start . '3'
                );

                $event->sheet->mergeCells($start . '1:' . $end . '1');
                $event->sheet->setCellValue($start . '1', $e[3]);
                $event->sheet->setCellValue($start . '2', 'ID');
                $event->sheet->setCellValue($end . '2', 'DESCRIPCION');
                $event->sheet->getColumnDimension($start)->setAutoSize(true);
                $event->sheet->getColumnDimension($end)->setAutoSize(true);

                if(sizeOf(str_split($start)) >= 2)
                {
                    ++$last;
                    ++$last;
                    $start = $start[0].$last;
                    ++$last;
                    $end = $end[0].$last;
                }else
                {
                    ++$start;
                    ++$start;
                    ++$start;
                    ++$end;
                    ++$end;
                    ++$end;
                }

            }
            ++$ax;
        }
    }

}