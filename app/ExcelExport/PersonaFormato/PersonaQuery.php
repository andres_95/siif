<?php
/**
 * Created by PhpStorm.
 * User: BimodalItil03
 * Date: 20/4/2018
 * Time: 12:28 PM
 */

namespace RTR\ExcelExport\PersonaFormato;


use RTR\modelos\TCadena;
use RTR\modelos\TCiclo;
use RTR\modelos\TCiudade;
use RTR\modelos\TClasePersona;
use RTR\modelos\TEmpresaGrupo;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TEstado;
use RTR\modelos\TFrecuenciaVisita;
use RTR\modelos\TGrupopersona;
use RTR\modelos\TPersona;
use RTR\modelos\TPaise;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TSubEspecialidad;
use RTR\modelos\TTipoPersona;
use RTR\modelos\TTipoEspecialidade;
use RTR\modelos\TTitulo;

class PersonaQuery
{
    public function __construct($Fabricante = '')
    {
        $all = collect(array());

        $fabricante = TPersona::withoutGlobalScopes()->find($Fabricante);

        $all->push([false, 11, null, null]);

        $grupo = TEmpresaGrupo::where('idOperador', $fabricante->idOperador)
            ->get(['id', 'Descripcion']);
        if ($grupo->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'GRUPO EMPRESARIAL']);
        else
            $all->push([$grupo->toArray(), false, '"'.implode(',', $grupo->pluck('id')->toArray()).'"','GRUPO EMPRESARIAL']);

        $sup = TPersona::withoutGlobalScopes()->where('idFabricante', $fabricante->idPersona)->where('idgrupo_persona', 'SUP')
            ->get(['idPersona', 'nombre_completo_razon_social']);
        if ($sup->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'SUPERVISORES']);
        else
            $all->push([$sup->toArray(), false, '"'.implode(',', $sup->pluck('idPersona')->toArray()).'"','SUPERVISORES']);

        $cadenas = TCadena::withoutGlobalScopes()->where('idFabricante', $fabricante->idPersona)
            ->get(['id', 'descripcion']);
        if ($cadenas->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'CADENAS']);
        else
            $all->push([$cadenas->toArray(), false, '"'.implode(',', $cadenas->pluck('id')->toArray()).'"','CADENAS']);

        $pais = TPaise::get(['idpais', 'nombrePais']);
        if ($pais->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'PAISES']);
        else
            $all->push([$pais->toArray(), false, '"'.implode(',', $pais->pluck('idpais')->toArray()).'"','PAISES']);

        $all->push([false, 3, null, null]);
        $all->push([false, 3, null, null]);

        $tipoPersona = TTipoPersona::where('estatus', 1)->get(['id', 'descripcion_tipo_persona']);
        if ($tipoPersona->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'TIPOS DE PERSONA']);
        else
            $all->push([$tipoPersona->toArray(), false, '"'.implode(',', $tipoPersona->pluck('id')->toArray()).'"','TIPOS DE PERSONA']);

        $esp = TTipoEspecialidade::where('estatus', 1)
            ->get(['idespecialidad', 'descripcion']);
        if ($esp->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'ESPECIALIDADES']);
        else
            $all->push([$esp->toArray(), false, '"'.implode(',', $esp->pluck('id')->toArray()).'"','ESPECIALIDADES']);

        $tipoEsp = TEspecialidade::withoutGlobalScopes()->where('estatus', 1)
            ->get(['id', 'descripcion_especialidad']);
        if ($tipoEsp->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'ACTIVIDAD NEGOCIO']);
        else
            $all->push([$tipoEsp->toArray(), false, '"'.implode(',', $tipoEsp->pluck('idespecialidad')->toArray()).'"','ACTIVIDAD NEGOCIO']);

        $subEsp = TSubEspecialidad::where('idestatus', 1)
            ->get(['id','descripcion_actividad_negocio']);
        if ($subEsp->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'SUB-ESPECIALIDADES']);
        else
            $all->push([$subEsp->toArray(), false, '"'.implode(',', $subEsp->pluck('id')->toArray()).'"','SUB-ESPECIALIDADES']);

        $all->push([false, 40, null, null]);

        $grupo = TGrupopersona::withoutGlobalScopes()
            ->get(['idgrupo_persona', 'descripciongrupoPersona']);
        if ($grupo->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'GRUPOS DE PERSONAS']);
        else
            $all->push([$grupo->toArray(), false, '"'.implode(',', $grupo->pluck('idgrupo_persona')->toArray()).'"','GRUPOS DE PERSONAS']);

        $clase = TClasePersona::withoutGlobalScopes()
            ->get(['id', 'descripcion']);
        if ($clase->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "", 'GRUPOS DE PERSONAS']);
        else
            $all->push([$clase->toArray(), false, '"'.implode(',', $clase->pluck('id')->toArray()).'"','CLASES DE PERSONAS']);

        $ranking = TRankingCliente::withoutGlobalScopes()->where('idFabricante', $fabricante->idPersona)
            ->get(['id', 'descripcion_ranking_cliente']);
        if ($ranking->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "",'RANKING']);
        else
            $all->push([$ranking->toArray(), false, '"'.implode(',', $ranking->pluck('id')->toArray()).'"','RANKING']);

        $all->push([false, 40, null, null]);
        $all->push([false, 80, null, null]);
        $all->push([false, 150, null, null]);
        $all->push([false, 1, null, null]);
        $all->push([false,true,null,null]);
        $all->push([false, 20, null, null]);
        $all->push([false, 20, null, null]);
        $all->push([false, 191, null, null]);

        $titulo = TTitulo::where('idestatus', 1)
            ->get(['id', 'descripcion']);
        if ($titulo->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "", 'TITULOS']);
        else
            $all->push([$titulo->toArray(), false, '"'.implode(',', $titulo->pluck('id')->toArray()).'"', 'TITULOS']);

        $ciclos = TCiclo::withoutGlobalScopes()->where('idFabricante', $fabricante->idPersona)
            ->get(['id', 'descripcion_ciclos']);
        if ($ciclos->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "", 'CICLOS']);
        else
            $all->push([$ciclos->toArray(), false, '"'.implode(',', $ciclos->pluck('id')->toArray()).'"', 'CICLOS']);

        $frec = TFrecuenciaVisita::withoutGlobalScopes()->where('idFabricante', $fabricante->idPersona)
            ->get(['idfrecuencia', 'descripcion_frecuencia_visitas']);
        if ($frec->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "", 'FRECUENCIA DE VISITAS']);
        else
            $all->push([$frec->toArray(), false, '"'.implode(',', $frec->pluck('idfrecuencia')->toArray()).'"', 'FRECUENCIA DE VISITAS']);

        $all->push([false, 255, null, null]);

        $estados = TEstado::where('idpais', $fabricante->idpais)
            ->get(['idestado', 'nombreCiudad']);
        if ($estados->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "", 'ESTADOS']);
        else
            $all->push([$estados->toArray(), false, '"'.implode(',', $estados->pluck('idestado')->toArray()).'"', 'ESTADOS']);

        $ciudades = TCiudade::where('idpais', $fabricante->idpais)
            ->get(['idCiudad', 'nombreCiudad']);
        if ($ciudades->isEmpty())
            $all->push([['no disponible', 'no disponible'], false, "", 'CIUDADES']);
        else
            $all->push([$ciudades->toArray(), false, '"'.implode(',', $ciudades->pluck('idCiudad')->toArray()).'"', 'CIUDADES']);

        $all->push([false, 10, null, null]);
        $all->push([false, 30, null, null]);
        $all->push([false, 20, null, null]);
        $all->push([false, 30, null, null]);
        $all->push([false, 20, null, null]);
        $all->push([false, 20, null, null]);
        $all->push([false, 20, null, null]);
        $all->push([false, 120, null, null]);
        $all->push([false, 20, null, null]);
        $all->push([false, 191, null, null]);
        $all->push([false, 11, null, null]);

        $this->info =$all->toArray();
    }
}