<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 12/04/2018
 * Time: 03:35 PM
 */

namespace RTR\ExcelExport\PersonaFormato;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ClienteFormato implements FromCollection, WithMultipleSheets
{
    use Exportable, RegistersEventListeners;

    public function sheets(): array
    {
        $sheets =[];
        $sheets[] = new PersonaInformacion();
        $sheets[] = new PersonaTabla();
        $sheets[] = new ReglasFormato();
        return $sheets;
    }

    public function collection()
    {
        return collect([]);
    }

    public function title(): string
    {
        return 'Persona';
    }

}