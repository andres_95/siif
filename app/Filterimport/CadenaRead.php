<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 03/05/2018
 * Time: 11:53
 */
namespace RTR\Filterimport;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class CadenaRead implements IReadFilter
{

    public function readCell($column, $row, $worksheetName = 'cadenas')
    {
        if (in_array($column,range('A', 'A'))){
            return true;
        }
        return false;
    }
}