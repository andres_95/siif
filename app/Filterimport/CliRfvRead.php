<?php


namespace RTR\Filterimport;

use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class CliRfvRead implements IReadFilter
{

    public function readCell($column, $row, $worksheetName = 'Cliente_RFV')
    {
        if (in_array($column,range('A', 'B'))){
            return true;
        }
        return false;
    }
}