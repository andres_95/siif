<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use RTR\Scopes\OperadorFabricante;

class TTipoActividade extends Model
{
    protected $primaryKey = 'idtipo_actividades';
	public $timestamps = false;

	protected $hidden = ['idOperador','idestatus','idFabricante'];

	protected  $guarded = ['idtipo_actividades'];

	protected $fillable = [
		'idFabricante',
	    'idtipo_actividades',
		'idOperador',
		'descripcion_tipo_actividades',
		'idestatus',
	];

 	public function getDescripcionTipoActividadesAttribute($value)
    {
        return  utf8_encode($value);
    }



 protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
        static::addGlobalScope('idestatus', function (Builder $builder) {
            $builder->where('idestatus', 1);
        });
    }
}
