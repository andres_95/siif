<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TPlanificadore extends Model
{
    protected $primaryKey = 'idAgenda';
	public $incrementing = false;
	public $timestamps = false;

	protected $guarded = [
		'idAgenda'
	];

	protected $hidden = ['idOperador','idreporte'];

	protected $fillable = [
		'idOperador',
		'idFabricante',
		'idRFV',
		'idCliente',
		'fecha_agenda',
		'hora',
		'idbrick',
		'observacion_agenda',
		'idSupervisor',
		'idestatus',
		'idreporte'
	];

	public function Actividad(){
        return $this->belongsTo(TActividadesRepresentante::class,'idreporte','idreporte');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('idestatus', function (Builder $builder) {
            $builder->where('idestatus', 1);
        });
    }

    public function scopeOperadorFabricante($query, $Operador,$Fabricante)
    {
        return $query->where('idOperador', $Operador)->where('idFabricante',$Fabricante);
    }
    
}
