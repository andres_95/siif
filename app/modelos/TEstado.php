<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;

class TEstado extends Model
{
    //
    public $incrementing = false;
	public $timestamps = false;
	protected $primaryKey='idestado';

	protected $fillable = [
		'idOperador',
		'idfabricante',
		'ididioma',
		'nombreCiudad',
		'idestatus'
	];

	public function t_paise()
	{
		return $this->belongsTo(\App\modelos\TPaise::class, 'idpais');
	}

	public function t_ciudades()
	{
		return $this->hasMany(\App\modelos\TCiudade::class, 'idestado');
	}
}
