<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;

class TZona extends Model
{
    protected $table='t_zonas';
	protected $primaryKey='idzona';
	public $incrementing =true;
	public $timestamps = false;

	protected $fillable = [
		'idOperador',
		'idFabricante',
		'idzona',
		'descripcion_zona',
		'idestatus',
		'idciudad',
		'idestado',
		'idpais'
	];
	  public static $validators=[
		  'fabricante'=>'required|exists:t_personas,idPersona',
		  'id'=>'required|unique:t_zonas,idzona',
		  'descripcion'=>'required|unique:t_zonas,descripcion_zona',
		  'idpais'=>'required|exists:t_paises,idpais',
		  'idestado'=>'required|exists:t_estados,idestado',
		  'idciudad'=>'required|exists:t_ciudades,idciudad',
	  ];

	public function bricks()
	{
		return $this->hasMany('RTR\modelos\TBrickRuta','idzona','idzona');
	}

	/* GLOBAL FILTER OPERADOR , FABRICANTE TO CONSULT MYSQL*/
	
	protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
