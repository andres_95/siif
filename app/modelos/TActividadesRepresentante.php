<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;

class TActividadesRepresentante extends Model
{
    protected $table = 't_actividades_representante';
	protected $primaryKey='idreporte';
	public $incrementing = true;
	public $timestamps = false;

	protected $guarded = ['idreporte'];

	protected $dates = [
	    'fecha_actividad'
    ];

	protected $fillable = [
		'idOperador',
		'idFabricante',
		'idPersona',
		'idRFV',
		'idCliente',
		'idtipo_actividades',
		'Firma_cliente',
		'fecha_actividad',
		'idtipo_incidentes',
		'observaciones_cliente',
		'coordenadas_l',
		'coordenadas_a',
		'idestatus'
	];


	public function RFV()
	{
		return $this->belongsTo(\RTR\modelos\TPersona::class, 'idPersona','idPersona');
	}

    public function Cliente(){
	   return $this->belongsTo(\RTR\modelos\TPersona::class, 'idCliente','idPersona');
    }

   	public function Tipo(){
		return $this->belongsTo(\RTR\modelos\TTipoActividade::class, 'idtipo_actividades' ,'idtipo_actividades' );
   	}

    public function Incidente(){
		return $this->belongsTo(\RTR\modelos\TTipoIncidente::class, 'idtipo_incidentes' ,'idtipo_incidentes' );
    }

    public function Muestras(){
	  	return $this->belongsToMany(\RTR\modelos\TProducto::class,'r_actividades_productos','idreporte','idproducto')->withPivot('cantidad','idOperador','idFabricante');
    }

	protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
