<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;

class TEstatusOrdene extends Model
{
    protected $primaryKey = 'idestatus';
    public $timestamps = false;


    protected $hidden = ['idOperador','idFabricante'];

    protected $fillable = ['idOperador','idFabricante','idestatus','descripcion'];

    public function scopeOperadorFabricante($builder,$o,$f)
    {
    	return $builder->where('idOperador',$o)->where('idFabricante',$f);
    }


    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}