<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;
use RTR\Traits\DML;

class TCiclo extends Model
{
	protected $table='t_ciclos';
	protected $primaryKey = 'id';
	public $incrementing = true;
	use DML;

	public $timestamps = false;

	protected $fillable = [
		'idOperador',
		'idFabricante',
		'descripcion_ciclos',
		'idestatus'
	];

	public static $validators=[
		'descripcion'=>'required|unique:t_ciclos,descripcion_ciclos',
		'id'=>'required|unique:t_ciclos,id'
	];

	public static function boot(){
		parent::boot();
		static::addGlobalScope(new OperadorFabricante);
	}
}
