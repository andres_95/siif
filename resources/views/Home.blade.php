@extends('layouts.main')

@section('titulo')
    Inicio
@stop

@section('breadcrumb')
    <li><a href="{{route('home')}}">SIIF</a></li>
    <li>Home</li>
@stop

@section('Content')
    <div class="jumbotron rounded">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="primary--text font-bold mb-4 mt-2">!Hola {{$Nombre}} {{$Apellido}}¡ Bienvenido al sistema automatizado APP SIIF</h1>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-sm-3">
                <h5><span class="font-bold">Perfil: </span> {{$Perfil->alias}} - {{$Perfil->descripcion_perfil}}</h5>
            </div>
            @if($Perfil->tipo!='ADM' && $Perfil->tipo!='OPE')
                <div class="col-sm-4">
                    <h5><span class="font-bold">Empresa-Fabricante: </span> {{$Fabricante->nombre_completo_razon_social}} {{$Fabricante->documento_identidad}}</h5>
                </div>
            @endif
            @if($Perfil->tipo == 'RFV')
                <div class="col-sm-4">
                    <h5><span class="font-bold">Supervisor: </span>{{$Supervisor->nombre_persona}} {{$Supervisor->apellido_persona}}</h5>
                </div>
            @endif
            @if($Perfil->tipo=='ADM' || $Perfil->tipo=='OPE')
                <div class="col-sm-3">
                    <v-select
                            :items="{{$Empresa}}"
                            label="Seleccione Empresa"
                            required
                            item-text="idPersona"
                            item-value="idPersona"
                            return-object
                            v-model="Fabrica"
                            single-line
                            id="empresa"
                    ></v-select>
                </div>
                <div class="col-2">
                    <button class="btn siif btn-w float-right " @click="CambiarFabricante(Fabrica.idPersona)" >Seleccionar</button>
                </div>
            @endif
        </div>
        <div class="row py-1 white-text" style="background-color: rgba(82, 86, 93, 0.83) !important;">
            <div class="col"><p class="text-uppercase h5-responsive my-2 ml-2">Acciones Rapidas</p></div>
        </div>
        @if($Perfil->tipo == 'RFV')
            <div class="row mt-4 justify-content-around">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-danger">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">view_module</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Consultas</p>
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('ConsultaReporte')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-accent">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Reportar</p>
                                <p>Citas</p> 
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('ReportarAgenda')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-accent">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">thumb_up</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Citas</p>
                                <p>Reportadas</p> 
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('reportes')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-success">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">receipt</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Tomar</p>
                                <p>Pedido</p> 
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('tomapedido')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-indigo">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">today</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Planificacion</p>
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('planificar')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-success">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">remove_red_eye</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Seguimiento</p>
                                <p>ORdenes</p>
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('seguimiento')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endif
        @if($Perfil->tipo == 'SUP')
            <div class="row mt-4">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-danger">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Consultas</p>
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('ConsultaReporte')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-accent">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">thumb_up</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Citas</p>
                                <p>Reportadas</p> 
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('reportes')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-success">
                        <div class="dashboard-stat__visual">
                                <i aria-hidden="true" class="icon fa-2x material-icons">remove_red_eye</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Seguimiento</p>
                                <p>ORdenes</p>
                            </div>
                            <div class="dashboard-stat__details__desc">
                                
                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('seguimiento')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endif
        @if($Perfil->tipo == 'ADM' || $Perfil->tipo == 'OPE')
            <div class="row mt-4" v-if="comentario && comentario!='01'">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-red">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Consultar Reportes</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('ConsultaReporte')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-green">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Consulta Gerencial</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('Gerencial')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-blue">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">receipt</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Conciliar</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('facturar')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-green">
                        <div class="dashboard-stat__visual">
                            <i class="fa fa-user fa-3x" style="color: rgb(218, 230, 227); fill-opacity: 0.3;"></i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Personas</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('Personas')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-blue">
                        <div class="dashboard-stat__visual">
                            <i class="fa fa-cubes fa-3x" style="color: rgb(218, 230, 227); fill-opacity: 0.3;"></i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Productos</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('Productos')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-blue">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">cloud_upload</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Monitor</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('monitor')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endif
        @if($Perfil->tipo == 'GRT')
            <div class="row mt-4">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-red">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Consultar Reportes</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('ConsultaReporte')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-green">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Consulta Gerencial</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('Gerencial')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-blue">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Lista Reportes</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('reportes')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                    <div class="dashboard-stat dashboard-stat-border-accent">
                        <div class="dashboard-stat__visual">
                            <i aria-hidden="true" class="icon fa-2x material-icons">content_paste</i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h5-responsive text-uppercase">
                                <p>Seguimiento</p>
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <a class="dashboard-stat__more" href="{{route('seguimiento')}}">
                            Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection
