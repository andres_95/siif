<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sistema SIIF - Web Admin</title>
    <link rel="shortcut icon" href="{{asset('logo.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" >
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}" type= "text/css">
    <link rel="stylesheet" href="{{asset('MDB/css/mdb.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/RTR.css')}}" type= "text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('Head')
</head>
<body onload="actualizaReloj()">
<div class="fondo-home">
    <div class="header-box">
        <header>
            <div class="row">
                <div class="col-sm-4 logo">
                    <div class="modificar text-center">
                        <div class="media align-items-center row">
                            <div class="col-sm-12">
                                <a href="{{route('home')}}"><img class="img-fluid" src="{{asset('img/logo.png')}}" alt="SIIF logo"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar header">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span ><i class="fas fa-bars " style="color: #24943d!important;"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                            <ul class="navbar-nav ml-auto">
                                <!--li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle white-text" href="#"  data-toggle="dropdown" id="navbarDropdownuser" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell victoria-text fa-lg" aria-hidden="true"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" style="margin-top: -4px;  " aria-labelledby="navbarDropdownuser">
                                        <a class="dropdown-item hover-victoria" href="{{url('Perfil')}}"><h5 class="text-center">Consiliadas</h5></a>
                                    </div>
                                </li-->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown white-text" href="#"  data-toggle="dropdown" id="navbarDropdownuser" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-user victoria-text fa-lg" aria-hidden="true"></i><span class="clearfix d-none d-sm-inline-block">{{Auth::user()->nombre_completo_razon_social}}</span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link white-text">Salir</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div>
                        <h6 id="date" class="text-right"></h6>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div id="app" fabricante="{{Auth::getSession()->get('user.teams')}}">
        <v-app id="siif">
            <v-content>
                @yield('main')
            </v-content>
        </v-app>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('MDB/js/mdb.min.js')}}"></script>
@yield('script')
<script>
    function actualizaReloj(){
        /* Capturamos la Hora, los minutos y los segundos */
        marcacion = new Date()
        Hora = marcacion.getHours()
        Minutos = marcacion.getMinutes()
        Segundos = marcacion.getSeconds()
        /* Si la Hora, los Minutos o los Segundos son Menores o igual a 9, le añadimos un 0 */


        var dn="am";
        if (Hora>12){
            dn="pm";
            Hora=Hora-12;
        }
        if (Hora==0)
            Hora=12;
        if (Minutos<=9)
            Minutos="0"+Minutos;
        if (Segundos<=9)
            Segundos="0"+Segundos;

        var mydate=new Date()
        var year=mydate.getYear()
        if (year < 1000)
            year+=1900
        var day=mydate.getDay()
        var month=mydate.getMonth()
        var daym=mydate.getDate()
        if (daym<10)
            daym="0"+daym
        var dayarray=new Array("Domingo,","Lunes,","Martes,","Miércoles,","Jueves,","Viernes,","Sábado,")
        var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")

        var tiempo, fecha, total

        fecha = dayarray[day]+" "+daym+" de "+montharray[month]+" de "+year
        /* En Reloj le indicamos la Hora, los Minutos y los Segundos */
        tiempo = Hora + ":" + Minutos + ":" + Segundos+" "+dn
        total = fecha + " "+ tiempo
        /* Capturamos una celda para mostrar el Reloj */
        document.getElementById('date').innerHTML = total

        /* Indicamos que nos refresque el Reloj cada 1 segundo */
        setTimeout("actualizaReloj()",1000)
    }
</script>
</body>
</html>