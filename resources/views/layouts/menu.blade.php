<div class="cuadro-verde">
    <div align="center" style="margin-top: -34px;font-size: 25px;font-weight: 700;">
        <a href="" class="white-text">SIIF</a>
    </div>
</div>

<nav class="vertical">
    <ul>
        <li><a href="{{route('ConsultaReporte')}}"><i class="far fa-file-alt"></i> Consultas - Reportes</a></li>
        <hr>
        @if(Auth::user()->idgrupo_persona != 'RFV')
            <li><a href="{{route('Gerencial')}}"><i class="far fa-file-alt"></i> Consultas Gerencial</a></li>
            <hr>
        @endif
        <li><a href="#RTR"><i class="far fa-clock"></i> RTR</a>
            <ul>
                <li><a href="{{route('nuevo_reporte')}}">Toma de Reporte</a></li>
                <hr>
                <li><a href="{{route('agenda')}}">Lista de Clientes</a></li>
                <hr>
                <li><a href="{{route('planificar')}}">Planificador de Visitas</a></li>
                <hr>
                <li><a href="{{route('reportes')}}">Lista de Reportes</a></li></li>
            </ul>
        </li>
        <hr>
        <li><a href="#CT"><i class="fas fa-exchange-alt"></i> CT</a>
            <ul>
                <li><a href="{{route('tomapedido')}}"> Toma de Pedidos</a></li>
                <hr>
                <li><a href="{{route('seguimiento')}}"> Seguimiento de Pedidos</a></li>
            </ul>
        </li>
        @if(Auth::user()->idperfil == 2 || Auth::user()->idperfil == 4)
        <hr>
        <li><a href="#Administrar"><i class="fa fa-lock"></i> Administrar</a>
            <ul>
                <li><a href="{{route('facturar')}}">Conciliar Facturas</a></li>
                <hr>
                <li><a href="#">Principales</a>
                    <ul>
                        @if(Auth::user()->idFabricante == 'SIIF' && Auth::user()->idperfil == 2)
                        <li><a href="#" onclick="document.getElementById('operador').submit()">Operadores</a>
                            <form id="operador" action="{{route('ListaPer')}}" method="post" hidden class="dashboard-stat__more">
                                {{csrf_field()}}
                                <input id="fab" name="Fabrica" type="text" hidden value="SIIF">
                                <input id="type" name="tipo" type="text" hidden value="SIIF">
                            </form>
                        </li>
                        <hr>
                        <li><a href="#" onclick="document.getElementById('empresa').submit()">Empresas</a>
                            <form id="empresa" action="{{route('ListaPer')}}" method="post" hidden class="dashboard-stat__more">
                                {{csrf_field()}}
                                <input id="fab" name="Fabrica" type="text" hidden value="SIIF">
                                <input id="type" name="tipo" type="text" hidden value="FABR">
                            </form></li>
                        <hr>
                        @endif
                        <li><a href="{{route('Personas')}}"> Personas</a></li>
                        <hr>
                        <li><a href="{{route('Productos')}}"> Productos</a></li>
                        <hr>
                        <li><a href="{{route('Reportes')}}"> Reportes</a></li>
                        <hr>
                        <li><a href="{{route('Ordenes')}}"> Ordenes-Pedidos</a></li>
                        <hr>
                        <li><a href="{{route('Facturas')}}"> Facturas</a></li>
                        <hr>
                        <li><a href="{{route('Categorias')}}"> Categorias</a></li>
                    </ul>
                </li>
                <hr>
                <li><a href="{{route('facturar')}}">Personas y Aspectos Sociales</a>
                    <ul>
                        <li><a href="{{route('Grupo')}}"> Grupos Sociales</a></li>
                        <hr>
                        <li><a href="{{route('Tipo')}}"> Tipos de Personas</a></li>
                        <hr>
                        <li><a href="{{route('Clase')}}"> Clases Sociales</a></li>
                        <hr>
                        <li><a href="{{route('Especialidad')}}"> Especialidades</a></li>
                        <hr>
                        <li><a href="{{route('Titulo')}}"> Titulos Sociales</a></li>
                    </ul>
                </li>
                <hr>
                <li><a href="#localizacion"> Estrategia Geografica</a>
                    <ul>
                        <li><a href="{{route('Localizacion')}}"> Localización</a></li>
                        <hr>
                        <li><a href="{{route('Zonas')}}"> Zonas</a></li>
                        <hr>
                        <li><a href="{{route('Cadenas')}}"> Cadenas</a></li>
                        <hr>
                        <li><a href="{{route('TLocalizacion')}}"> Formas de Localizacion</a></li>
                        <hr>
                        <li><a href="{{route('ADMVisitas')}}"> Clientes a Visitar</a></li>
                    </ul>
                </li>
                <hr>
                <li><a href="#localizacion"> Parametros</a>
                    <ul>
                        <!--li><a href="#localizacion"> Coberturas</a></li>
                        <hr-->
                        <li><a href="{{route('Ciclos')}}"> Ciclos</a></li>
                        <hr>
                        <li><a href="{{route('Frecuencias')}}"> Frecuencia de Visitas</a></li>
                        <hr>
                        <li><a href="{{route('Ranking')}}"> Ranking de Clientes</a></li>
                        <hr>
                        <li><a href="{{route('Parametros')}}"> Parametros</a></li>
                    </ul>
                </li>
                <hr>
                <li><a href="#localizacion"> Seguridad</a>
                    <ul>
                        <li><a href="{{route('Auditoria')}}"> Auditoria</a></li>
                        <hr>
                        <li><a href="{{route('Logs')}}"> Logs</a></li>
                        <hr>
                        <li><a href="{{route('ListadoNegro')}}"> Lista Negra</a></li>
                        <hr>
                        <li><a href="{{route('Perfil')}}"> Perfiles</a></li>
                        <hr>
                        <li><a href="{{route('Roles')}}"> Rol</a></li><!--
                        <hr>
                        <li><a href="#localizacion"> Notificaciones</a></li>-->
                    </ul>
                </li>
                <hr>
                <li><a href="#localizacion"> Configuracion</a>
                    <ul>
                        <li><a href="{{route('Pais')}}"> Paises</a></li>
                        <hr>
                        <li><a href="{{route('Idioma')}}"> Idiomas</a></li>
                        <hr>
                        <li><a href="{{route('Moneda')}}"> Monedas</a></li>
                        <hr>
                        <!--<li><a href="#localizacion"> Planificaciones</a></li>
                        <hr>
                        <li><a href="#localizacion"> WebSite</a></li>
                        <hr>
                        <li><a href="{{route('RedesS')}}"> Redes Sociales</a></li>
                        <hr>-->
                        <li><a href="{{route('DiasF')}}"> Dias Feriados</a></li>
                        <hr>
                        <li><a href="{{route('DiasV')}}"> Dias Habiles</a></li>
                        <hr>
                        <li><a href="{{route('Horario')}}"> Horarios</a></li>
                        <hr>
                        <li><a href="{{route('Actividades')}}"> Tipos de Actividades</a></li>
                        <hr>
                        <li><a href="{{route('Incidentes')}}"> Tipos de Incidentes</a></li>
                    </ul>
                </li>
                <hr>
                <li><a href="#localizacion"> Caracteristicas Productos</a>
                    <ul>
                        <li><a href="{{route('Esquema')}}"> Productos Promocionados</a></li>
                        <hr>
                        <li><a href="{{route('LineaProduct')}}"> Linea de Productos</a></li>
                        <hr>
                        <li><a href="{{route('TipoProduct')}}"> Tipos de Productos</a></li>
                        <hr>
                        <li><a href="{{route('Unidades')}}"> Unidades Metricas</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <hr>
        <li><a href="#Procesos"><i class="fa fa-cogs"></i> Procesos</a>
            <ul>
                <li><a href="{{route('monitor')}}">Monitor de Archivos</a></li>
                <!--<hr>
                <li><a href="{{route('parametros')}}">Definir Parámetros</a></li>
                <hr>
                <li><a href="{{route('alertas')}}">Manejo de Alertas</a></li>
                <hr>
                <li><a href="#InfHistorica">Información Historica</a>
                    <ul>
                        <li><a href="{{route('historico')}}">Pase a Historicos</a></li>
                        <hr>
                        <li><a href="#InfHistorica">Consulta inf. Historica</a></li>
                    </ul>
                </li>-->
                <hr>
                <li><a href="{{route('notificacion')}}">Notificación</a></li>
            <!--<li><a href="">Configuración</a></li>-->
            </ul>
        </li>
        @endif
        <hr>
        <li><a href="{{route('contacto')}}"><i class="fa fa-user"></i> Contacto</a></li>
        <hr>
        <li><a href="{{url('Manual_SIIF.pdf')}}"><i class="fas fa-file-alt"></i> Manual en Linea</a></li>
        <hr>
        <li><a href="{{route('FQ')}}"><i class="fa fa-question-circle"></i> Preguntas Frecuentes</a></li>
        <hr>
        <li><a href="{{route('videos')}}"><i class="fa fa-play"></i> Video del Sistema</a></li>
        <hr>
    </ul>
</nav>