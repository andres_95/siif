@extends('layouts.main')



@section('titulo')
    Preguntas Frecuentes
@stop


@section('breadcrumb')
    <li><a href="#">SIIF</a></li>
    <li>Preguntas Frecuentes</li>
@stop



@section('Content')

<div class="row">
    <div class="col-12">
        <div class="card card-cascade narrower mt-1 p-2 cuadro-tabla">

           <!-- <div class="view gradient-card-header purple-gradient narrower py-0 mx-4 mb-1 d-flex align-content-center">
                    <h6 class="black-text font-up font-bold mb-0" style="font-weight:700;">Preguntas Frecuentes</h6>
            </div>
            <br>-->
            <div class="content">
                <div class="col-md-12 col-sm-12">
                    <div class="tab-content" style="padding:0; background: #fff;">
                      <!-- START TAB 1 -->
                      <div class="tab-pane active" id="tab_1">
                         <div class="panel-group" id="accordion1">
                            <div class="panel">
                               <div>
                                  <h4 style="font-size:17px !important;">
                                     <a href="#accordion1_1" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle">
                                     1. ¿Cómo me registro?
                                     </a>
                                  </h4>
                               </div>
                               <div class="panel-collapse collapse in" id="accordion1_1">
                                  <div class="panel-body" style="text-align: justify;">
                                    Servicios Integrales I.F. le envía un usuario y clave (el cual utilizara en una sola oportunidad) a través de su email o correo electrónico, con mutuo y previo acuerdo con su empresa contratante.                                  </div>
                               </div>
                            </div>
                            <br>
                            <div class="panel">
                               <div>
                                  <h4 style="font-size:17px !important;">
                                     <a href="#accordion1_2" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                                     2. ¿En cuántos dispositivos puedo descargar la APP?
                                     </a>
                                  </h4>
                               </div>
                               <div class="panel-collapse collapse" id="accordion1_2">
                                  <div class="panel-body" style="text-align: justify;">
                                        Se puede descargar la APP SIIF una sola vez por equipo que vaya a utilizar la FV, una vez logeado en la APP no podrá ser logeado en otro dispositivo, al menos que se realice un acuerdo de más de un equipo por representante.
                                  </div>
                               </div>
                            </div>
                            <br>
                            <div class="panel">
                               <div>
                                  <h4 style="font-size:17px !important;">
                                     <a href="#accordion1_3" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                                     3. ¿Puedo usar la aplicación sin WIFI o Datos de Internet?
                                     </a>
                                  </h4>
                               </div>
                               <div class="panel-collapse collapse" id="accordion1_3">
                                  <div class="panel-body" style="text-align: justify;">
                                     Se puede utilizar sin datos ni WI FI (OFF-LINE) pero, para enviar la información de las actividades realizadas, necesitan tener alguna conexión a internet.
                                  </div>
                               </div>
                            </div>
                            <br>
                            <div class="panel">
                               <div>
                                  <h4 style="font-size:17px !important;">
                                     <a href="#accordion1_4" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                                     4. ¿Puedo reportar clientes que no pertenezcan a mi fichero o aparezcan en la APP?
                                     </a>
                                  </h4>
                               </div>
                               <div class="panel-collapse collapse" id="accordion1_4">
                                  <div class="panel-body" style="text-align: justify;">
                                     Si puedes reportar a los clientes como CLIENTE NO FICHADO y colocarías el nombre y apellido del cliente visitado, una vez reportado se enviaría la información de ALTA (para ingresar a este cliente en el fichero) y sincronizarlo con la APP para que puedas seleccionarlo en el siguiente ciclo de trabajo.
                                  </div>
                               </div>
                            </div>
                            <br>
                            <div class="panel">
                               <div>
                                  <h4 style="font-size:17px !important;">
                                     <a href="#accordion1_5" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                                     5. Diferencias entre MODULO RTR Y MODULO CT
                                     </a>
                                  </h4>
                               </div>
                               <div class="panel-collapse collapse" id="accordion1_5">
                                  <div class="panel-body" style="text-align: justify;">
                                        <p>El módulo RTR está programado en su APP (aplicación) para que pueda reportar actividades y darle seguimiento a su cobertura en el plan establecido, previamente acordado con su empresa contratante.</p>
                                        <br>
                                        <p>El módulo CT está programado en su APP (aplicación) para que pueda procesar y darle seguimiento a cualquier toma de pedido o transferencia, acordado con su empresa contratante.</p>
                                  </div>
                               </div>
                            </div>
                            <br>
                            <div class="panel">
                               <div>
                                  <h4 style="font-size:17px !important;">
                                     <a href="#accordion1_6" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                                     6. ¿Qué visualizo en el botón u opción PERFIL?
                                     </a>
                                  </h4>
                               </div>
                               <div class="panel-collapse collapse" id="accordion1_6">
                                  <div class="panel-body" style="text-align: justify;">
                                        En esta opción podrá observar sus datos y un resumen de sus actividades realizadas hasta el momento con su APP (aplicación).
                                  </div>
                               </div>
                            </div>
                            <br>
                            <div class="panel">
                               <div>
                                  <h4 style="font-size:17px !important;">
                                     <a href="#accordion1_7" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                                     7. EN EL CASO DE: 
                                     </a>
                                  </h4>
                               </div>
                               <div class="panel-collapse collapse" id="accordion1_7">
                                  <div class="panel-body" style="text-align: justify;">
                                     <ul type="none">
                                         <li>No visualizo mi nombre correcto en mi APP (aplicación)</li><br>
                                         <li>No se visualizan los productos correctos</li><br>
                                         <li>No se visualizan los mayoristas correctos</li><br>
                                         <li>No se visualizan mis reportes de los representantes o FV</li><br>
                                         <li>No puedo entrar en la APP </li><br>
                                         <li>No funciona correctamente las opciones de la APP</li><br>
                                         <li>No pueden descargar la APP del E-MAIL</li><br>
                                         <li>Da error al instalar la APP</li><br>
                                         <li>Se cierra la APP sin aviso</li><br>
                                     </ul>
                                     O cualquier inconveniente con la APLICACIÓN<br>
                                     <b>Por favor enviar contáctanos al E-Mail Empresarial, el cual responderemos en la brevedad posible.</b>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
            
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection