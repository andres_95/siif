@extends('layouts.main')


@section('titulo')
    Reportes
@stop


@section('breadcrumb')
    <li><a href="#">Real Time Report (RTR)
        </a></li>
    <li><a href="{{route('nuevo_reporte')}}">Toma de Reporte
        </a></li>
    <li><a href="{{route('ReportarAgenda')}}">Visitas
        </a></li>
    <li>Toma de reporte de visita</li>
@stop

@section('Content')
    <div class="jumbotron py-3">
        <tomareporte 
            :actividades="{{$actividades}}" 
            :incidentes="{{$incidentes}}" 
            :rfv="{{$RFV}}" 
            clientenombre="{{$nombreCli}}"
            :idcliente="'{{$idCli}}'"
            :agenda='{{$idagenda}}'>
        </tomareporte>
    </div>
@endsection