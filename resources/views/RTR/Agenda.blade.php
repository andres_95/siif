@extends('layouts.main')


@section('titulo')
    Lista de Clientes
@stop


@section('breadcrumb')
    <li><a href="#">Real Time Report (RTR)</a></li>
    <li>Lista de Clientes</li>
@stop


@section('Content')

    <div class="row">
        <div class="col-12">
            <div class="card card-cascade narrower mt-1 cuadro-tabla">

                <div class="card-body">
                    <!--Card image-->
                    <div class="view gradient-card-header purple-gradient narrower py-2 mx-1 mb-1 d-flex justify-content-center align-items-center">

                        <h5 class="font-bold font-up mb-0" style="width: 80%;font-size: 16px;">
                            Visitas del Ciclo: <Span>{{$ciclo}}</Span> &nbsp;
                            @if($Dhabiles)
                            Dias habiles del Ciclo: <span>{{$Dhabiles}}</span> &nbsp;
                            Visitas Diarias: <span>{{$ciclo/$Dhabiles}}</span> &nbsp;
                            @endif
                            Cobertura: <span>{{$visitados/$ciclo * 100}}%</span>
                        </h5>

                    </div>
                    <!--/Card image-->
                    <v-spacer></v-spacer>
                    <div class="row">
                        <div class="col-sm-10 col-md-6">
                            <v-text-field
                                    append-icon="search"
                                    label="Buscar"
                                    single-line
                                    hide-details
                                    v-model="search"
                            ></v-text-field>
                        </div>
                        <download-excel
                            class   = "btn  siif float-right btn-w"
                            :data   = "{{$Agenda}}"
                            :fields = "xlsclientes"
                            name    = "ClientesAvisitar.xls">
                        
                            Descargar XLS <i class="far fa-file-alt"></i>
                        
                        </download-excel>
                    </div>

                    <div class="px-4">
                        <v-data-table
                                :headers="headers_tablas.clientes"
                                :items="{{$Agenda}}"
                                class="table-hover table-responsive-md"
                                no-data-text="Sin Clientes"
                                :search="search"
                                :loading="loading"
                                :rows-per-page-items='[15,25,50,{"text":"All","value":-1}]'
                                rows-per-page-text="Registros por pagina"
                        >
                            <v-progress-linear slot="progress" :indeterminate="loading"></v-progress-linear>
                            <template slot="items" slot-scope="props">
                                <td>@{{ props.item.idRFV}}</td>
                                <td >@{{ props.item.nombre_completo_razon_social}}</td>
                                <td >@{{ props.item.idranking}}</td>
                                <td >@{{ props.item.idactividad_negocio}}</td>
                                <td >
                                    <v-menu offset-x transition="slide-x-transition" >
                                        <v-btn  icon  dark slot="activator" class="p-0">
                                            <v-icon color="accent">more_vert</v-icon>
                                        </v-btn>
                                        <v-list dense>
                                            <v-list-tile v-for="item in props.item.dias_visita">
                                                <v-list-tile-title >@{{ item.descripcion_dias_visita }}</v-list-tile-title>
                                            </v-list-tile>
                                        </v-list>
                                    </v-menu>
                                </td>
                                <td >
                                    <v-menu offset-x transition="slide-x-transition" >
                                        <v-btn  icon  dark slot="activator" class="p-0">
                                            <v-icon color="accent">more_vert</v-icon>
                                        </v-btn>
                                        <v-list dense>
                                            <v-list-tile v-for="item in props.item.horarios"  >
                                                <v-list-tile-title>@{{ item.descripcion_horarios }}</v-list-tile-title>
                                            </v-list-tile>
                                        </v-list>
                                    </v-menu>
                                </td>
                                <td >@{{props.item.visitado+" / "+props.item.frecuencia}}
                                    <!--<v-icon color="green" v-if="props.item.visitado-props.item.frecuencia==0">fas fa-check</v-icon>-->
                                </td>
                            </template>
                            <v-alert slot="no-results" :value="true" color="error" icon="warning">
                            Tu busqueda para "@{{ searchmuestras }}" no tuvo resultados.
                            </v-alert>
                        </v-data-table>
                    </div>
                </div>

             </div>
        </div>
    </div>

@endsection

@section('script')

@endsection