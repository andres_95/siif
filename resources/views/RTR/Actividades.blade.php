@extends('layouts.main')


@section('titulo')
    Listado de Reportes
@stop


@section('breadcrumb')
    @if((Auth::user()->idperfil==4 || Auth::user()->idperfil==2) && Auth::user()->idFabricante != 'SIIF')
        <li><a href="#">{{Auth::user()->idFabricante}}</a></li>
    @endif
    <li><a href="#">Real Time Report (RTR)</a></li>
    <li>Listado de Reportes</li>
@stop


@section('Content')


<div class="row">
    <div class="col-12">
        <div class="card card-cascade narrower mt-1 p-3 cuadro-tabla">
            <div class="view gradient-card-header purple-gradient narrower py-2 mx-1 mb-1 d-flex justify-content-center align-items-center">

                <h5 class="font-bold font-up mb-0" style="width: 80%;font-size: 16px;">
                    Visitas del Ciclo: <Span>{{$Tvisit}}</Span> &nbsp;
                    @if($Dhabiles)
                    Dias habiles del Ciclo: <span>{{$Dhabiles}}</span> &nbsp;
                    Visitas Diarias: <span>{{$Tvisit/$Dhabiles}}</span> &nbsp;
                    @endif
                    @if($Tvisit) Cobertura: <span>{{$visit/$Tvisit * 100}}%</span> @endif
                </h5>

            </div>
        
            <div class="px-4,mt-4">

                <v-data-table
                        :headers="headers_tablas.reportes"
                        :items="{{$ActiviD}}"
                        class="table-bordered table-responsive-md"
                        no-data-text="Sin Reportes"
                        :loading="loadermuestras"
                        :rows-per-page-items='[10,25,50,{"text":"All","value":-1}]'
                        rows-per-page-text="Registros por pagina"
                >
                    <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                    <template slot="items" slot-scope="props">
                        <td>@{{ props.item.RFV}}</td>
                        <td >@{{ props.item.Cliente}}</td>
                        <td >@{{ props.item.fecha_actividad}}</td>
                        <td >@{{ props.item.tipo_actividades}}</td>
                        <td >@{{ props.item.observaciones_cliente?props.item.observaciones_cliente:"S/C"}}</td>
                        <td class="justify-center layout">
                            <v-btn class="mx-0 p-0 btn-w" data-toggle="modal" data-target="#myModal" color="siif" :loading="props.item.loading" @click="detallereporte(props.item)">
                                Ver Detalle
                            </v-btn>
                        </td>
                    </template>
                </v-data-table>

            </div>
        

        
        </div>

    </div>

    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header accent">
                    <h4 class="modal-title" id="myModalLabel" style="color:white;">Reporte de Actividad</h4>
                    <button type="button" class="close btn-w" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!--Table-->
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width: 45%; font-weight: 700;">RFV</td>
                                    <td style="width: 45%; font-weight: 700;">CLIENTE</td>
                                </tr>
                                <tr>
                                    <td >
                                        @{{confirm}}
                                    </td>
                                    <td >
                                        @{{codigo}}
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width: 45%; font-weight: 700;">ACTIVIDAD</td>
                                    <td style="width: 45%; font-weight: 700;">FECHA</td>
                                </tr>
                                <tr>
                                    <td >
                                        @{{actividad}}
                                    </td>
                                    <td >
                                        @{{date}}
                                    </td>
                                </tr>
                            </table>
                            <div v-if="cantidad == 1">
                                <br>
                                <table class="table table-bordered " >

                                        <tr align="center" >
                                            <td style="width: 14%; font-weight: 700;">CODIGO</td>
                                            <td style="width: 55%; font-weight: 700;">DESCRIPCION</td>
                                            <td style="width: 7%; font-weight: 700;">CANT.</td>
                                            <td style="width: 14%; font-weight: 700;">LOTE</td>

                                        </tr>
                                    <tr v-for="muestra in mayorista">
                                        <td >@{{muestra.codigo}}</td>
                                        <td >@{{muestra.descripcion}}</td>
                                        <td align="right">@{{muestra.cantidad}}</td>
                                        <td align="right">@{{muestra.lote}}</td>
                                    </tr>
                                </table>
                            </div>
                            <br>
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td style="width: 45%; font-weight: 700;">COMENTARIO</td>
                                    <td style="width: 45%; font-weight: 700;">FIRMA DEL VISITADO</td>
                                </tr>
                                <tr>
                                    <td style="width: 45%;">@{{comentario?comentario:"S/C"}}</td>
                                    <td style="width: 45%;"><img :src=nombre alt=""></td>
                                </tr>
                            </table>
                            <!--Table-->
                        </div>
                    </div>
                    <div id="loader" style="position: absolute;	text-align: center;	top: 55px;	width: 100%;display:none;"></div><!-- Carga gif animado -->
                    <div class="outer_div" ></div><!-- Datos ajax Final -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-ct btn-w siif"  data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</div>
    
@endsection

@section('script')
    
@endsection