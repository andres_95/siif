@extends('layouts.main')


@section('titulo')
    Notificación
@stop


@section('breadcrumb')
    <li><a href="#">Procesos</a></li>
    <li>Notificación</li>
@stop


@section('Content')

<div class="jumbotron rounded">
    <div v-if="loading">
        <v-progress-linear :indeterminate="loading"></v-progress-linear>
        <p class="text-center">Enviando...</p>
    </div>
    <v-alert type="success" dismissible v-model="alertreport">
        Se ha enviado su notificación
    </v-alert>
    <div class="row border border-dark py-3">
        <div class="col-md-4 md-form">
            <v-select
                    :items="{{$Empresa}}"
                    label="Seleccione Empresa"
                    required
                    item-text="nombre_completo_razon_social"
                    item-value="idPersona"
                    return-object
                    v-model="Fabrica"
                    single-line
                    id="empresa"
            ></v-select>
        </div>
        <div class="col-2">
            <button class="btn siif btn-w float-right " @click="rfvs(Fabrica.idPersona)" >Seleccionar</button>
        </div>
        <div class="col-md-4 md-form">
            <v-select
                v-bind:items="rfv"
                label="RFV"
                item-text="nombre_completo_razon_social"
                item-value="idPersona"
                return-object
                dense
                clearable
                v-model="codigo"
            ></v-select>
        </div>
        <div class="col-md-12 md-form px-5">
                <v-text-field
                label="Descripcion"
                textarea
                rows="3"
                counter="255"
                name="descripcion"
                v-model="comentario"
                ></v-text-field>
        </div>
        <div class="col-12">
                <p class="red-text"><b>Nota:</b> De no especificar el código la notificacion sera enviada a todos .</p>
        </div>
    </div>
    <div class="row ">
            <div class="col-12" align="right">
                <button class="btn btn-ct victoria" @click="notificacion({{Auth::user()}})" data-toggle="modal" data-target="#myModal" style="margin-left: 4%;margin-top: 3%;width: 148px; height: 30px; padding:0px !important;">Enviar</button>
            </div>
    </div>
</div>
@endsection
