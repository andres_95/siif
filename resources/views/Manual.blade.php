@extends('layouts.main')

@section('titulo')
    Manual RTR
@stop


@section('breadcrumb')
    <li><a href="">SIIF</a></li>
    <li>Manual en linea</li>
@stop



@section('Content')

    <div class="row">
        <div class="col-12">
            <div class="card card-cascade narrower mt-1 p-2 cuadro-tabla ">
                <div style="border:1px solid gray;padding:0;">
                    <p >Para poder usar los servicios de nuestra plataforma online usted debera aceptar los siguientes términos y condiciones de uso, de no hacerlo o no estar de acuerdo con alguno de ellos no se registre ni descargue nuestra aplicación ya que no prodrá acceder ni utilizar nuestros servicios.</p>
                    <p >Los Términos y Condiciones de Uso funcionan como contrato vinculante entre el usuario y RTR. En este contrato se rigen las normas para el uso de los Servicios que RTR ofrece como online marketplace a través de su aplicación móvil y sitio web, para que los usuarios puedan comprar o vender productos en linea deben aceptar todos los Términos y Condiciones de Uso, de lo contrario no tendran derecho a utilizar los servicios.</p>
                    <p >Al utilizar los servicios de cualquier manera usted está aceptando nuestras condiciones de uso las mismas permaneceran en vigor mientras usted use los Servicios de nuestro online marketplace.</p>
                    <p >Estos Términos y Condiciones de Uso incluyen Política de Privacidad, asi como las directrices, politicas y reglamentos que RTR pone a disposicion de los Servicios estos últimos están sujetos a cambios en oportunidades sin previo aviso.</p>

                    <h5>Cambios en los Términos y Condiciones de Uso</h5>

                    <p >Nos esforzaremos continuamente por mejorar nuestro Servicios, por lo que en oportunidades puede resultar necesario cambiar estos Términos junto con los servicios parareflejar esas mejoras. Nos reservamos el derecho de cambiar los Términos en cualquier momento y por cualquier razón. Si bien podemos optar por notificar cuando se realizaran cambios sustanciales, le invitamos a revisar con frecuencia la ultima copia de estos Términos en nuestro sitio web: www.RTR.com para asegurarse de las condiciones vigentes.</p>

                    <h5> Politicas de privacidad; Seguridad y Confidencialidad de datos personales</h5>

                    <p >Seguridad y Confidencialidad de datos personales, RTR toma muy enserio la privacidad de sus usuarios. conozca la actual Politica, haciendo click aqui(vinculo).</p>
                    <p >Si Usted es es menor de 13 años no debe utilizar este sitio ni descargar nuestra App. No recopilamos ni solicitamos informacion de identificación personalñ de niños menores de 13 años; Si usted es un niño menor de 13 años, absténgase de enviar cualquier información personal sobre usted a nosotros. Si nos esnteramos de que hemos recopilado información personal de un niño menor de 13 años, eliminaremos esa informacion lo más rápidamente posible.</p>

                    <h5>Edad Requerida</h5>

                    <p >Los Servicios están destinados a ser utilizados por adultos, según lo definido bajo cualquier ley aplicable en cada caso específico, Si no es un adulto, no está autorizado para descargar la aplicacion, registrarse o crear una cuenta con nosotros. Los niños mayores de 13 años que deseen utilizar nuestros servicios requieren el permiso de su representante legal rspectivo para poder hacer uso de los mismos.</p>

                    <h5>Uso correcto del online marketplace RTR</h5>

                    <p >Debe registrarse para obtener una cuenta, seleccionar una contraseña y un nombre de usuario. Usted se compromete a proporcionarnos información de registro exacta, completa y veraz. También se compromete a notificarnos con prontitud si cambia el número de telefono movil que nos proporsionó para comunicarnos con usted.</p>
                    <p >No puede seleccionar como nombre de usuario en RTR un nombre del cual no tiene derecho a usar o el nombre de otra persona con la intencion de hacerse paras por esta. Si usted proporciona informacion falsa RTR no se hace responsable, debido a que no se puede verificar la información proporcionada con certeza.</p>
                    <p >Usted no puede transferir su cuenta a otra persona sin solicitar de manera previa permiso por escrito.</p>
                    <p >RTR lo obliga a no utilizar sus servicios para llevar acabo actividades ilícitas o que de alguna manera constituyan un delito, incluyendo violaciones de los derechos de un tercero.</p>

                    <h5> Acciones Prohibidas</h5>

                    <p >En relación con el uso o acceso a los Servicios, usted acuerda no realizar o participar en ninguna acción de la siguiente lista no exhaustiva de otras acciones que se puedan considerar prohibidas:</p>
                </div>
            </div>
        </div>
    </div>
@endsection