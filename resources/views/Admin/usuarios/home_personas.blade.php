@extends('layouts.main')


@section('titulo')
    Administrar Personas
@stop


@section('breadcrumb')
    <li>Administrar</li>
    <li>Administrar Personas</li>
@stop



@section('Content')
    <div class="jumbotron rounded">
        <div class="row">
            <div class="col-md-4 md-form">
                <v-select
                        :items="{{  $Empresa }}"
                        label="Seleccione Fabricante"
                        required
                        item-text="idPersona"
                        item-value="idPersona"
                        id="empresa"
                        return-object
                        v-model="Fabrica"
                        name=incidente
                        dense
                ></v-select>
            </div>
            <div class="col-2">
                <button class="btn siif btn-w float-right " @click="CategoriPersonaFabricante(Fabrica.idPersona)" >Seleccionar</button>
            </div>
            <div class="col-md-4 md-form">
                <v-progress-circular :indeterminate="loading" color="primary" v-if="loading"></v-progress-circular>
            </div>
        </div>

        <div  id="categorias" style="display: none">
            <div class="row mt-3">
                <div class="col-md-4 col-sm-6 col-xs-12 col-12" v-for="(mayor,index) in mayorista">
                    <div class="dashboard-stat dashboard-stat-border-blue">
                        <div class="dashboard-stat__visual">
                            <i class="fa fa-user fa-3x" style="color: rgb(218, 230, 227); fill-opacity: 0.3;"></i>
                        </div>
                        <div class="dashboard-stat__details">
                            <div class="dashboard-stat__details__number h3-responsive">
                                @{{mayor.descripciongrupoPersona}}
                            </div>
                            <div class="dashboard-stat__details__desc">

                            </div>
                        </div>
                        <div >
                            <form  action="{{route('ListaPer')}}" method="post" class="dashboard-stat__more">
                                {{csrf_field()}}
                                <input id="fab" name="Fabrica" type="text" hidden v-model="Fabrica.idPersona">
                                <input id="type" name="tipo" type="text" hidden v-model="mayor.idgrupo_persona">
                                <button type="submit" class="btn-w">Listar Todos</button> <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection