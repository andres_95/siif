@extends('layouts.main')


@section('titulo')
    Agregar Usuarios
@stop


@section('breadcrumb')
    <li>Administrar</li>
    <li><a href="{{route('Personas')}}">Administrar Personas</a></li>
    <li>Actualizar Personas</li>
@stop

@section('Content')
    <div class="jumbotron">
        <div class="">
            <p class="h4-responsive font-weight-light border-title">Actualizar Persona - {{$DescripGrupopersona->descripciongrupoPersona}}</p>
            <p class="font-bold">Fabricante: <span class="font-weight-light">{{$fabricante}}</span> </p>
        </div>
        @if($tipo=='CLI')
            <form action="{{route('updP')}}" method="post" class="mt-1 pt-0" >
                {{csrf_field()}}
                <div class="row mt-1 p-1 border-dark border">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Personales:</p>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="nombre"
                                label="Nombres"
                                value="{{$nombre_persona}}"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="apellido"
                                label="Apellidos"
                                value="{{$apellido_persona}}"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2">
                        <div class="md-form">
                            <v-select
                            :items="{{  $tipopersona }}"
                            item-text="descripcion_tipo_persona"
                            item-value="cod_tipo_persona"
                            return-object
                            single-line
                            name="tipopersona"
                            ></v-select>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <div class="md-form">
                            <v-text-field
                                name="documento_identidad"
                                label="Documento identidad"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="email"
                                label="email"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="telf"
                                label="Telefono"
                                type="number"
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <v-text-field
                            name="Direccion"
                            label="Direccion"
                            textarea
                            rows="3"
                            counter="100"
                            ></v-text-field>
                    </div>
                    <div class="col-md-6 col-sm-6 align-self-center pl-5">
                        <div class="form-group form-md-radios ">
                            <span>Genero</span>
                            <v-radio-group name="Genero" column>
                                <v-radio label="Femenino" value="F" ></v-radio>
                                <v-radio label="Masculino" value="M"></v-radio>
                            </v-radio-group>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark" style="min-height: 100px;">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Basicos Cliente:</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="fabricante"
                                label="Fabricante"
                                value="{{$fabricante}}"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="fabricante" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="Tpersona"
                                label="Tipo Persona"
                                value={{$tipo}}
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="Tpersona" value={{$tipo}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $especialidad }}"
                            label="Especialidad"
                            item-text="descripcion_especialidad"
                            item-value="id"
                            return-object
                            autocomplete
                            v-model="especialidad"
                            dense
                            ></v-select>
                            <input type="hidden" name="especialidad" v-model="especialidad.id">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $clase }}"
                            label="Clase"
                            item-text="descripcion"
                            item-value="id"
                            return-object
                            autocomplete
                            v-model="confirm"
                            dense
                            ></v-select>
                            <input type="hidden" name="clase" v-model="confirm.id">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $ranking }}"
                            label="Ranking"
                            item-text="descripcion_ranking_cliente"
                            item-value="id"
                            return-object
                            autocomplete
                            v-model="factura"
                            dense
                            ></v-select>
                            <input type="hidden" name="ranking" v-model="factura.id">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $freq }}"
                            label="Frecuencia"
                            item-text="descripcion_frecuencia_visitas"
                            item-value="idfrecuencia"
                            return-object
                            autocomplete
                            v-model="statusF"
                            ></v-select>
                            <input type="hidden" name="frecuencia" v-model="statusF.idfrecuencia">
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Ubicacion</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $paises }}"
                            label="Pais"
                            item-text="nombrePais"
                            item-value="idpais"
                            v-model="nombre"
                            return-object
                            autocomplete
                            dense
                            @focusout="buscaestado()"
                            ></v-select>
                        </div>
                        <input type="hidden" name="pais" v-model="nombre.idpais">
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayoristas"
                            label="Estado"
                            item-text="nombreCiudad"
                            item-value="idestado"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayoristas"
                            v-model="comentario"
                            @focusout="buscaciudad()"
                            ></v-select>
                            <input type="hidden" name="estado" v-model="comentario.idestado">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayorista"
                            label="Ciudad"
                            item-text="nombreCiudad"
                            item-value="idCiudad"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayorista"
                            v-model="incidente"
                            ></v-select>
                            <input type="hidden" name="ciudad" v-model="incidente.idCiudad">
                        </div>
                    </div>
                </div>
                <!--<div class="row mt-3 border-dark border p-3">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Permisos y roles:</p>
                </div>-->
                <div  align="right">
                    <button type="submit" class="btn btn-w siif">Registrar</button>
                </div>
            </form>
        @endif
        
        @if($tipo=='MAY')
            <form action="{{route('storeP')}}" method="post" class="mt-1 pt-0" >
                {{csrf_field()}}
                <div class="row mt-1 p-1 border-dark border">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Personales:</p>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="nombre"
                                label="Nombres"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="apellido"
                                label="Apellidos"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2">
                        <div class="md-form">
                            <v-select
                            :items="{{  $tipopersona }}"
                            item-text="descripcion_tipo_persona"
                            item-value="cod_tipo_persona"
                            return-object
                            single-line
                            name="tipopersona"
                            ></v-select>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <div class="md-form">
                            <v-text-field
                                name="documento_identidad"
                                label="Documento identidad"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="email"
                                label="email"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="telf"
                                label="Telefono"
                                type="number"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <v-text-field
                            name="Direccion"
                            label="Direccion"
                            textarea
                            rows="3"
                            counter="100"
                            ></v-text-field>
                    </div>
                    <div class="col-md-6 col-sm-6 align-self-center pl-5">
                        <div class="form-group form-md-radios ">
                            <span>Genero</span>
                            <v-radio-group name="Genero" column>
                                <v-radio label="Femenino" value="F" ></v-radio>
                                <v-radio label="Masculino" value="M"></v-radio>
                            </v-radio-group>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark" style="min-height: 100px;">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Basicos Mayorista:</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="fabricante"
                                label="Fabricante"
                                value="{{$fabricante}}"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="fabricante" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="Tpersona"
                                label="Tipo Persona"
                                value={{$tipo}}
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="Tpersona" value={{$tipo}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="descuento"
                                label="Descuento"
                                type="number"
                                min='0'
                                max='100'
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Ubicacion</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $paises }}"
                            label="Pais"
                            item-text="nombrePais"
                            item-value="idpais"
                            v-model="nombre"
                            return-object
                            autocomplete
                            dense
                            @focusout="buscaestado()"
                            ></v-select>
                        </div>
                        <input type="hidden" name="pais" v-model="nombre.idpais">
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayoristas"
                            label="Estado"
                            item-text="nombreCiudad"
                            item-value="idestado"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayoristas"
                            v-model="comentario"
                            @focusout="buscaciudad()"
                            ></v-select>
                            <input type="hidden" name="estado" v-model="comentario.idestado">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayorista"
                            label="Ciudad"
                            item-text="nombreCiudad"
                            item-value="idCiudad"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayorista"
                            v-model="incidente"
                            ></v-select>
                            <input type="hidden" name="ciudad" v-model="incidente.idCiudad">
                        </div>
                    </div>
                </div>
                <div align="right">
                    <button type="submit" class="btn btn-w siif">Registrar</button>
                </div>
            </form>
        @endif

        @if($tipo!='MAY' && $tipo!='CLI' && $tipo!='RFV' && $tipo!='SUP')
            <form action="{{route('storeP')}}" method="post" class="mt-1 pt-0" >
                {{csrf_field()}}
                <div class="row mt-1 p-1 border-dark border">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Personales:</p>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="nombre"
                                label="Nombres"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="apellido"
                                label="Apellidos"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2">
                        <div class="md-form">
                            <v-select
                            :items="{{  $tipopersona }}"
                            item-text="descripcion_tipo_persona"
                            item-value="cod_tipo_persona"
                            return-object
                            single-line
                            name="tipopersona"
                            ></v-select>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <div class="md-form">
                            <v-text-field
                                name="documento_identidad"
                                label="RIFF"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="email"
                                label="email"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="telf"
                                label="Telefono"
                                type="number"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <v-text-field
                            name="Direccion"
                            label="Direccion"
                            textarea
                            rows="3"
                            counter="100"
                            ></v-text-field>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark" style="min-height: 100px;">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Basicos Laboratorio:</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="fabricante"
                                label="Fabricante"
                                value="{{$fabricante}}"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="fabricante" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="Tpersona"
                                label="Tipo Persona"
                                value={{$tipo}}
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="Tpersona" value={{$tipo}}>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Ubicacion</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $paises }}"
                            label="Pais"
                            item-text="nombrePais"
                            item-value="idpais"
                            v-model="nombre"
                            return-object
                            autocomplete
                            dense
                            @focusout="buscaestado()"
                            ></v-select>
                        </div>
                        <input type="hidden" name="pais" v-model="nombre.idpais">
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayoristas"
                            label="Estado"
                            item-text="nombreCiudad"
                            item-value="idestado"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayoristas"
                            v-model="comentario"
                            @focusout="buscaciudad()"
                            ></v-select>
                            <input type="hidden" name="estado" v-model="comentario.idestado">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayorista"
                            label="Ciudad"
                            item-text="nombreCiudad"
                            item-value="idCiudad"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayorista"
                            v-model="incidente"
                            ></v-select>
                            <input type="hidden" name="ciudad" v-model="incidente.idCiudad">
                        </div>
                    </div>
                </div>
                <div>
                    <button type="submit" class="btn btn-success mt-3">Guardar</button>
                </div>
            </form>
        @endif

        @if($tipo=='RFV' || $tipo=='SUP')
            <form action="{{route('storeP')}}" method="post" class="mt-1 pt-0" >
                {{csrf_field()}}
                <div class="row mt-1 p-1 border-dark border">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Personales:</p>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="nombre"
                                label="Nombres"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="apellido"
                                label="Apellidos"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2">
                        <div class="md-form">
                            <v-select
                            :items="{{  $tipopersona }}"
                            item-text="descripcion_tipo_persona"
                            item-value="cod_tipo_persona"
                            return-object
                            name="tipopersona"
                            ></v-select>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <div class="md-form">
                            <v-text-field
                                name="documento_identidad"
                                label="Documento identidad"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="email"
                                label="email"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="telf"
                                label="Telefono"
                                type="number"
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <v-text-field
                            name="Direccion"
                            label="Direccion"
                            textarea
                            rows="3"
                            counter="100"
                            ></v-text-field>
                    </div>
                    <div class="col-md-6 col-sm-6 align-self-center pl-5">
                        <div class="form-group form-md-radios ">
                            <span>Genero</span>
                            <v-radio-group name="Genero" column>
                                <v-radio label="Femenino" value="F" ></v-radio>
                                <v-radio label="Masculino" value="M"></v-radio>
                            </v-radio-group>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark" style="min-height: 100px;">
                    @if($tipo=='RFV')
                        <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Basicos RFV:</p>
                    @else
                        <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Basicos SUP:</p>
                    @endif
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="fabricante"
                                label="Fabricante"
                                value="{{$fabricante}}"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="fabricante" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="Tpersona"
                                label="Tipo Persona"
                                value={{$tipo}}
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="Tpersona" value={{$tipo}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                            name="username"
                            label="Username"
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                            name="password"
                            label="Password"
                            type="password"
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $ciclo }}"
                            label="Ciclo"
                            item-text="descripcion_ciclos"
                            item-value="id"
                            return-object
                            autocomplete
                            v-model="date"
                            ></v-select>
                            <input type="hidden" name="ciclo" v-model="date.id">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $freq }}"
                            label="Supervisor"
                            item-text="descripcion_frecuencia_visitas"
                            item-value="idfrecuencia"
                            return-object
                            autocomplete
                            v-model="codigo"
                            ></v-select>
                            <input type="hidden" name="frecuencia" v-model="codigo.idfrecuencia">
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Ubicacion</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $paises }}"
                            label="Pais"
                            item-text="nombrePais"
                            item-value="idpais"
                            v-model="nombre"
                            return-object
                            autocomplete
                            dense
                            @focusout="buscaestado()"
                            ></v-select>
                        </div>
                        <input type="hidden" name="pais" v-model="nombre.idpais">
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayoristas"
                            label="Estado"
                            item-text="nombreCiudad"
                            item-value="idestado"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayoristas"
                            v-model="comentario"
                            @focusout="buscaciudad()"
                            ></v-select>
                            <input type="hidden" name="estado" v-model="comentario.idestado">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="mayorista"
                            label="Ciudad"
                            item-text="nombreCiudad"
                            item-value="idCiudad"
                            return-object
                            autocomplete
                            dense
                            :disabled="!mayorista"
                            v-model="incidente"
                            ></v-select>
                            <input type="hidden" name="ciudad" v-model="incidente.idCiudad">
                        </div>
                    </div>
                </div>
                <div align="right">
                    <button type="submit" class="btn btn-w siif">Registrar</button>
                </div>
            </form>
        @endif

    </div>
@endsection


@section('script')

    
@endsection