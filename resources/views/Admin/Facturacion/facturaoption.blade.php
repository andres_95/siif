@extends('layouts.main')



@section('titulo')
    Facturar
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Facturar</li>
@stop



@section('Content')
    <div class="jumbotron rounded">
                <div class="alert alert-info d-block" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
                  <h4 class="alert-heading font-bold" >Información</h4>
                  <p style="line-height: 1.5; font-size: 1rem">Solo se pueden conciliar ordenes que no han sido anuladas o previamente conciliadas, solo se mostrara el primer mayorista solicitado en una orden en caso de existir mas, si la orden se concilia con productos faltantes el sistema generara y le mostrara una nueva orden con el siguiente mayorista, si solo si, existe otro mayorista.</p>
                  <hr class="my-3">
                  <p class="mb-0"><span class="font-bold">¡Importante!</span> Debe seleccionar una empresa fabricante para filtrar correctamente las ordenes a conciliar</p>
                </div>
            <div class="row mt-2 py-2 border border-dark">
                <div class="col-sm-6 col-md-4 md-form">
                    <v-select
                            :items="{{$Fabricantes}}"
                            label="Fabricante"
                            required
                            item-text="nombre_completo_razon_social"
                            item-value="idPersona"
                            return-object
                            v-model="Fabrica"
                            id="empresa"
                            dense
                    ></v-select>
                </div>
                <div class="col-sm-6 col-md-4">
                        <form method="post" action="{{route('conciliar')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="Fabricante" v-model="Fabrica.idPersona">
                            <button type="submit" class="btn btn-accent btn-w" :disabled="!Fabrica.idPersona">Conciliar</button>
                        </form>
                    </div>
            </div>
       



    </div>
@endsection