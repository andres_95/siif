@extends('layouts.main')


@section('titulo')
    Administrar Categorias
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Administrar Categorias</li>
@stop


@section('Content')
    <div class="jumbotron rounded">
        
        <div class="row">
            <div class="col-md-4 md-form">
                <v-select
                        :items="{{  $Empresa }}"
                        label="Seleccione Fabricante"
                        required
                        item-text="idPersona"
                        item-value="idPersona"
                        return-object
                        v-model="nombre"
                        single-line
                        id="empresa"
                        @input="CategoriProductFabricante(nombre.idPersona)"
                ></v-select>
            </div>
            <div class="col-md-4 md-form">
                <v-progress-circular :indeterminate="loading" color="primary" v-if="loading"></v-progress-circular>
            </div>
        </div>

        <div  id="categorias" style="display: none">
            <v-data-table
                    :headers="headers_tablas.categorias"
                    :items="mayorista"
                    class="table-bordered table-responsive-md"
                    no-data-text="Sin Categoria"
                    :loading="loadermuestras"
            >
                <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                <template slot="items" slot-scope="props">
                    <td>@{{ props.item.idcategorias}}</td>
                    <td>@{{ props.item.NombreCategorias}}</td>
                    <td >@{{ props.item.FechaCreacion}}</td>
                    <td class="justify-center layout px-0">
                        <v-btn icon class="mx-0" @click="deleteCategoria(props.item)">
                            <v-icon color="accent">delete</v-icon>
                        </v-btn>
                    </td>
                </template>
            </v-data-table>
        </div>
    </div>

    
@endsection