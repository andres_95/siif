@extends('layouts.main')


@section('titulo')
    Administrar Esquemas
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Administrar Esquemas</li>
@stop


@section('Content')
    <div class="jumbotron rounded">
        <esquema 
            :Empresa="{{$Empresa}}"
            :mayoristas="{{$mayoristas}}"
        >
        </esquema>
    </div>

@endsection