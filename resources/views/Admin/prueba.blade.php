@extends('layouts.main')


@section('titulo')
    Agregar Usuarios
@stop


@section('breadcrumb')
    <li>Administrar</li>
    @if($DescripGrupopersona != 'Fabricante' && $DescripGrupopersona != 'Operadores')
        <li><a href="{{route('Personas')}}">Administrar Personas</a></li>
    @endif
    <li>Agregar {{$DescripGrupopersona}}</li>
@stop

@section('Content')
    <div class="jumbotron">
        <div class="">
            <p class="h4-responsive font-weight-light border-title">Agregar - {{$DescripGrupopersona}}</p>
            <p class="font-bold">Fabricante: <span class="font-weight-light">{{$Empresa}}</span> </p>
        </div>
        <prueba
                fabricante="{{$Empresa}}"
                categ="{{$grupopersona}}"
                accion="{{$accion}}"
                :tipo="{{$tipopersona}}"
                :pais="{{$paises}}"
                :es="{{$especialidad}}"
                :cl="{{$clase}}"
                :r="{{$ranking}}"
                :fr="{{$freq}}"
              
        ></prueba>
    </div>

@endsection