@extends('layouts.main')


@section('titulo')
    Pedidos
@stop


@section('breadcrumb')
    <li><a href="#">Centro de Transferencias (CT)</a></li>
    <li>Toma de Pedidos</li>
@stop


@section('Content')
    <div class="jumbotron">
        
        <v-alert type="success" dismissible v-model="alertreport">
            Se ha procesado la orden Nº @{{confirm}} con exito
        </v-alert>
        <v-alert type="error" dismissible v-model="alertreporterror">
            Hubo un error al procesar
        </v-alert>
        <v-alert type="error" dismissible v-model="alertbuscar">
            No se encontro la busqueda
        </v-alert>
        <div class="row px-2 pt-2" >
            <div class="col-2 md-form">
                <v-text-field
                        label="Código"
                        id="codigo"
                        v-model=codigo
                        @focusout="codigoE(codigo)"
                ></v-text-field>
            </div>&nbsp;&nbsp;
            <div class="col-4 md-form">
                <v-select
                        label="Cliente"
                        autocomplete
                        :loading="loadingselect"
                        cache-items
                        required
                        :rules="[() => nombre != '' || 'Seleccione una empresa puede hacer busquedas']"
                        :items="clientes"
                        @input="inputcodigo(nombre.idPersona)"
                        return-object
                        item-text="nombre_completo_razon_social"
                        item-value="idPersona"
                        :search-input.sync="search"
                        append-icon="search"
                        v-model="nombre"
                ></v-select>
            </div>
            <div class="col-md-5 md-form">
                <v-text-field
                label="Pasado por"
                value= '{{Auth::user()->nombre_completo_razon_social}}'
                disabled
              ></v-text-field>
            </div>
        </div>
        <div class="row px-2 pt-2">
            <div class="col-2 pt-2">
                <label class="prefix" for="representante">Representante</label>
            </div>
            <div class="col-4 mx-3 md-form">
                <select class="form-control" id="representante">
                    @if(Auth::user()->idgrupo_persona=='RFV')
                        <option value="{{Auth::user()->idPersona}}">{{Auth::user()->nombre_completo_razon_social}}</option>
                    @endif
                    @foreach($RFV as $rfv)
                        <option value="{{$rfv->idPersona}}">{{$rfv->nombre_completo_razon_social}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-5 md-form" style="margin-top: -16px;">
                <v-text-field
                        label="Comentario"
                        textarea
                        rows="3"
                        counter="500"
                        v-model="comentario"
                ></v-text-field>
            </div>
        </div>
        <div class="narrower py-0 mx-2 mb-1 d-flex row align-items-center">
            <div class="col-2"><h6 class="black-text font-bold font-up mb-0" style="font-weight:700; font-size:15px; !important;">Mayoristas</h6></div>

        </div>
        <div class="row align-items-center" >
            <div class="col-12 ">
                <v-data-table
                        :headers="headers_tablas.mayoristas"
                        :items="mayoristas"
                        class="table-bordered table-responsive-md"
                        no-data-text="Sin Mayoristas"
                        rows-per-page-text="Registros por pagina"
                >
                    <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                    <template slot="items" slot-scope="props">
                        <td>@{{ props.item.id}}</td>
                        <td >@{{ props.item.nombre }}</td>
                        <td >@{{ props.item.descuento }}</td>
                        <td>
                        <v-btn icon flat class="mx-0 p-0" color="blue" @click="borrar3(props.index)">
                            <v-icon color="blue">delete</v-icon>
                        </v-btn>
                        </td>
                    </template>
                    <template slot="footer">
                        <td colspan="100%">
                            <div class="row">
                                <div class="col-sm-4 d-flex">
                                    <button
                                            class="btn btn-w btn-ct siif"
                                            @click="buscamayorista()"
                                            data-toggle="modal"
                                            data-target="#myModal2"
                                            style="width: 148px; height: 30px; padding:0px !important;">
                                        Agregar Mayorista
                                    </button>
                                </div>
                            </div>
                        </td>
                    </template>
                </v-data-table>
            </div>
        </div>
        <br>
        <hr>
        <br>
        <div class="row ">
            <div class="col-12">
                <v-data-table
                        :headers="headers_tablas.productos"
                        :items="muestrasfinal"
                        class="table-bordered table-responsive-md"
                        no-data-text="Sin Productos"
                        rows-per-page-text="Registros por pagina"
                >
                    <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                    <template slot="items" slot-scope="props">
                        <td>@{{ props.item.id}}</td>
                        <td >@{{ props.item.nombre }}</td>
                        <td align="right">@{{ props.item.cantidad }}</td>
                        <td align="right">@{{ props.item.descuento }}</td>
                        <td align="right">@{{ props.item.precio.toLocaleString() }}</td>
                        <td>
                            <v-btn icon flat class="mx-0 p-0" color="blue" @click="borrar2(props.item.cantidad*1,props.index,props.item.cantidad*props.item.precio,props)">
                                <v-icon color="accent">delete</v-icon>
                            </v-btn>
                        </td>
                    </template>
                </v-data-table>
            </div>
        </div>
        <br>
        <div class="row" >
            <div class="col-2 align-self-center">
                <button class="btn btn-w btn-ct siif" @click="buscaproductos()" data-toggle="modal" data-target="#myModal" style="margin-left: 4%;margin-top: -2%;width: 148px; height: 30px;">Agregar Productos</button>
            </div>
            <div class="col-2 justify-content-center d-flex">
                <h6 class="black-text font-bold  mb-0 align-self-center"><b>Total items:</b> <span>@{{cantidad}}</span></h6>
                
            </div>
            <div class="col-3 justify-content-center d-flex">
                <h6 class="black-text font-bold mb-0 align-self-center"><b>Monto total: </b><span>
                    @{{total.toLocaleString()}}
                </span></h6>
                
            </div>
            <div class="col-3 justify-content-end ">
                <div class="md-form">
                    <input type="number" name="impuesto" id="impuesto" v-model="impuesto" max="100" min="0" class="form-control">
                    <label for="impuesto">Impuesto</label>
                </div>
            </div>
            <div class="col-2 align-self-center">
                <button class="btn btn-w btn-ct siif float-right" @click="validarenviopedido()">Procesar</button>
                <button class="btn btn-w btn-ct siif float-right" hidden data-toggle="modal" data-target="#myModal3">Procesar</button>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col">
                <p style="font-size: 1rem" class="text-center"><span>NOTA: </span>El impuesto es calculado al momento de la concilicacion de esta orden, los precios como descuentos pueden cambiar al momento de la conciliacion.</p>
            </div>
        </div>
        <div v-if="loading">
            <v-progress-linear :indeterminate="loading"></v-progress-linear>
            <p class="text-center">Procesando...</p>
        </div>
    </div>
    <div>
        <!--Modal Productos-->
        <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel" >Buscar productos</h4>
                        <v-spacer></v-spacer>
                        <v-text-field
                                append-icon="search"
                                label="Buscar"
                                single-line
                                hide-details
                                v-model="searchmuestras"
                        ></v-text-field>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <v-data-table
                                :headers="headers_tablas.productos"
                                :items="muestras"
                                class="table-bordered table-responsive-md"
                                no-data-text="Sin Productos"
                                :search="searchmuestras"
                                :loading="loadermuestras"
                                rows-per-page-text="Registros por pagina"
                        >
                            <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                            <template slot="items" slot-scope="props">
                                <td>@{{ props.item.id}}</td>
                                <td >@{{ props.item.nombre }}</td>
                                <td align="right">
                                    <v-text-field
                                    v-model=props.item.cant
                                    type="number"
                                    class="text-xs-center"
                                    min=1
                                    ></v-text-field>
                                </td>
                                <td>@{{ props.item.descuento }}</td>
                                <td align="right">@{{ props.item.precio }}</td>
                                <td class="justify-center layout">
                                    <v-btn flat icon class="mx-0 p-0" color="blue" @click="enviar2(props.item)">
                                        <v-icon color="accent">add</v-icon>
                                    </v-btn>
                                </td>
                            </template>
                            <v-alert slot="no-results" :value="true" color="error" icon="warning">
                                Tu busqueda para "@{{ searchmuestras }}" no tuvo resultados.
                            </v-alert>
                        </v-data-table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-w btn-ct siif"  data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal mayoristas -->
        <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel" >Buscar mayorista</h4>
                        <v-spacer></v-spacer>
                        <v-text-field
                                append-icon="search"
                                label="Buscar"
                                single-line
                                hide-details
                                v-model="searchmuestras2"
                        ></v-text-field>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <v-data-table
                                :headers="headers_tablas.mayoristas"
                                :items="mayorista"
                                class="table-bordered table-responsive-md"
                                no-data-text="Sin Mayoristas"
                                :search="searchmuestras2"
                                :loading="loadermuestras"
                                rows-per-page-text="Registros por pagina"
                        >
                            <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                            <template slot="items" slot-scope="props">
                                <td>@{{ props.item.id}}</td>
                                <td >@{{ props.item.nombre }}</td>
                                <td >@{{ props.item.descuento }}</td>
                                <td class="justify-center layout">
                                    <v-btn flat icon class="mx-0 p-0" color="blue" @click="enviar3(props.item)">
                                        <v-icon color="accent">add</v-icon>
                                    </v-btn>
                                </td>
                            </template>
                            <v-alert slot="no-results" :value="true" color="error" icon="warning">
                                Tu busqueda para "@{{ searchmuestras2 }}" no tuvo resultados.
                            </v-alert>
                        </v-data-table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-w btn-ct siif"  data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal confirmacion -->
        <div class="modal fade bs-example-modal-lg" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header accent">
                        <h4 class="modal-title" id="myModalLabel" style="color:white;">Confirmación</h4>
                        <button type="button" class="close btn-w" data-dismiss="modal" @click="refrescar()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="formchangepassword">
                            <div class="row">
                                <div class="col-6">
                                    <h5>Introduzca código para enviar el pedido</h5>
                                    <br>
                                    <v-text-field
                                        label="Codigo"
                                        name="codigo"
                                        type=password
                                        v-model=pass
                                        required
                                        :rules="[() => pass != '' || 'Debe colocar el codigo de confirmación']"
                                    ></v-text-field>
                                </div>
                            </div>
                            <div id="loader" style="position: absolute;	text-align: center;	top: 55px;	width: 100%;display:none;"></div><!-- Carga gif animado -->
                            <div class="outer_div" ></div><!-- Datos ajax Final --><br>
                            <div class="modal-footer">
                                <button type="button" class="btn siif btn-w" @click="password()">Enviar</button>
                                <button type="button" class="hidden" data-dismiss="modal" id="cerrar"></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection